// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CommonAssetToolFactoryBase.h"
#include "CommonImageToolFactory.generated.h"

/**
 * 
 */
UCLASS(BlueprintType, Category="CommonAssetTool")
class COMMONASSETTOOLS_API UCommonImageToolFactory : public UCommonAssetToolFactoryBase
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable, Category=CommonAssetTool)
	class UCommonImageTool* CreateTool();
	
public:
	UPROPERTY()
	UCommonImageTool* GCPointer;
};
