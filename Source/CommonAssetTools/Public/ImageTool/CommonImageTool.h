// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/Texture2D.h"
#include "HAL/PlatformFileManager.h"
#include "Misc/FileHelper.h"
#include "Runtime/Engine/Public/TextureResource.h"
#include "IImageWrapper.h"
#include "IImageWrapperModule.h"
#include "CommonImageTool.generated.h"

UENUM(BlueprintType, Category="CommonAssetTool")
enum class FEImageFormat : uint8
{
	/** Invalid or unrecognized format. */
	Invalid = 0xFF,

	/** Portable Network Graphics. */
	PNG = 0,
	
	/** Joint Photographic Experts Group. */
	JPEG,

	/** Single channel JPEG. */
	GrayscaleJPEG,	

	/** Windows Bitmap. */
	BMP,

	/** Windows Icon resource. */
	ICO,

	/** OpenEXR (HDR) image file format. */
	EXR,

	/** Mac icon. */
	ICNS,
	
	/** Truevision TGA / TARGA */
	TGA,

	/** Hdr file from radiance using RGBE */
	HDR,

	/** Tag Image File Format files */
	TIFF,

	/** DirectDraw Surface */
	DDS,
};

UCLASS(BlueprintType, Category="CommonAssetTool")
class COMMONASSETTOOLS_API UCommonImageTool : public UObject
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable, Category="CommonAssetTool")
	void ImportImageWithPath(const FString& ImagePath,  UTexture2D*& ImgObj, int32& OutWidth, int32& OutHeight, TArray<uint8>& ImgBin, FEImageFormat& ImgFormat);

	UFUNCTION(BlueprintCallable, Category="CommonAssetTool")
	void ImportImageWithBinary(const TArray<uint8>& ImgBin, FEImageFormat ImgFormat,  UTexture2D*& ImgObj, int32& OutWidth, int32& OutHeight);

private:
	TSharedPtr<class IImageWrapper> GetImageWrapperByExtention(const FString InImagePath, EImageFormat& ImageFormat);
	TSharedPtr<class IImageWrapper> GetImageWrapperByExtention(FEImageFormat ImgFormat);
};
