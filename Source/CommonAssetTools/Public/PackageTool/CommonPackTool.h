// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GenericPlatform/GenericPlatformFile.h"
#include "Templates/SharedPointer.h"
#include "CommonPackTool.generated.h"

class FPathPermissionList;

USTRUCT(BlueprintType, Category="CommonAssetTool")
struct FAssetPackageConfig
{
	GENERATED_BODY()
	
	FAssetPackageConfig() : RefPath(L""), RefObject(nullptr) {}

	UPROPERTY(BlueprintReadWrite, Category="CommonAssetTool")
	FString RefPath;
	
	UPROPERTY(BlueprintReadWrite, Category="CommonAssetTool")
	UObject* RefObject;
};

UCLASS(BlueprintType, Category="CommonAssetTool")
class COMMONASSETTOOLS_API UCommonPackTool : public UObject
{
	GENERATED_BODY()
public:
	UCommonPackTool();
	virtual ~UCommonPackTool() override;

	UFUNCTION(BlueprintCallable, Category="CommonAssetTool")
	void LoadAssetPackage(FString InpakFullPath);
	
	UFUNCTION(BlueprintCallable, Category="CommonAssetTool")
	void LoadAsset(FString AssetNameWithoutPathAndSuffix, bool bIsLevelAsset, TArray<FAssetPackageConfig>& AssetObjects, TArray<FString>& LevelRefPaths);
	
	UFUNCTION(BlueprintCallable, Category="CommonAssetTool")
	TArray<UObject*> LoadAssetPackFromFBX(const FString& DestinationPath, bool bAllowAsyncImport);
private:
	void Conv_RealPath_RefPath(const FString& RealPath, FString& RefPath, bool bIsLevel);
	//TSharedRef<FPathPermissionList>& GetWritableFolderPermissionList() const;
	void NotifyBlockedByWritableFolderFilter() const;
private:
	//Pak包导入相关
	class FPakPlatformFile* PakPlatform;
	TArray<FString> FoundFilenames;
	FString PakFileFullPath;
	FString NewMountPoint;
	UPROPERTY()
	TArray<AActor*> SpawnedActors;
	int32 result;
	//FBX导入相关
	/** Permission list of folder paths to write to */
	//TSharedRef<FPathPermissionList> WritableFolderPermissionList;
};
