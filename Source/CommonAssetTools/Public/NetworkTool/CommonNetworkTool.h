// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "HttpModule.h"
#include "Interfaces/IHttpResponse.h"
#include "Templates/SharedPointer.h"
#include "CommonNetworkTool.generated.h"


UCLASS(BlueprintType, Category="CommonAssetTool")
class COMMONASSETTOOLS_API UCommonNetworkTool : public UObject
{
	GENERATED_BODY()
public:
	UCommonNetworkTool();
	UFUNCTION(BlueprintCallable, Category="CommonAssetTool")
	bool IsNetworkConnected() const;

	UFUNCTION(BlueprintCallable, Category="CommonAssetTool")
	void RunApi(const FString& APIAddress);

	UFUNCTION(BlueprintCallable, Category="CommonAssetTool")
	void GetApiResult(FString& ResultContent, int32& APIState);

private:
	void OnApiResult(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	void GetApiState(int32& APIAddress) const;
private:
	FHttpRequestPtr HttpRequest;
	FString APIResultStr;
	int32 APIResStateCode;
};
