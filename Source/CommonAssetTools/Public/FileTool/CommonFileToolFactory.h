// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CommonAssetToolFactoryBase.h"
#include "CommonFileToolFactory.generated.h"

/**
 * 
 */
UCLASS()
class COMMONASSETTOOLS_API UMyCommonFileToolFactory : public UCommonAssetToolFactoryBase
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable, Category=CommonAssetTool)
	class UCommonFileTool* CreateTool();
	
public:
	UPROPERTY()
	UCommonFileTool* GCPointer;
};
