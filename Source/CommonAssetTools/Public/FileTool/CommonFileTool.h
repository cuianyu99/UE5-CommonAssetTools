// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#ifdef _WIN32
#include "Windows/AllowWindowsPlatformTypes.h"
	#include <commdlg.h>
	#include <shellapi.h>
	#include <shlobj.h>
	#include <Winver.h>
	#include <LM.h>
	#include <tlhelp32.h>
	#include <Psapi.h>
	#include <windows.h>
#include "Windows/HideWindowsPlatformTypes.h"
#include "Microsoft/COMPointer.h"
#endif
#include "CommonFileTool.generated.h"

USTRUCT(BlueprintType, Category=CommonAssetTool)
struct FFileSuffixType
{
	GENERATED_BODY()
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category=CommonAssetTool)
	FString SuffixName;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category=CommonAssetTool)
	FString SuffixType;
};

UCLASS(BlueprintType, Category="CommonAssetTool")
class COMMONASSETTOOLS_API UCommonFileTool : public UObject
{
	GENERATED_BODY()
public:
	UCommonFileTool();
	virtual ~UCommonFileTool();
	
	UFUNCTION(BlueprintCallable, Category=CommonAssetTool)
	void GetFilePathWithinFileDialog(FString DialogTitle, FString DefaultPath, TArray<FFileSuffixType> SurffixTypes, bool bSave, FString& FilePath);

	UFUNCTION(BlueprintCallable, Category=CommonAssetTool)
	void CreateFolder(FString DirPath);
	
	UFUNCTION(BlueprintCallable, Category=CommonAssetTool)
	void RenameFile(FString OriginalFileName, FString NewFileName);

	UFUNCTION(BlueprintCallable, Category=CommonAssetTool)
	void CopyFile(FString SrcFile, FString DestFile);

	UFUNCTION(BlueprintCallable, Category=CommonAssetTool)
	void GetFileName(const FString& FilePath, FString& FileName);

	UFUNCTION(BlueprintCallable, Category=CommonAssetTool)
	void ReadFileToString(const FString& FilePath, FString& FileContent);

	UFUNCTION(BlueprintCallable, Category=CommonAssetTool)
	bool IsDirectoryExists(const FString& DirPath) const;

	UFUNCTION(BlueprintCallable, Category=CommonAssetTool)
	void RemoveFilesWithinDirectory(const FString& TargetDir);
private:
	TComPtr<IFileDialog> pfd;
	HRESULT hr;
};
