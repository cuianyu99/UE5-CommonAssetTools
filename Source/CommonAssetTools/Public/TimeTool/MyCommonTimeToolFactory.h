// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CommonAssetToolFactoryBase.h"
#include "MyCommonTimeToolFactory.generated.h"

/**
 * 
 */
UCLASS()
class COMMONASSETTOOLS_API UMyCommonTimeToolFactory : public UCommonAssetToolFactoryBase
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable, Category=CommonAssetTool)
	class UCommonTimeTool* CreateTool();
	
public:
	UPROPERTY()
	UCommonTimeTool* GCPointer;
};
