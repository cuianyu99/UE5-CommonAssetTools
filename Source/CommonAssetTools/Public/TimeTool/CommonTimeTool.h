// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "CommonTimeTool.generated.h"

USTRUCT(BlueprintType, Category="CommonAssetTool")
struct FDateTimeGetter
{
	GENERATED_BODY()
	
	FDateTimeGetter()
	{
		Year.Empty();
		Month.Empty();
		Day.Empty();
		Hour.Empty();
		Minute.Empty();
		Second.Empty();
		IsAM.Empty();
	}

	void Clear()
	{
		Year.Empty();
		Month.Empty();
		Day.Empty();
		Hour.Empty();
		Minute.Empty();
		Second.Empty();
		IsAM.Empty();
	}
	
	UPROPERTY(BlueprintReadWrite, Category="CommonAssetTool")
	FString Year;
	
	UPROPERTY(BlueprintReadWrite, Category="CommonAssetTool")
	FString Month;
	
	UPROPERTY(BlueprintReadWrite, Category="CommonAssetTool")
	FString Day;
	
	UPROPERTY(BlueprintReadWrite, Category="CommonAssetTool")
	FString Hour;
	
	UPROPERTY(BlueprintReadWrite, Category="CommonAssetTool")
	FString Minute;
	
	UPROPERTY(BlueprintReadWrite, Category="CommonAssetTool")
	FString Second;
	
	UPROPERTY(BlueprintReadWrite, Category="CommonAssetTool")
	FString IsAM;
};

UCLASS(BlueprintType, Category="CommonAssetTool")
class COMMONASSETTOOLS_API UCommonTimeTool : public UObject
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable, BlueprintPure, Category="CommonAssetTool")
	void GetDateTimeToString(FDateTimeGetter& DateTimeStr, bool bTo12);
private:
	void IsAm(const int32& Hour, FString& Result);
};
