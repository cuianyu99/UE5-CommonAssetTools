// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "CommonAssetToolsSubsystem.generated.h"

class UCommonAssetToolFactoryBase;

UCLASS()
class COMMONASSETTOOLS_API UCommonAssetToolsSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable, Category=CommonAssetTool, meta=(WorldContext="WorldContextObject", DeterminesOutputType="ToolClass"))
	static UCommonAssetToolFactoryBase* CreateTool(const UObject* WorldContextObject, TSubclassOf<UCommonAssetToolFactoryBase> ToolClass);
};
