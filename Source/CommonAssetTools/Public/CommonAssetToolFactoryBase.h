// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "CommonAssetToolFactoryBase.generated.h"

/**
 * 
 */
UCLASS(Abstract, Category="CommonAssetTool")
class COMMONASSETTOOLS_API UCommonAssetToolFactoryBase : public UObject
{
	GENERATED_BODY()
};
