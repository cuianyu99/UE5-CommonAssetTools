// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "CommonStringTool.generated.h"

/**
 * 
 */
UCLASS(BlueprintType, Category="CommonAssetTool")
class COMMONASSETTOOLS_API UCommonStringTool : public UObject
{
	GENERATED_BODY()
public:
	/// 任意字符串转SHA1码
	UFUNCTION(BlueprintCallable, Category="CommonAssetTool")
	FString AnyStringToSha1(const FString& AnyStr);
};
