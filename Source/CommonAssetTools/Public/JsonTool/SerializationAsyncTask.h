// Copyright 2023 Alexander Floden, Alias Alex River. All Rights Reserved. 

#pragma once

#include "CoreMinimal.h"
#include "RapidJsonUEFunctionLibrary.h"
#include "Async/AsyncWork.h"


struct FSerializationAsyncTask {


	DECLARE_DELEGATE_OneParam(FOnTaskComplete, const FAsyncSerializationInfo&);
	FOnTaskComplete OnTaskComplete;


	TStatId GetStatId() const {

		RETURN_QUICK_DECLARE_CYCLE_STAT(FSerializationAsyncTask, STATGROUP_ThreadPoolAsyncTasks);
	}

	bool CanAbandon() {

		return true;
	}

	void Abandon() {

		// Cleanup or finalize any remaining logic if the task is abandoned
	}

	FAsyncSerializationInfo serializationInfoToFill;

	FSerializationAsyncTask(FAsyncSerializationInfo serializationInfo) {

		serializationInfoToFill = serializationInfo;
	}

	void DoWork() {


		switch (serializationInfoToFill.arrayType) {

			case ESerializationArrayTypes::Null:
				break;

			case ESerializationArrayTypes::Int32Array:

				// If array have been set previous to this, we should serialize, instead of de-serialize. 
				if (serializationInfoToFill.int32Array.Num() > 0)
					serializationInfoToFill.serializedString = URapidJsonUEFunctionLibrary::SerializeTArrayInt_Wrapper(serializationInfoToFill.int32Array);
				else if (serializationInfoToFill.serializedString.Len() > 0)
					serializationInfoToFill.int32Array = URapidJsonUEFunctionLibrary::DeserializeTArrayInt_Wrapper(serializationInfoToFill.serializedString);

				break;

			case ESerializationArrayTypes::Uint8Array:

				if (serializationInfoToFill.uint8Array.Num() > 0)
					serializationInfoToFill.serializedString = URapidJsonUEFunctionLibrary::SerializeTArrayUInt8_Wrapper(serializationInfoToFill.uint8Array);
				else if (serializationInfoToFill.serializedString.Len() > 0)
					serializationInfoToFill.uint8Array = URapidJsonUEFunctionLibrary::DeserializeTArrayUInt8_Wrapper(serializationInfoToFill.serializedString);

				break;

			case ESerializationArrayTypes::Int64Array:

				if (serializationInfoToFill.int64Array.Num() > 0)
					serializationInfoToFill.serializedString = URapidJsonUEFunctionLibrary::SerializeTArrayInt64_Wrapper(serializationInfoToFill.int64Array);
				else if (serializationInfoToFill.serializedString.Len() > 0)
					serializationInfoToFill.int64Array = URapidJsonUEFunctionLibrary::DeserializeTArrayInt64_Wrapper(serializationInfoToFill.serializedString);

				break;

			case ESerializationArrayTypes::FStringArray:

				if (serializationInfoToFill.stringArray.Num() > 0)
					serializationInfoToFill.serializedString = URapidJsonUEFunctionLibrary::SerializeTArrayFString_Wrapper(serializationInfoToFill.stringArray);
				else if (serializationInfoToFill.serializedString.Len() > 0)
					serializationInfoToFill.stringArray = URapidJsonUEFunctionLibrary::DeserializeTArrayFString_Wrapper(serializationInfoToFill.serializedString);

				break;

			case ESerializationArrayTypes::FColorArray:

				if (serializationInfoToFill.colorArray.Num() > 0)
					serializationInfoToFill.serializedString = URapidJsonUEFunctionLibrary::SerializeTArrayFColor_Wrapper(serializationInfoToFill.colorArray);
				else if (serializationInfoToFill.serializedString.Len() > 0)
					serializationInfoToFill.colorArray = URapidJsonUEFunctionLibrary::DeserializeTArrayFColor_Wrapper(serializationInfoToFill.serializedString);

				break;

			case ESerializationArrayTypes::BoolArray:

				if (serializationInfoToFill.boolArray.Num() > 0)
					serializationInfoToFill.serializedString = URapidJsonUEFunctionLibrary::SerializeTArrayBool_Wrapper(serializationInfoToFill.boolArray);
				else if (serializationInfoToFill.serializedString.Len() > 0)
					serializationInfoToFill.boolArray = URapidJsonUEFunctionLibrary::DeserializeTArrayBool_Wrapper(serializationInfoToFill.serializedString);

				break;

			case ESerializationArrayTypes::FNameArray:

				if (serializationInfoToFill.nameArray.Num() > 0)
					serializationInfoToFill.serializedString = URapidJsonUEFunctionLibrary::SerializeTArrayFName_Wrapper(serializationInfoToFill.nameArray);
				else if (serializationInfoToFill.serializedString.Len() > 0)
					serializationInfoToFill.nameArray = URapidJsonUEFunctionLibrary::DeserializeTArrayFName_Wrapper(serializationInfoToFill.serializedString);

				break;

			case ESerializationArrayTypes::FTextArray:

				if (serializationInfoToFill.textArray.Num() > 0)
					serializationInfoToFill.serializedString = URapidJsonUEFunctionLibrary::SerializeTArrayFText_Wrapper(serializationInfoToFill.textArray);
				else if (serializationInfoToFill.serializedString.Len() > 0)
					serializationInfoToFill.textArray = URapidJsonUEFunctionLibrary::DeserializeTArrayFText_Wrapper(serializationInfoToFill.serializedString);

				break;

			case ESerializationArrayTypes::FloatArray:

				if (serializationInfoToFill.floatArray.Num() > 0)
					serializationInfoToFill.serializedString = URapidJsonUEFunctionLibrary::SerializeTArrayFloat_Wrapper(serializationInfoToFill.floatArray);
				else if (serializationInfoToFill.serializedString.Len() > 0)
					serializationInfoToFill.floatArray = URapidJsonUEFunctionLibrary::DeserializeTArrayFloat_Wrapper(serializationInfoToFill.serializedString);

				break;

			case ESerializationArrayTypes::FVectorArray:

				if (serializationInfoToFill.vectorArray.Num() > 0)
					serializationInfoToFill.serializedString = URapidJsonUEFunctionLibrary::SerializeTArrayFVector_Wrapper(serializationInfoToFill.vectorArray);
				else if (serializationInfoToFill.serializedString.Len() > 0)
					serializationInfoToFill.vectorArray = URapidJsonUEFunctionLibrary::DeserializeTArrayFVector_Wrapper(serializationInfoToFill.serializedString);

				break;

			case ESerializationArrayTypes::FRotatorArray:

				if (serializationInfoToFill.rotatorArray.Num() > 0)
					serializationInfoToFill.serializedString = URapidJsonUEFunctionLibrary::SerializeTArrayFRotator_Wrapper(serializationInfoToFill.rotatorArray);
				else if (serializationInfoToFill.serializedString.Len() > 0)
					serializationInfoToFill.rotatorArray = URapidJsonUEFunctionLibrary::DeserializeTArrayFRotator_Wrapper(serializationInfoToFill.serializedString);

				break;

			case ESerializationArrayTypes::FTransformArray:

				if (serializationInfoToFill.transformArray.Num() > 0)
					serializationInfoToFill.serializedString = URapidJsonUEFunctionLibrary::SerializeTArrayFTransform_Wrapper(serializationInfoToFill.transformArray);
				else if (serializationInfoToFill.serializedString.Len() > 0)
					serializationInfoToFill.transformArray = URapidJsonUEFunctionLibrary::DeserializeTArrayFTransform_Wrapper(serializationInfoToFill.serializedString);

				break;

			default:
				break;
		}

		OnTaskComplete.ExecuteIfBound(serializationInfoToFill);
	}
};