// Fill out your copyright notice in the Description page of Project Settings.


#include "PackageTool/CommonPackageToolFactory.h"
#include "PackageTool/CommonPackTool.h"

UCommonPackTool* UCommonPackageToolFactory::CreateTool()
{
	if (!GCPointer)
	{
		GCPointer = NewObject<UCommonPackTool>();
	}

	return GCPointer;
}
