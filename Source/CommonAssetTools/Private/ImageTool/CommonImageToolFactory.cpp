// Fill out your copyright notice in the Description page of Project Settings.


#include "ImageTool/CommonImageToolFactory.h"

#include "ImageTool/CommonImageTool.h"

UCommonImageTool* UCommonImageToolFactory::CreateTool()
{
	if (!GCPointer)
	{
		GCPointer = NewObject<UCommonImageTool>();
	}

	return GCPointer;
}
