// Fill out your copyright notice in the Description page of Project Settings.


#include "TimeTool/MyCommonTimeToolFactory.h"
#include "TimeTool/CommonTimeTool.h"

UCommonTimeTool* UMyCommonTimeToolFactory::CreateTool()
{
	if (!GCPointer)
	{
		GCPointer = NewObject<UCommonTimeTool>();
	}

	return GCPointer;
}
