// Fill out your copyright notice in the Description page of Project Settings.


#include "TimeTool/CommonTimeTool.h"
#include "Misc/FileHelper.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetStringLibrary.h"

void UCommonTimeTool::GetDateTimeToString(FDateTimeGetter& DateTimeStr, bool bTo12)
{
	const auto& year = UKismetMathLibrary::Now().GetYear();
	const auto& month = UKismetMathLibrary::Now().GetMonth();
	const auto& Day = UKismetMathLibrary::Now().GetDay();
	const auto& Hour = UKismetMathLibrary::Now().GetHour();
	const auto& Minute = UKismetMathLibrary::Now().GetMinute();
	const auto& Second = UKismetMathLibrary::Now().GetSecond();
	
	DateTimeStr.Clear();
	
	DateTimeStr.Year = UKismetStringLibrary::Conv_IntToString(year);
	
	DateTimeStr.Month = month < 10 ? DateTimeStr.Month.Append("0" + UKismetStringLibrary::Conv_IntToString(month))
				: UKismetStringLibrary::Conv_IntToString(month);

	DateTimeStr.Day = Day < 10 ? DateTimeStr.Day.Append("0" + UKismetStringLibrary::Conv_IntToString(Day))
			: UKismetStringLibrary::Conv_IntToString(Day);

	if (bTo12)
	{
		const int32 Hour12 = Hour > 12 ? Hour - 12 : Hour;

		DateTimeStr.Hour = Hour12 < 10 ? DateTimeStr.Hour.Append("0" + UKismetStringLibrary::Conv_IntToString(Hour12))
			: UKismetStringLibrary::Conv_IntToString(Hour12);

		IsAm(Hour, DateTimeStr.IsAM);
	}
	else
	{
		DateTimeStr.Hour = Hour < 10 ? DateTimeStr.Hour.Append("0" + UKismetStringLibrary::Conv_IntToString(Hour))
			: UKismetStringLibrary::Conv_IntToString(Hour);
	}

	DateTimeStr.Minute = Minute < 10 ? DateTimeStr.Minute.Append("0" + UKismetStringLibrary::Conv_IntToString(Minute))
			: UKismetStringLibrary::Conv_IntToString(Minute);

	DateTimeStr.Second = Second < 10 ?  DateTimeStr.Second.Append("0" + UKismetStringLibrary::Conv_IntToString(Second))
			: UKismetStringLibrary::Conv_IntToString(Second);
}

void UCommonTimeTool::IsAm(const int32& Hour, FString& Result)
{
	Result = Hour >= 0 && Hour <= 11 ? L"AM" : L"PM";
}
