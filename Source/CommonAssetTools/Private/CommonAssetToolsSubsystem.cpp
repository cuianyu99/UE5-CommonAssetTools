// Fill out your copyright notice in the Description page of Project Settings.


#include "CommonAssetToolsSubsystem.h"
#include "CommonAssetToolFactoryBase.h"

UCommonAssetToolFactoryBase* UCommonAssetToolsSubsystem::CreateTool(const UObject* WorldContextObject,
	TSubclassOf<UCommonAssetToolFactoryBase> ToolClass)
{
	if (ToolClass)
	{
		return NewObject<UCommonAssetToolFactoryBase>(const_cast<UObject*>(WorldContextObject), ToolClass);
	}

	return nullptr;
}
