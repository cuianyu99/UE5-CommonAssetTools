// Copyright Epic Games, Inc. All Rights Reserved.

#include "CommonAssetTools.h"
#include "UObject/Package.h"

#define LOCTEXT_NAMESPACE "FCommonAssetToolsModule"

void FCommonAssetToolsModule::StartupModule()
{
}

void FCommonAssetToolsModule::ShutdownModule()
{
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FCommonAssetToolsModule, CommonAssetTools)