// Fill out your copyright notice in the Description page of Project Settings.


#include "NetworkTool/CommonNetWorkToolFactory.h"
#include "NetworkTool/CommonNetworkTool.h"

UCommonNetworkTool* UCommonNetWorkToolFactory::CreateTool()
{
	if (!GCPointer)
	{
		GCPointer = NewObject<UCommonNetworkTool>();
	}
			
	return GCPointer;
}
