import os

def DeleteAllFiles(folder_path):
    for filename in os.listdir(folder_path):
        file_path = os.path.join(folder_path, filename)

        if os.path.isfile(file_path):
            os.remove(file_path)


DeleteAllFiles('C:/Users/lenovo/Desktop/Test')