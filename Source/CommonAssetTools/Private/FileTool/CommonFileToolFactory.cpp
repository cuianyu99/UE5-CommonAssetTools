// Fill out your copyright notice in the Description page of Project Settings.


#include "FileTool/CommonFileToolFactory.h"
#include "FileTool/CommonFileTool.h"

UCommonFileTool* UMyCommonFileToolFactory::CreateTool()
{
	if (!GCPointer)
	{
		GCPointer = NewObject<UCommonFileTool>();
	}

	return GCPointer;
}
