// Fill out your copyright notice in the Description page of Project Settings.


#include "StringTool/MyCommonStringToolFactory.h"
#include "StringTool/CommonStringTool.h"

UCommonStringTool* UMyCommonStringToolFactory::CreateTool()
{
	return NewObject<UCommonStringTool>();
}
