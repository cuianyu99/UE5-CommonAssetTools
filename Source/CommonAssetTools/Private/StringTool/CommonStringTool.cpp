// Fill out your copyright notice in the Description page of Project Settings.


#include "StringTool/CommonStringTool.h"
#include  "OpenSSL/sha.h"
#include <iostream>
#include <string>

using namespace std;

FString UCommonStringTool::AnyStringToSha1(const FString& AnyStr)
{
	string sha1str(TCHAR_TO_UTF8(*AnyStr));
	
	uint8 digest[SHA_DIGEST_LENGTH];
	
	SHA1(reinterpret_cast<uint8*>(sha1str.data()), sha1str.length(), digest);

	string result;

	for (int32 i = 0; i < SHA_DIGEST_LENGTH; ++i)
	{
		char hex[3] = "";
		sprintf_s(hex, "%02x", digest[i]);

		result += hex;
	}
	
	result += '\0';
	
	return result.data();
}
