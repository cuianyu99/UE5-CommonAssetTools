// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "StringTool/MyCommonStringToolFactory.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMyCommonStringToolFactory() {}
// Cross Module References
	COMMONASSETTOOLS_API UClass* Z_Construct_UClass_UCommonAssetToolFactoryBase();
	COMMONASSETTOOLS_API UClass* Z_Construct_UClass_UCommonStringTool_NoRegister();
	COMMONASSETTOOLS_API UClass* Z_Construct_UClass_UMyCommonStringToolFactory();
	COMMONASSETTOOLS_API UClass* Z_Construct_UClass_UMyCommonStringToolFactory_NoRegister();
	UPackage* Z_Construct_UPackage__Script_CommonAssetTools();
// End Cross Module References
	DEFINE_FUNCTION(UMyCommonStringToolFactory::execCreateTool)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UCommonStringTool**)Z_Param__Result=P_THIS->CreateTool();
		P_NATIVE_END;
	}
	void UMyCommonStringToolFactory::StaticRegisterNativesUMyCommonStringToolFactory()
	{
		UClass* Class = UMyCommonStringToolFactory::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CreateTool", &UMyCommonStringToolFactory::execCreateTool },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMyCommonStringToolFactory_CreateTool_Statics
	{
		struct MyCommonStringToolFactory_eventCreateTool_Parms
		{
			UCommonStringTool* ReturnValue;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMyCommonStringToolFactory_CreateTool_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(MyCommonStringToolFactory_eventCreateTool_Parms, ReturnValue), Z_Construct_UClass_UCommonStringTool_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMyCommonStringToolFactory_CreateTool_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMyCommonStringToolFactory_CreateTool_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMyCommonStringToolFactory_CreateTool_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonAssetTool" },
		{ "ModuleRelativePath", "Public/StringTool/MyCommonStringToolFactory.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UMyCommonStringToolFactory_CreateTool_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMyCommonStringToolFactory, nullptr, "CreateTool", nullptr, nullptr, sizeof(Z_Construct_UFunction_UMyCommonStringToolFactory_CreateTool_Statics::MyCommonStringToolFactory_eventCreateTool_Parms), Z_Construct_UFunction_UMyCommonStringToolFactory_CreateTool_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMyCommonStringToolFactory_CreateTool_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMyCommonStringToolFactory_CreateTool_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMyCommonStringToolFactory_CreateTool_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMyCommonStringToolFactory_CreateTool()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UMyCommonStringToolFactory_CreateTool_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UMyCommonStringToolFactory);
	UClass* Z_Construct_UClass_UMyCommonStringToolFactory_NoRegister()
	{
		return UMyCommonStringToolFactory::StaticClass();
	}
	struct Z_Construct_UClass_UMyCommonStringToolFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMyCommonStringToolFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCommonAssetToolFactoryBase,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonAssetTools,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMyCommonStringToolFactory_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMyCommonStringToolFactory_CreateTool, "CreateTool" }, // 4091966650
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMyCommonStringToolFactory_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Category", "CommonAssetTool" },
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "StringTool/MyCommonStringToolFactory.h" },
		{ "ModuleRelativePath", "Public/StringTool/MyCommonStringToolFactory.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMyCommonStringToolFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMyCommonStringToolFactory>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UMyCommonStringToolFactory_Statics::ClassParams = {
		&UMyCommonStringToolFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMyCommonStringToolFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMyCommonStringToolFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMyCommonStringToolFactory()
	{
		if (!Z_Registration_Info_UClass_UMyCommonStringToolFactory.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UMyCommonStringToolFactory.OuterSingleton, Z_Construct_UClass_UMyCommonStringToolFactory_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UMyCommonStringToolFactory.OuterSingleton;
	}
	template<> COMMONASSETTOOLS_API UClass* StaticClass<UMyCommonStringToolFactory>()
	{
		return UMyCommonStringToolFactory::StaticClass();
	}
	UMyCommonStringToolFactory::UMyCommonStringToolFactory(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMyCommonStringToolFactory);
	UMyCommonStringToolFactory::~UMyCommonStringToolFactory() {}
	struct Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_StringTool_MyCommonStringToolFactory_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_StringTool_MyCommonStringToolFactory_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UMyCommonStringToolFactory, UMyCommonStringToolFactory::StaticClass, TEXT("UMyCommonStringToolFactory"), &Z_Registration_Info_UClass_UMyCommonStringToolFactory, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UMyCommonStringToolFactory), 4103341422U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_StringTool_MyCommonStringToolFactory_h_1643193275(TEXT("/Script/CommonAssetTools"),
		Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_StringTool_MyCommonStringToolFactory_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_StringTool_MyCommonStringToolFactory_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
