// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FileTool/CommonFileTool.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonFileTool() {}
// Cross Module References
	COMMONASSETTOOLS_API UClass* Z_Construct_UClass_UCommonFileTool();
	COMMONASSETTOOLS_API UClass* Z_Construct_UClass_UCommonFileTool_NoRegister();
	COMMONASSETTOOLS_API UScriptStruct* Z_Construct_UScriptStruct_FFileSuffixType();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_CommonAssetTools();
// End Cross Module References
	static FStructRegistrationInfo Z_Registration_Info_UScriptStruct_FileSuffixType;
class UScriptStruct* FFileSuffixType::StaticStruct()
{
	if (!Z_Registration_Info_UScriptStruct_FileSuffixType.OuterSingleton)
	{
		Z_Registration_Info_UScriptStruct_FileSuffixType.OuterSingleton = GetStaticStruct(Z_Construct_UScriptStruct_FFileSuffixType, (UObject*)Z_Construct_UPackage__Script_CommonAssetTools(), TEXT("FileSuffixType"));
	}
	return Z_Registration_Info_UScriptStruct_FileSuffixType.OuterSingleton;
}
template<> COMMONASSETTOOLS_API UScriptStruct* StaticStruct<FFileSuffixType>()
{
	return FFileSuffixType::StaticStruct();
}
	struct Z_Construct_UScriptStruct_FFileSuffixType_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_SuffixName_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_SuffixName;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_SuffixType_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_SuffixType;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UECodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFileSuffixType_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Category", "CommonAssetTool" },
		{ "ModuleRelativePath", "Public/FileTool/CommonFileTool.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FFileSuffixType_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FFileSuffixType>();
	}
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFileSuffixType_Statics::NewProp_SuffixName_MetaData[] = {
		{ "Category", "CommonAssetTool" },
		{ "ModuleRelativePath", "Public/FileTool/CommonFileTool.h" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FFileSuffixType_Statics::NewProp_SuffixName = { "SuffixName", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FFileSuffixType, SuffixName), METADATA_PARAMS(Z_Construct_UScriptStruct_FFileSuffixType_Statics::NewProp_SuffixName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFileSuffixType_Statics::NewProp_SuffixName_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFileSuffixType_Statics::NewProp_SuffixType_MetaData[] = {
		{ "Category", "CommonAssetTool" },
		{ "ModuleRelativePath", "Public/FileTool/CommonFileTool.h" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FFileSuffixType_Statics::NewProp_SuffixType = { "SuffixType", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FFileSuffixType, SuffixType), METADATA_PARAMS(Z_Construct_UScriptStruct_FFileSuffixType_Statics::NewProp_SuffixType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFileSuffixType_Statics::NewProp_SuffixType_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FFileSuffixType_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFileSuffixType_Statics::NewProp_SuffixName,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFileSuffixType_Statics::NewProp_SuffixType,
	};
	const UECodeGen_Private::FStructParams Z_Construct_UScriptStruct_FFileSuffixType_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CommonAssetTools,
		nullptr,
		&NewStructOps,
		"FileSuffixType",
		sizeof(FFileSuffixType),
		alignof(FFileSuffixType),
		Z_Construct_UScriptStruct_FFileSuffixType_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFileSuffixType_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FFileSuffixType_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFileSuffixType_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FFileSuffixType()
	{
		if (!Z_Registration_Info_UScriptStruct_FileSuffixType.InnerSingleton)
		{
			UECodeGen_Private::ConstructUScriptStruct(Z_Registration_Info_UScriptStruct_FileSuffixType.InnerSingleton, Z_Construct_UScriptStruct_FFileSuffixType_Statics::ReturnStructParams);
		}
		return Z_Registration_Info_UScriptStruct_FileSuffixType.InnerSingleton;
	}
	DEFINE_FUNCTION(UCommonFileTool::execRemoveFilesWithinDirectory)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_TargetDir);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RemoveFilesWithinDirectory(Z_Param_TargetDir);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonFileTool::execIsDirectoryExists)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_DirPath);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsDirectoryExists(Z_Param_DirPath);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonFileTool::execReadFileToString)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_FilePath);
		P_GET_PROPERTY_REF(FStrProperty,Z_Param_Out_FileContent);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ReadFileToString(Z_Param_FilePath,Z_Param_Out_FileContent);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonFileTool::execGetFileName)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_FilePath);
		P_GET_PROPERTY_REF(FStrProperty,Z_Param_Out_FileName);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetFileName(Z_Param_FilePath,Z_Param_Out_FileName);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonFileTool::execCopyFile)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_SrcFile);
		P_GET_PROPERTY(FStrProperty,Z_Param_DestFile);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->CopyFile(Z_Param_SrcFile,Z_Param_DestFile);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonFileTool::execRenameFile)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_OriginalFileName);
		P_GET_PROPERTY(FStrProperty,Z_Param_NewFileName);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RenameFile(Z_Param_OriginalFileName,Z_Param_NewFileName);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonFileTool::execCreateFolder)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_DirPath);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->CreateFolder(Z_Param_DirPath);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonFileTool::execGetFilePathWithinFileDialog)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_DialogTitle);
		P_GET_PROPERTY(FStrProperty,Z_Param_DefaultPath);
		P_GET_TARRAY(FFileSuffixType,Z_Param_SurffixTypes);
		P_GET_UBOOL(Z_Param_bSave);
		P_GET_PROPERTY_REF(FStrProperty,Z_Param_Out_FilePath);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetFilePathWithinFileDialog(Z_Param_DialogTitle,Z_Param_DefaultPath,Z_Param_SurffixTypes,Z_Param_bSave,Z_Param_Out_FilePath);
		P_NATIVE_END;
	}
	void UCommonFileTool::StaticRegisterNativesUCommonFileTool()
	{
		UClass* Class = UCommonFileTool::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CopyFile", &UCommonFileTool::execCopyFile },
			{ "CreateFolder", &UCommonFileTool::execCreateFolder },
			{ "GetFileName", &UCommonFileTool::execGetFileName },
			{ "GetFilePathWithinFileDialog", &UCommonFileTool::execGetFilePathWithinFileDialog },
			{ "IsDirectoryExists", &UCommonFileTool::execIsDirectoryExists },
			{ "ReadFileToString", &UCommonFileTool::execReadFileToString },
			{ "RemoveFilesWithinDirectory", &UCommonFileTool::execRemoveFilesWithinDirectory },
			{ "RenameFile", &UCommonFileTool::execRenameFile },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCommonFileTool_CopyFile_Statics
	{
		struct CommonFileTool_eventCopyFile_Parms
		{
			FString SrcFile;
			FString DestFile;
		};
		static const UECodeGen_Private::FStrPropertyParams NewProp_SrcFile;
		static const UECodeGen_Private::FStrPropertyParams NewProp_DestFile;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UCommonFileTool_CopyFile_Statics::NewProp_SrcFile = { "SrcFile", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonFileTool_eventCopyFile_Parms, SrcFile), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UCommonFileTool_CopyFile_Statics::NewProp_DestFile = { "DestFile", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonFileTool_eventCopyFile_Parms, DestFile), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonFileTool_CopyFile_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonFileTool_CopyFile_Statics::NewProp_SrcFile,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonFileTool_CopyFile_Statics::NewProp_DestFile,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonFileTool_CopyFile_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonAssetTool" },
		{ "ModuleRelativePath", "Public/FileTool/CommonFileTool.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonFileTool_CopyFile_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonFileTool, nullptr, "CopyFile", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCommonFileTool_CopyFile_Statics::CommonFileTool_eventCopyFile_Parms), Z_Construct_UFunction_UCommonFileTool_CopyFile_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonFileTool_CopyFile_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonFileTool_CopyFile_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonFileTool_CopyFile_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonFileTool_CopyFile()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCommonFileTool_CopyFile_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonFileTool_CreateFolder_Statics
	{
		struct CommonFileTool_eventCreateFolder_Parms
		{
			FString DirPath;
		};
		static const UECodeGen_Private::FStrPropertyParams NewProp_DirPath;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UCommonFileTool_CreateFolder_Statics::NewProp_DirPath = { "DirPath", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonFileTool_eventCreateFolder_Parms, DirPath), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonFileTool_CreateFolder_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonFileTool_CreateFolder_Statics::NewProp_DirPath,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonFileTool_CreateFolder_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonAssetTool" },
		{ "ModuleRelativePath", "Public/FileTool/CommonFileTool.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonFileTool_CreateFolder_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonFileTool, nullptr, "CreateFolder", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCommonFileTool_CreateFolder_Statics::CommonFileTool_eventCreateFolder_Parms), Z_Construct_UFunction_UCommonFileTool_CreateFolder_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonFileTool_CreateFolder_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonFileTool_CreateFolder_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonFileTool_CreateFolder_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonFileTool_CreateFolder()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCommonFileTool_CreateFolder_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonFileTool_GetFileName_Statics
	{
		struct CommonFileTool_eventGetFileName_Parms
		{
			FString FilePath;
			FString FileName;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_FilePath_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_FilePath;
		static const UECodeGen_Private::FStrPropertyParams NewProp_FileName;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonFileTool_GetFileName_Statics::NewProp_FilePath_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UCommonFileTool_GetFileName_Statics::NewProp_FilePath = { "FilePath", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonFileTool_eventGetFileName_Parms, FilePath), METADATA_PARAMS(Z_Construct_UFunction_UCommonFileTool_GetFileName_Statics::NewProp_FilePath_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonFileTool_GetFileName_Statics::NewProp_FilePath_MetaData)) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UCommonFileTool_GetFileName_Statics::NewProp_FileName = { "FileName", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonFileTool_eventGetFileName_Parms, FileName), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonFileTool_GetFileName_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonFileTool_GetFileName_Statics::NewProp_FilePath,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonFileTool_GetFileName_Statics::NewProp_FileName,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonFileTool_GetFileName_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonAssetTool" },
		{ "ModuleRelativePath", "Public/FileTool/CommonFileTool.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonFileTool_GetFileName_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonFileTool, nullptr, "GetFileName", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCommonFileTool_GetFileName_Statics::CommonFileTool_eventGetFileName_Parms), Z_Construct_UFunction_UCommonFileTool_GetFileName_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonFileTool_GetFileName_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonFileTool_GetFileName_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonFileTool_GetFileName_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonFileTool_GetFileName()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCommonFileTool_GetFileName_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonFileTool_GetFilePathWithinFileDialog_Statics
	{
		struct CommonFileTool_eventGetFilePathWithinFileDialog_Parms
		{
			FString DialogTitle;
			FString DefaultPath;
			TArray<FFileSuffixType> SurffixTypes;
			bool bSave;
			FString FilePath;
		};
		static const UECodeGen_Private::FStrPropertyParams NewProp_DialogTitle;
		static const UECodeGen_Private::FStrPropertyParams NewProp_DefaultPath;
		static const UECodeGen_Private::FStructPropertyParams NewProp_SurffixTypes_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_SurffixTypes;
		static void NewProp_bSave_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_bSave;
		static const UECodeGen_Private::FStrPropertyParams NewProp_FilePath;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UCommonFileTool_GetFilePathWithinFileDialog_Statics::NewProp_DialogTitle = { "DialogTitle", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonFileTool_eventGetFilePathWithinFileDialog_Parms, DialogTitle), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UCommonFileTool_GetFilePathWithinFileDialog_Statics::NewProp_DefaultPath = { "DefaultPath", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonFileTool_eventGetFilePathWithinFileDialog_Parms, DefaultPath), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UCommonFileTool_GetFilePathWithinFileDialog_Statics::NewProp_SurffixTypes_Inner = { "SurffixTypes", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UScriptStruct_FFileSuffixType, METADATA_PARAMS(nullptr, 0) }; // 1870699063
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UCommonFileTool_GetFilePathWithinFileDialog_Statics::NewProp_SurffixTypes = { "SurffixTypes", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonFileTool_eventGetFilePathWithinFileDialog_Parms, SurffixTypes), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) }; // 1870699063
	void Z_Construct_UFunction_UCommonFileTool_GetFilePathWithinFileDialog_Statics::NewProp_bSave_SetBit(void* Obj)
	{
		((CommonFileTool_eventGetFilePathWithinFileDialog_Parms*)Obj)->bSave = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCommonFileTool_GetFilePathWithinFileDialog_Statics::NewProp_bSave = { "bSave", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(CommonFileTool_eventGetFilePathWithinFileDialog_Parms), &Z_Construct_UFunction_UCommonFileTool_GetFilePathWithinFileDialog_Statics::NewProp_bSave_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UCommonFileTool_GetFilePathWithinFileDialog_Statics::NewProp_FilePath = { "FilePath", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonFileTool_eventGetFilePathWithinFileDialog_Parms, FilePath), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonFileTool_GetFilePathWithinFileDialog_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonFileTool_GetFilePathWithinFileDialog_Statics::NewProp_DialogTitle,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonFileTool_GetFilePathWithinFileDialog_Statics::NewProp_DefaultPath,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonFileTool_GetFilePathWithinFileDialog_Statics::NewProp_SurffixTypes_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonFileTool_GetFilePathWithinFileDialog_Statics::NewProp_SurffixTypes,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonFileTool_GetFilePathWithinFileDialog_Statics::NewProp_bSave,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonFileTool_GetFilePathWithinFileDialog_Statics::NewProp_FilePath,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonFileTool_GetFilePathWithinFileDialog_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonAssetTool" },
		{ "ModuleRelativePath", "Public/FileTool/CommonFileTool.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonFileTool_GetFilePathWithinFileDialog_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonFileTool, nullptr, "GetFilePathWithinFileDialog", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCommonFileTool_GetFilePathWithinFileDialog_Statics::CommonFileTool_eventGetFilePathWithinFileDialog_Parms), Z_Construct_UFunction_UCommonFileTool_GetFilePathWithinFileDialog_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonFileTool_GetFilePathWithinFileDialog_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonFileTool_GetFilePathWithinFileDialog_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonFileTool_GetFilePathWithinFileDialog_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonFileTool_GetFilePathWithinFileDialog()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCommonFileTool_GetFilePathWithinFileDialog_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonFileTool_IsDirectoryExists_Statics
	{
		struct CommonFileTool_eventIsDirectoryExists_Parms
		{
			FString DirPath;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_DirPath_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_DirPath;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonFileTool_IsDirectoryExists_Statics::NewProp_DirPath_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UCommonFileTool_IsDirectoryExists_Statics::NewProp_DirPath = { "DirPath", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonFileTool_eventIsDirectoryExists_Parms, DirPath), METADATA_PARAMS(Z_Construct_UFunction_UCommonFileTool_IsDirectoryExists_Statics::NewProp_DirPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonFileTool_IsDirectoryExists_Statics::NewProp_DirPath_MetaData)) };
	void Z_Construct_UFunction_UCommonFileTool_IsDirectoryExists_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((CommonFileTool_eventIsDirectoryExists_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCommonFileTool_IsDirectoryExists_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(CommonFileTool_eventIsDirectoryExists_Parms), &Z_Construct_UFunction_UCommonFileTool_IsDirectoryExists_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonFileTool_IsDirectoryExists_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonFileTool_IsDirectoryExists_Statics::NewProp_DirPath,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonFileTool_IsDirectoryExists_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonFileTool_IsDirectoryExists_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonAssetTool" },
		{ "ModuleRelativePath", "Public/FileTool/CommonFileTool.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonFileTool_IsDirectoryExists_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonFileTool, nullptr, "IsDirectoryExists", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCommonFileTool_IsDirectoryExists_Statics::CommonFileTool_eventIsDirectoryExists_Parms), Z_Construct_UFunction_UCommonFileTool_IsDirectoryExists_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonFileTool_IsDirectoryExists_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonFileTool_IsDirectoryExists_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonFileTool_IsDirectoryExists_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonFileTool_IsDirectoryExists()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCommonFileTool_IsDirectoryExists_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonFileTool_ReadFileToString_Statics
	{
		struct CommonFileTool_eventReadFileToString_Parms
		{
			FString FilePath;
			FString FileContent;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_FilePath_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_FilePath;
		static const UECodeGen_Private::FStrPropertyParams NewProp_FileContent;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonFileTool_ReadFileToString_Statics::NewProp_FilePath_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UCommonFileTool_ReadFileToString_Statics::NewProp_FilePath = { "FilePath", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonFileTool_eventReadFileToString_Parms, FilePath), METADATA_PARAMS(Z_Construct_UFunction_UCommonFileTool_ReadFileToString_Statics::NewProp_FilePath_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonFileTool_ReadFileToString_Statics::NewProp_FilePath_MetaData)) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UCommonFileTool_ReadFileToString_Statics::NewProp_FileContent = { "FileContent", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonFileTool_eventReadFileToString_Parms, FileContent), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonFileTool_ReadFileToString_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonFileTool_ReadFileToString_Statics::NewProp_FilePath,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonFileTool_ReadFileToString_Statics::NewProp_FileContent,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonFileTool_ReadFileToString_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonAssetTool" },
		{ "ModuleRelativePath", "Public/FileTool/CommonFileTool.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonFileTool_ReadFileToString_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonFileTool, nullptr, "ReadFileToString", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCommonFileTool_ReadFileToString_Statics::CommonFileTool_eventReadFileToString_Parms), Z_Construct_UFunction_UCommonFileTool_ReadFileToString_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonFileTool_ReadFileToString_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonFileTool_ReadFileToString_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonFileTool_ReadFileToString_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonFileTool_ReadFileToString()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCommonFileTool_ReadFileToString_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonFileTool_RemoveFilesWithinDirectory_Statics
	{
		struct CommonFileTool_eventRemoveFilesWithinDirectory_Parms
		{
			FString TargetDir;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_TargetDir_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_TargetDir;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonFileTool_RemoveFilesWithinDirectory_Statics::NewProp_TargetDir_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UCommonFileTool_RemoveFilesWithinDirectory_Statics::NewProp_TargetDir = { "TargetDir", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonFileTool_eventRemoveFilesWithinDirectory_Parms, TargetDir), METADATA_PARAMS(Z_Construct_UFunction_UCommonFileTool_RemoveFilesWithinDirectory_Statics::NewProp_TargetDir_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonFileTool_RemoveFilesWithinDirectory_Statics::NewProp_TargetDir_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonFileTool_RemoveFilesWithinDirectory_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonFileTool_RemoveFilesWithinDirectory_Statics::NewProp_TargetDir,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonFileTool_RemoveFilesWithinDirectory_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonAssetTool" },
		{ "ModuleRelativePath", "Public/FileTool/CommonFileTool.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonFileTool_RemoveFilesWithinDirectory_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonFileTool, nullptr, "RemoveFilesWithinDirectory", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCommonFileTool_RemoveFilesWithinDirectory_Statics::CommonFileTool_eventRemoveFilesWithinDirectory_Parms), Z_Construct_UFunction_UCommonFileTool_RemoveFilesWithinDirectory_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonFileTool_RemoveFilesWithinDirectory_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonFileTool_RemoveFilesWithinDirectory_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonFileTool_RemoveFilesWithinDirectory_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonFileTool_RemoveFilesWithinDirectory()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCommonFileTool_RemoveFilesWithinDirectory_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonFileTool_RenameFile_Statics
	{
		struct CommonFileTool_eventRenameFile_Parms
		{
			FString OriginalFileName;
			FString NewFileName;
		};
		static const UECodeGen_Private::FStrPropertyParams NewProp_OriginalFileName;
		static const UECodeGen_Private::FStrPropertyParams NewProp_NewFileName;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UCommonFileTool_RenameFile_Statics::NewProp_OriginalFileName = { "OriginalFileName", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonFileTool_eventRenameFile_Parms, OriginalFileName), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UCommonFileTool_RenameFile_Statics::NewProp_NewFileName = { "NewFileName", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonFileTool_eventRenameFile_Parms, NewFileName), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonFileTool_RenameFile_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonFileTool_RenameFile_Statics::NewProp_OriginalFileName,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonFileTool_RenameFile_Statics::NewProp_NewFileName,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonFileTool_RenameFile_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonAssetTool" },
		{ "ModuleRelativePath", "Public/FileTool/CommonFileTool.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonFileTool_RenameFile_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonFileTool, nullptr, "RenameFile", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCommonFileTool_RenameFile_Statics::CommonFileTool_eventRenameFile_Parms), Z_Construct_UFunction_UCommonFileTool_RenameFile_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonFileTool_RenameFile_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonFileTool_RenameFile_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonFileTool_RenameFile_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonFileTool_RenameFile()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCommonFileTool_RenameFile_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UCommonFileTool);
	UClass* Z_Construct_UClass_UCommonFileTool_NoRegister()
	{
		return UCommonFileTool::StaticClass();
	}
	struct Z_Construct_UClass_UCommonFileTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonFileTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonAssetTools,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCommonFileTool_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCommonFileTool_CopyFile, "CopyFile" }, // 56904515
		{ &Z_Construct_UFunction_UCommonFileTool_CreateFolder, "CreateFolder" }, // 1553121127
		{ &Z_Construct_UFunction_UCommonFileTool_GetFileName, "GetFileName" }, // 315492687
		{ &Z_Construct_UFunction_UCommonFileTool_GetFilePathWithinFileDialog, "GetFilePathWithinFileDialog" }, // 3105594039
		{ &Z_Construct_UFunction_UCommonFileTool_IsDirectoryExists, "IsDirectoryExists" }, // 2273024173
		{ &Z_Construct_UFunction_UCommonFileTool_ReadFileToString, "ReadFileToString" }, // 3162405390
		{ &Z_Construct_UFunction_UCommonFileTool_RemoveFilesWithinDirectory, "RemoveFilesWithinDirectory" }, // 1168825245
		{ &Z_Construct_UFunction_UCommonFileTool_RenameFile, "RenameFile" }, // 2671281349
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonFileTool_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Category", "CommonAssetTool" },
		{ "IncludePath", "FileTool/CommonFileTool.h" },
		{ "ModuleRelativePath", "Public/FileTool/CommonFileTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonFileTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonFileTool>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UCommonFileTool_Statics::ClassParams = {
		&UCommonFileTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonFileTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonFileTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonFileTool()
	{
		if (!Z_Registration_Info_UClass_UCommonFileTool.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UCommonFileTool.OuterSingleton, Z_Construct_UClass_UCommonFileTool_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UCommonFileTool.OuterSingleton;
	}
	template<> COMMONASSETTOOLS_API UClass* StaticClass<UCommonFileTool>()
	{
		return UCommonFileTool::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonFileTool);
	struct Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_FileTool_CommonFileTool_h_Statics
	{
		static const FStructRegisterCompiledInInfo ScriptStructInfo[];
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FStructRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_FileTool_CommonFileTool_h_Statics::ScriptStructInfo[] = {
		{ FFileSuffixType::StaticStruct, Z_Construct_UScriptStruct_FFileSuffixType_Statics::NewStructOps, TEXT("FileSuffixType"), &Z_Registration_Info_UScriptStruct_FileSuffixType, CONSTRUCT_RELOAD_VERSION_INFO(FStructReloadVersionInfo, sizeof(FFileSuffixType), 1870699063U) },
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_FileTool_CommonFileTool_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UCommonFileTool, UCommonFileTool::StaticClass, TEXT("UCommonFileTool"), &Z_Registration_Info_UClass_UCommonFileTool, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UCommonFileTool), 3804322123U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_FileTool_CommonFileTool_h_1851059139(TEXT("/Script/CommonAssetTools"),
		Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_FileTool_CommonFileTool_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_FileTool_CommonFileTool_h_Statics::ClassInfo),
		Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_FileTool_CommonFileTool_h_Statics::ScriptStructInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_FileTool_CommonFileTool_h_Statics::ScriptStructInfo),
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
