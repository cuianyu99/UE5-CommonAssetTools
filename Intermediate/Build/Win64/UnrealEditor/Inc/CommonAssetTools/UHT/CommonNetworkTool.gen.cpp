// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NetworkTool/CommonNetworkTool.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonNetworkTool() {}
// Cross Module References
	COMMONASSETTOOLS_API UClass* Z_Construct_UClass_UCommonNetworkTool();
	COMMONASSETTOOLS_API UClass* Z_Construct_UClass_UCommonNetworkTool_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_CommonAssetTools();
// End Cross Module References
	DEFINE_FUNCTION(UCommonNetworkTool::execGetApiResult)
	{
		P_GET_PROPERTY_REF(FStrProperty,Z_Param_Out_ResultContent);
		P_GET_PROPERTY_REF(FIntProperty,Z_Param_Out_APIState);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetApiResult(Z_Param_Out_ResultContent,Z_Param_Out_APIState);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonNetworkTool::execRunApi)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_APIAddress);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RunApi(Z_Param_APIAddress);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonNetworkTool::execIsNetworkConnected)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsNetworkConnected();
		P_NATIVE_END;
	}
	void UCommonNetworkTool::StaticRegisterNativesUCommonNetworkTool()
	{
		UClass* Class = UCommonNetworkTool::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetApiResult", &UCommonNetworkTool::execGetApiResult },
			{ "IsNetworkConnected", &UCommonNetworkTool::execIsNetworkConnected },
			{ "RunApi", &UCommonNetworkTool::execRunApi },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCommonNetworkTool_GetApiResult_Statics
	{
		struct CommonNetworkTool_eventGetApiResult_Parms
		{
			FString ResultContent;
			int32 APIState;
		};
		static const UECodeGen_Private::FStrPropertyParams NewProp_ResultContent;
		static const UECodeGen_Private::FIntPropertyParams NewProp_APIState;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UCommonNetworkTool_GetApiResult_Statics::NewProp_ResultContent = { "ResultContent", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonNetworkTool_eventGetApiResult_Parms, ResultContent), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UCommonNetworkTool_GetApiResult_Statics::NewProp_APIState = { "APIState", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonNetworkTool_eventGetApiResult_Parms, APIState), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonNetworkTool_GetApiResult_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonNetworkTool_GetApiResult_Statics::NewProp_ResultContent,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonNetworkTool_GetApiResult_Statics::NewProp_APIState,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonNetworkTool_GetApiResult_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonAssetTool" },
		{ "ModuleRelativePath", "Public/NetworkTool/CommonNetworkTool.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonNetworkTool_GetApiResult_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonNetworkTool, nullptr, "GetApiResult", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCommonNetworkTool_GetApiResult_Statics::CommonNetworkTool_eventGetApiResult_Parms), Z_Construct_UFunction_UCommonNetworkTool_GetApiResult_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonNetworkTool_GetApiResult_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonNetworkTool_GetApiResult_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonNetworkTool_GetApiResult_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonNetworkTool_GetApiResult()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCommonNetworkTool_GetApiResult_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonNetworkTool_IsNetworkConnected_Statics
	{
		struct CommonNetworkTool_eventIsNetworkConnected_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UCommonNetworkTool_IsNetworkConnected_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((CommonNetworkTool_eventIsNetworkConnected_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCommonNetworkTool_IsNetworkConnected_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(CommonNetworkTool_eventIsNetworkConnected_Parms), &Z_Construct_UFunction_UCommonNetworkTool_IsNetworkConnected_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonNetworkTool_IsNetworkConnected_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonNetworkTool_IsNetworkConnected_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonNetworkTool_IsNetworkConnected_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonAssetTool" },
		{ "ModuleRelativePath", "Public/NetworkTool/CommonNetworkTool.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonNetworkTool_IsNetworkConnected_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonNetworkTool, nullptr, "IsNetworkConnected", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCommonNetworkTool_IsNetworkConnected_Statics::CommonNetworkTool_eventIsNetworkConnected_Parms), Z_Construct_UFunction_UCommonNetworkTool_IsNetworkConnected_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonNetworkTool_IsNetworkConnected_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonNetworkTool_IsNetworkConnected_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonNetworkTool_IsNetworkConnected_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonNetworkTool_IsNetworkConnected()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCommonNetworkTool_IsNetworkConnected_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonNetworkTool_RunApi_Statics
	{
		struct CommonNetworkTool_eventRunApi_Parms
		{
			FString APIAddress;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_APIAddress_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_APIAddress;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonNetworkTool_RunApi_Statics::NewProp_APIAddress_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UCommonNetworkTool_RunApi_Statics::NewProp_APIAddress = { "APIAddress", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonNetworkTool_eventRunApi_Parms, APIAddress), METADATA_PARAMS(Z_Construct_UFunction_UCommonNetworkTool_RunApi_Statics::NewProp_APIAddress_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonNetworkTool_RunApi_Statics::NewProp_APIAddress_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonNetworkTool_RunApi_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonNetworkTool_RunApi_Statics::NewProp_APIAddress,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonNetworkTool_RunApi_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonAssetTool" },
		{ "ModuleRelativePath", "Public/NetworkTool/CommonNetworkTool.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonNetworkTool_RunApi_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonNetworkTool, nullptr, "RunApi", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCommonNetworkTool_RunApi_Statics::CommonNetworkTool_eventRunApi_Parms), Z_Construct_UFunction_UCommonNetworkTool_RunApi_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonNetworkTool_RunApi_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonNetworkTool_RunApi_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonNetworkTool_RunApi_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonNetworkTool_RunApi()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCommonNetworkTool_RunApi_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UCommonNetworkTool);
	UClass* Z_Construct_UClass_UCommonNetworkTool_NoRegister()
	{
		return UCommonNetworkTool::StaticClass();
	}
	struct Z_Construct_UClass_UCommonNetworkTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonNetworkTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonAssetTools,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCommonNetworkTool_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCommonNetworkTool_GetApiResult, "GetApiResult" }, // 70059156
		{ &Z_Construct_UFunction_UCommonNetworkTool_IsNetworkConnected, "IsNetworkConnected" }, // 2092473996
		{ &Z_Construct_UFunction_UCommonNetworkTool_RunApi, "RunApi" }, // 3501291075
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonNetworkTool_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Category", "CommonAssetTool" },
		{ "IncludePath", "NetworkTool/CommonNetworkTool.h" },
		{ "ModuleRelativePath", "Public/NetworkTool/CommonNetworkTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonNetworkTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonNetworkTool>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UCommonNetworkTool_Statics::ClassParams = {
		&UCommonNetworkTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonNetworkTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonNetworkTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonNetworkTool()
	{
		if (!Z_Registration_Info_UClass_UCommonNetworkTool.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UCommonNetworkTool.OuterSingleton, Z_Construct_UClass_UCommonNetworkTool_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UCommonNetworkTool.OuterSingleton;
	}
	template<> COMMONASSETTOOLS_API UClass* StaticClass<UCommonNetworkTool>()
	{
		return UCommonNetworkTool::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonNetworkTool);
	UCommonNetworkTool::~UCommonNetworkTool() {}
	struct Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_NetworkTool_CommonNetworkTool_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_NetworkTool_CommonNetworkTool_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UCommonNetworkTool, UCommonNetworkTool::StaticClass, TEXT("UCommonNetworkTool"), &Z_Registration_Info_UClass_UCommonNetworkTool, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UCommonNetworkTool), 45304314U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_NetworkTool_CommonNetworkTool_h_4027088309(TEXT("/Script/CommonAssetTools"),
		Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_NetworkTool_CommonNetworkTool_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_NetworkTool_CommonNetworkTool_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
