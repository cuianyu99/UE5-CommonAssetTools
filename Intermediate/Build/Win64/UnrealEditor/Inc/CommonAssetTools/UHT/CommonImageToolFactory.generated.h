// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "ImageTool/CommonImageToolFactory.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UCommonImageTool;
#ifdef COMMONASSETTOOLS_CommonImageToolFactory_generated_h
#error "CommonImageToolFactory.generated.h already included, missing '#pragma once' in CommonImageToolFactory.h"
#endif
#define COMMONASSETTOOLS_CommonImageToolFactory_generated_h

#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageToolFactory_h_15_SPARSE_DATA
#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageToolFactory_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execCreateTool);


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageToolFactory_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execCreateTool);


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageToolFactory_h_15_ACCESSORS
#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageToolFactory_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonImageToolFactory(); \
	friend struct Z_Construct_UClass_UCommonImageToolFactory_Statics; \
public: \
	DECLARE_CLASS(UCommonImageToolFactory, UCommonAssetToolFactoryBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonAssetTools"), NO_API) \
	DECLARE_SERIALIZER(UCommonImageToolFactory)


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageToolFactory_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUCommonImageToolFactory(); \
	friend struct Z_Construct_UClass_UCommonImageToolFactory_Statics; \
public: \
	DECLARE_CLASS(UCommonImageToolFactory, UCommonAssetToolFactoryBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonAssetTools"), NO_API) \
	DECLARE_SERIALIZER(UCommonImageToolFactory)


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageToolFactory_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonImageToolFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonImageToolFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonImageToolFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonImageToolFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonImageToolFactory(UCommonImageToolFactory&&); \
	NO_API UCommonImageToolFactory(const UCommonImageToolFactory&); \
public: \
	NO_API virtual ~UCommonImageToolFactory();


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageToolFactory_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonImageToolFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonImageToolFactory(UCommonImageToolFactory&&); \
	NO_API UCommonImageToolFactory(const UCommonImageToolFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonImageToolFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonImageToolFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonImageToolFactory) \
	NO_API virtual ~UCommonImageToolFactory();


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageToolFactory_h_12_PROLOG
#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageToolFactory_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageToolFactory_h_15_SPARSE_DATA \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageToolFactory_h_15_RPC_WRAPPERS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageToolFactory_h_15_ACCESSORS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageToolFactory_h_15_INCLASS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageToolFactory_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageToolFactory_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageToolFactory_h_15_SPARSE_DATA \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageToolFactory_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageToolFactory_h_15_ACCESSORS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageToolFactory_h_15_INCLASS_NO_PURE_DECLS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageToolFactory_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONASSETTOOLS_API UClass* StaticClass<class UCommonImageToolFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageToolFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
