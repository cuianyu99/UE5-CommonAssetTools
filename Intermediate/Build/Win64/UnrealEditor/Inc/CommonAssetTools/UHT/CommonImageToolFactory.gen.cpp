// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ImageTool/CommonImageToolFactory.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonImageToolFactory() {}
// Cross Module References
	COMMONASSETTOOLS_API UClass* Z_Construct_UClass_UCommonAssetToolFactoryBase();
	COMMONASSETTOOLS_API UClass* Z_Construct_UClass_UCommonImageTool_NoRegister();
	COMMONASSETTOOLS_API UClass* Z_Construct_UClass_UCommonImageToolFactory();
	COMMONASSETTOOLS_API UClass* Z_Construct_UClass_UCommonImageToolFactory_NoRegister();
	UPackage* Z_Construct_UPackage__Script_CommonAssetTools();
// End Cross Module References
	DEFINE_FUNCTION(UCommonImageToolFactory::execCreateTool)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UCommonImageTool**)Z_Param__Result=P_THIS->CreateTool();
		P_NATIVE_END;
	}
	void UCommonImageToolFactory::StaticRegisterNativesUCommonImageToolFactory()
	{
		UClass* Class = UCommonImageToolFactory::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CreateTool", &UCommonImageToolFactory::execCreateTool },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCommonImageToolFactory_CreateTool_Statics
	{
		struct CommonImageToolFactory_eventCreateTool_Parms
		{
			UCommonImageTool* ReturnValue;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCommonImageToolFactory_CreateTool_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonImageToolFactory_eventCreateTool_Parms, ReturnValue), Z_Construct_UClass_UCommonImageTool_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonImageToolFactory_CreateTool_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonImageToolFactory_CreateTool_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonImageToolFactory_CreateTool_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonAssetTool" },
		{ "ModuleRelativePath", "Public/ImageTool/CommonImageToolFactory.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonImageToolFactory_CreateTool_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonImageToolFactory, nullptr, "CreateTool", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCommonImageToolFactory_CreateTool_Statics::CommonImageToolFactory_eventCreateTool_Parms), Z_Construct_UFunction_UCommonImageToolFactory_CreateTool_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonImageToolFactory_CreateTool_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonImageToolFactory_CreateTool_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonImageToolFactory_CreateTool_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonImageToolFactory_CreateTool()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCommonImageToolFactory_CreateTool_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UCommonImageToolFactory);
	UClass* Z_Construct_UClass_UCommonImageToolFactory_NoRegister()
	{
		return UCommonImageToolFactory::StaticClass();
	}
	struct Z_Construct_UClass_UCommonImageToolFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_GCPointer_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_GCPointer;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonImageToolFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCommonAssetToolFactoryBase,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonAssetTools,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCommonImageToolFactory_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCommonImageToolFactory_CreateTool, "CreateTool" }, // 3098755291
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonImageToolFactory_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Category", "CommonAssetTool" },
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "ImageTool/CommonImageToolFactory.h" },
		{ "ModuleRelativePath", "Public/ImageTool/CommonImageToolFactory.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonImageToolFactory_Statics::NewProp_GCPointer_MetaData[] = {
		{ "ModuleRelativePath", "Public/ImageTool/CommonImageToolFactory.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UCommonImageToolFactory_Statics::NewProp_GCPointer = { "GCPointer", nullptr, (EPropertyFlags)0x0010000000000000, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UCommonImageToolFactory, GCPointer), Z_Construct_UClass_UCommonImageTool_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCommonImageToolFactory_Statics::NewProp_GCPointer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonImageToolFactory_Statics::NewProp_GCPointer_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCommonImageToolFactory_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonImageToolFactory_Statics::NewProp_GCPointer,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonImageToolFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonImageToolFactory>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UCommonImageToolFactory_Statics::ClassParams = {
		&UCommonImageToolFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UCommonImageToolFactory_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UCommonImageToolFactory_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonImageToolFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonImageToolFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonImageToolFactory()
	{
		if (!Z_Registration_Info_UClass_UCommonImageToolFactory.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UCommonImageToolFactory.OuterSingleton, Z_Construct_UClass_UCommonImageToolFactory_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UCommonImageToolFactory.OuterSingleton;
	}
	template<> COMMONASSETTOOLS_API UClass* StaticClass<UCommonImageToolFactory>()
	{
		return UCommonImageToolFactory::StaticClass();
	}
	UCommonImageToolFactory::UCommonImageToolFactory(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonImageToolFactory);
	UCommonImageToolFactory::~UCommonImageToolFactory() {}
	struct Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageToolFactory_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageToolFactory_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UCommonImageToolFactory, UCommonImageToolFactory::StaticClass, TEXT("UCommonImageToolFactory"), &Z_Registration_Info_UClass_UCommonImageToolFactory, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UCommonImageToolFactory), 3242437925U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageToolFactory_h_2498504206(TEXT("/Script/CommonAssetTools"),
		Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageToolFactory_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageToolFactory_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
