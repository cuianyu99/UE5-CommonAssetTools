// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "CommonAssetToolFactoryBase.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMMONASSETTOOLS_CommonAssetToolFactoryBase_generated_h
#error "CommonAssetToolFactoryBase.generated.h already included, missing '#pragma once' in CommonAssetToolFactoryBase.h"
#endif
#define COMMONASSETTOOLS_CommonAssetToolFactoryBase_generated_h

#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolFactoryBase_h_15_SPARSE_DATA
#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolFactoryBase_h_15_RPC_WRAPPERS
#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolFactoryBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolFactoryBase_h_15_ACCESSORS
#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolFactoryBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonAssetToolFactoryBase(); \
	friend struct Z_Construct_UClass_UCommonAssetToolFactoryBase_Statics; \
public: \
	DECLARE_CLASS(UCommonAssetToolFactoryBase, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CommonAssetTools"), NO_API) \
	DECLARE_SERIALIZER(UCommonAssetToolFactoryBase)


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolFactoryBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUCommonAssetToolFactoryBase(); \
	friend struct Z_Construct_UClass_UCommonAssetToolFactoryBase_Statics; \
public: \
	DECLARE_CLASS(UCommonAssetToolFactoryBase, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CommonAssetTools"), NO_API) \
	DECLARE_SERIALIZER(UCommonAssetToolFactoryBase)


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolFactoryBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonAssetToolFactoryBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonAssetToolFactoryBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonAssetToolFactoryBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonAssetToolFactoryBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonAssetToolFactoryBase(UCommonAssetToolFactoryBase&&); \
	NO_API UCommonAssetToolFactoryBase(const UCommonAssetToolFactoryBase&); \
public: \
	NO_API virtual ~UCommonAssetToolFactoryBase();


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolFactoryBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonAssetToolFactoryBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonAssetToolFactoryBase(UCommonAssetToolFactoryBase&&); \
	NO_API UCommonAssetToolFactoryBase(const UCommonAssetToolFactoryBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonAssetToolFactoryBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonAssetToolFactoryBase); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonAssetToolFactoryBase) \
	NO_API virtual ~UCommonAssetToolFactoryBase();


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolFactoryBase_h_12_PROLOG
#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolFactoryBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolFactoryBase_h_15_SPARSE_DATA \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolFactoryBase_h_15_RPC_WRAPPERS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolFactoryBase_h_15_ACCESSORS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolFactoryBase_h_15_INCLASS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolFactoryBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolFactoryBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolFactoryBase_h_15_SPARSE_DATA \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolFactoryBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolFactoryBase_h_15_ACCESSORS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolFactoryBase_h_15_INCLASS_NO_PURE_DECLS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolFactoryBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONASSETTOOLS_API UClass* StaticClass<class UCommonAssetToolFactoryBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolFactoryBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
