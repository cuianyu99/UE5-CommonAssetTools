// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TimeTool/CommonTimeTool.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonTimeTool() {}
// Cross Module References
	COMMONASSETTOOLS_API UClass* Z_Construct_UClass_UCommonTimeTool();
	COMMONASSETTOOLS_API UClass* Z_Construct_UClass_UCommonTimeTool_NoRegister();
	COMMONASSETTOOLS_API UScriptStruct* Z_Construct_UScriptStruct_FDateTimeGetter();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_CommonAssetTools();
// End Cross Module References
	static FStructRegistrationInfo Z_Registration_Info_UScriptStruct_DateTimeGetter;
class UScriptStruct* FDateTimeGetter::StaticStruct()
{
	if (!Z_Registration_Info_UScriptStruct_DateTimeGetter.OuterSingleton)
	{
		Z_Registration_Info_UScriptStruct_DateTimeGetter.OuterSingleton = GetStaticStruct(Z_Construct_UScriptStruct_FDateTimeGetter, (UObject*)Z_Construct_UPackage__Script_CommonAssetTools(), TEXT("DateTimeGetter"));
	}
	return Z_Registration_Info_UScriptStruct_DateTimeGetter.OuterSingleton;
}
template<> COMMONASSETTOOLS_API UScriptStruct* StaticStruct<FDateTimeGetter>()
{
	return FDateTimeGetter::StaticStruct();
}
	struct Z_Construct_UScriptStruct_FDateTimeGetter_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Year_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_Year;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Month_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_Month;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Day_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_Day;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Hour_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_Hour;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Minute_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_Minute;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Second_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_Second;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_IsAM_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_IsAM;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UECodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDateTimeGetter_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Category", "CommonAssetTool" },
		{ "ModuleRelativePath", "Public/TimeTool/CommonTimeTool.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDateTimeGetter_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDateTimeGetter>();
	}
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDateTimeGetter_Statics::NewProp_Year_MetaData[] = {
		{ "Category", "CommonAssetTool" },
		{ "ModuleRelativePath", "Public/TimeTool/CommonTimeTool.h" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDateTimeGetter_Statics::NewProp_Year = { "Year", nullptr, (EPropertyFlags)0x0010000000000004, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FDateTimeGetter, Year), METADATA_PARAMS(Z_Construct_UScriptStruct_FDateTimeGetter_Statics::NewProp_Year_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDateTimeGetter_Statics::NewProp_Year_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDateTimeGetter_Statics::NewProp_Month_MetaData[] = {
		{ "Category", "CommonAssetTool" },
		{ "ModuleRelativePath", "Public/TimeTool/CommonTimeTool.h" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDateTimeGetter_Statics::NewProp_Month = { "Month", nullptr, (EPropertyFlags)0x0010000000000004, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FDateTimeGetter, Month), METADATA_PARAMS(Z_Construct_UScriptStruct_FDateTimeGetter_Statics::NewProp_Month_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDateTimeGetter_Statics::NewProp_Month_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDateTimeGetter_Statics::NewProp_Day_MetaData[] = {
		{ "Category", "CommonAssetTool" },
		{ "ModuleRelativePath", "Public/TimeTool/CommonTimeTool.h" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDateTimeGetter_Statics::NewProp_Day = { "Day", nullptr, (EPropertyFlags)0x0010000000000004, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FDateTimeGetter, Day), METADATA_PARAMS(Z_Construct_UScriptStruct_FDateTimeGetter_Statics::NewProp_Day_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDateTimeGetter_Statics::NewProp_Day_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDateTimeGetter_Statics::NewProp_Hour_MetaData[] = {
		{ "Category", "CommonAssetTool" },
		{ "ModuleRelativePath", "Public/TimeTool/CommonTimeTool.h" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDateTimeGetter_Statics::NewProp_Hour = { "Hour", nullptr, (EPropertyFlags)0x0010000000000004, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FDateTimeGetter, Hour), METADATA_PARAMS(Z_Construct_UScriptStruct_FDateTimeGetter_Statics::NewProp_Hour_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDateTimeGetter_Statics::NewProp_Hour_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDateTimeGetter_Statics::NewProp_Minute_MetaData[] = {
		{ "Category", "CommonAssetTool" },
		{ "ModuleRelativePath", "Public/TimeTool/CommonTimeTool.h" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDateTimeGetter_Statics::NewProp_Minute = { "Minute", nullptr, (EPropertyFlags)0x0010000000000004, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FDateTimeGetter, Minute), METADATA_PARAMS(Z_Construct_UScriptStruct_FDateTimeGetter_Statics::NewProp_Minute_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDateTimeGetter_Statics::NewProp_Minute_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDateTimeGetter_Statics::NewProp_Second_MetaData[] = {
		{ "Category", "CommonAssetTool" },
		{ "ModuleRelativePath", "Public/TimeTool/CommonTimeTool.h" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDateTimeGetter_Statics::NewProp_Second = { "Second", nullptr, (EPropertyFlags)0x0010000000000004, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FDateTimeGetter, Second), METADATA_PARAMS(Z_Construct_UScriptStruct_FDateTimeGetter_Statics::NewProp_Second_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDateTimeGetter_Statics::NewProp_Second_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDateTimeGetter_Statics::NewProp_IsAM_MetaData[] = {
		{ "Category", "CommonAssetTool" },
		{ "ModuleRelativePath", "Public/TimeTool/CommonTimeTool.h" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDateTimeGetter_Statics::NewProp_IsAM = { "IsAM", nullptr, (EPropertyFlags)0x0010000000000004, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FDateTimeGetter, IsAM), METADATA_PARAMS(Z_Construct_UScriptStruct_FDateTimeGetter_Statics::NewProp_IsAM_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDateTimeGetter_Statics::NewProp_IsAM_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDateTimeGetter_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDateTimeGetter_Statics::NewProp_Year,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDateTimeGetter_Statics::NewProp_Month,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDateTimeGetter_Statics::NewProp_Day,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDateTimeGetter_Statics::NewProp_Hour,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDateTimeGetter_Statics::NewProp_Minute,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDateTimeGetter_Statics::NewProp_Second,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDateTimeGetter_Statics::NewProp_IsAM,
	};
	const UECodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDateTimeGetter_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CommonAssetTools,
		nullptr,
		&NewStructOps,
		"DateTimeGetter",
		sizeof(FDateTimeGetter),
		alignof(FDateTimeGetter),
		Z_Construct_UScriptStruct_FDateTimeGetter_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDateTimeGetter_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDateTimeGetter_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDateTimeGetter_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDateTimeGetter()
	{
		if (!Z_Registration_Info_UScriptStruct_DateTimeGetter.InnerSingleton)
		{
			UECodeGen_Private::ConstructUScriptStruct(Z_Registration_Info_UScriptStruct_DateTimeGetter.InnerSingleton, Z_Construct_UScriptStruct_FDateTimeGetter_Statics::ReturnStructParams);
		}
		return Z_Registration_Info_UScriptStruct_DateTimeGetter.InnerSingleton;
	}
	DEFINE_FUNCTION(UCommonTimeTool::execGetDateTimeToString)
	{
		P_GET_STRUCT_REF(FDateTimeGetter,Z_Param_Out_DateTimeStr);
		P_GET_UBOOL(Z_Param_bTo12);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetDateTimeToString(Z_Param_Out_DateTimeStr,Z_Param_bTo12);
		P_NATIVE_END;
	}
	void UCommonTimeTool::StaticRegisterNativesUCommonTimeTool()
	{
		UClass* Class = UCommonTimeTool::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetDateTimeToString", &UCommonTimeTool::execGetDateTimeToString },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCommonTimeTool_GetDateTimeToString_Statics
	{
		struct CommonTimeTool_eventGetDateTimeToString_Parms
		{
			FDateTimeGetter DateTimeStr;
			bool bTo12;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_DateTimeStr;
		static void NewProp_bTo12_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_bTo12;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UCommonTimeTool_GetDateTimeToString_Statics::NewProp_DateTimeStr = { "DateTimeStr", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonTimeTool_eventGetDateTimeToString_Parms, DateTimeStr), Z_Construct_UScriptStruct_FDateTimeGetter, METADATA_PARAMS(nullptr, 0) }; // 1786283476
	void Z_Construct_UFunction_UCommonTimeTool_GetDateTimeToString_Statics::NewProp_bTo12_SetBit(void* Obj)
	{
		((CommonTimeTool_eventGetDateTimeToString_Parms*)Obj)->bTo12 = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCommonTimeTool_GetDateTimeToString_Statics::NewProp_bTo12 = { "bTo12", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(CommonTimeTool_eventGetDateTimeToString_Parms), &Z_Construct_UFunction_UCommonTimeTool_GetDateTimeToString_Statics::NewProp_bTo12_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonTimeTool_GetDateTimeToString_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonTimeTool_GetDateTimeToString_Statics::NewProp_DateTimeStr,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonTimeTool_GetDateTimeToString_Statics::NewProp_bTo12,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonTimeTool_GetDateTimeToString_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonAssetTool" },
		{ "ModuleRelativePath", "Public/TimeTool/CommonTimeTool.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonTimeTool_GetDateTimeToString_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonTimeTool, nullptr, "GetDateTimeToString", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCommonTimeTool_GetDateTimeToString_Statics::CommonTimeTool_eventGetDateTimeToString_Parms), Z_Construct_UFunction_UCommonTimeTool_GetDateTimeToString_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTimeTool_GetDateTimeToString_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonTimeTool_GetDateTimeToString_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonTimeTool_GetDateTimeToString_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonTimeTool_GetDateTimeToString()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCommonTimeTool_GetDateTimeToString_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UCommonTimeTool);
	UClass* Z_Construct_UClass_UCommonTimeTool_NoRegister()
	{
		return UCommonTimeTool::StaticClass();
	}
	struct Z_Construct_UClass_UCommonTimeTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonTimeTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonAssetTools,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCommonTimeTool_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCommonTimeTool_GetDateTimeToString, "GetDateTimeToString" }, // 772989461
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonTimeTool_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Category", "CommonAssetTool" },
		{ "IncludePath", "TimeTool/CommonTimeTool.h" },
		{ "ModuleRelativePath", "Public/TimeTool/CommonTimeTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonTimeTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonTimeTool>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UCommonTimeTool_Statics::ClassParams = {
		&UCommonTimeTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonTimeTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonTimeTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonTimeTool()
	{
		if (!Z_Registration_Info_UClass_UCommonTimeTool.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UCommonTimeTool.OuterSingleton, Z_Construct_UClass_UCommonTimeTool_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UCommonTimeTool.OuterSingleton;
	}
	template<> COMMONASSETTOOLS_API UClass* StaticClass<UCommonTimeTool>()
	{
		return UCommonTimeTool::StaticClass();
	}
	UCommonTimeTool::UCommonTimeTool(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonTimeTool);
	UCommonTimeTool::~UCommonTimeTool() {}
	struct Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_TimeTool_CommonTimeTool_h_Statics
	{
		static const FStructRegisterCompiledInInfo ScriptStructInfo[];
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FStructRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_TimeTool_CommonTimeTool_h_Statics::ScriptStructInfo[] = {
		{ FDateTimeGetter::StaticStruct, Z_Construct_UScriptStruct_FDateTimeGetter_Statics::NewStructOps, TEXT("DateTimeGetter"), &Z_Registration_Info_UScriptStruct_DateTimeGetter, CONSTRUCT_RELOAD_VERSION_INFO(FStructReloadVersionInfo, sizeof(FDateTimeGetter), 1786283476U) },
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_TimeTool_CommonTimeTool_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UCommonTimeTool, UCommonTimeTool::StaticClass, TEXT("UCommonTimeTool"), &Z_Registration_Info_UClass_UCommonTimeTool, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UCommonTimeTool), 1019022913U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_TimeTool_CommonTimeTool_h_2268308755(TEXT("/Script/CommonAssetTools"),
		Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_TimeTool_CommonTimeTool_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_TimeTool_CommonTimeTool_h_Statics::ClassInfo),
		Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_TimeTool_CommonTimeTool_h_Statics::ScriptStructInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_TimeTool_CommonTimeTool_h_Statics::ScriptStructInfo),
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
