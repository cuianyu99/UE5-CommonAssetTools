// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "ImageTool/CommonImageTool.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UTexture2D;
enum class FEImageFormat : uint8;
#ifdef COMMONASSETTOOLS_CommonImageTool_generated_h
#error "CommonImageTool.generated.h already included, missing '#pragma once' in CommonImageTool.h"
#endif
#define COMMONASSETTOOLS_CommonImageTool_generated_h

#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageTool_h_57_SPARSE_DATA
#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageTool_h_57_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execImportImageWithBinary); \
	DECLARE_FUNCTION(execImportImageWithPath);


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageTool_h_57_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execImportImageWithBinary); \
	DECLARE_FUNCTION(execImportImageWithPath);


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageTool_h_57_ACCESSORS
#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageTool_h_57_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonImageTool(); \
	friend struct Z_Construct_UClass_UCommonImageTool_Statics; \
public: \
	DECLARE_CLASS(UCommonImageTool, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonAssetTools"), NO_API) \
	DECLARE_SERIALIZER(UCommonImageTool)


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageTool_h_57_INCLASS \
private: \
	static void StaticRegisterNativesUCommonImageTool(); \
	friend struct Z_Construct_UClass_UCommonImageTool_Statics; \
public: \
	DECLARE_CLASS(UCommonImageTool, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonAssetTools"), NO_API) \
	DECLARE_SERIALIZER(UCommonImageTool)


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageTool_h_57_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonImageTool(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonImageTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonImageTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonImageTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonImageTool(UCommonImageTool&&); \
	NO_API UCommonImageTool(const UCommonImageTool&); \
public: \
	NO_API virtual ~UCommonImageTool();


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageTool_h_57_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonImageTool(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonImageTool(UCommonImageTool&&); \
	NO_API UCommonImageTool(const UCommonImageTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonImageTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonImageTool); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonImageTool) \
	NO_API virtual ~UCommonImageTool();


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageTool_h_54_PROLOG
#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageTool_h_57_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageTool_h_57_SPARSE_DATA \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageTool_h_57_RPC_WRAPPERS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageTool_h_57_ACCESSORS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageTool_h_57_INCLASS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageTool_h_57_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageTool_h_57_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageTool_h_57_SPARSE_DATA \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageTool_h_57_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageTool_h_57_ACCESSORS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageTool_h_57_INCLASS_NO_PURE_DECLS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageTool_h_57_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONASSETTOOLS_API UClass* StaticClass<class UCommonImageTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageTool_h


#define FOREACH_ENUM_FEIMAGEFORMAT(op) \
	op(FEImageFormat::Invalid) \
	op(FEImageFormat::PNG) \
	op(FEImageFormat::JPEG) \
	op(FEImageFormat::GrayscaleJPEG) \
	op(FEImageFormat::BMP) \
	op(FEImageFormat::ICO) \
	op(FEImageFormat::EXR) \
	op(FEImageFormat::ICNS) \
	op(FEImageFormat::TGA) \
	op(FEImageFormat::HDR) \
	op(FEImageFormat::TIFF) \
	op(FEImageFormat::DDS) 

enum class FEImageFormat : uint8;
template<> struct TIsUEnumClass<FEImageFormat> { enum { Value = true }; };
template<> COMMONASSETTOOLS_API UEnum* StaticEnum<FEImageFormat>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
