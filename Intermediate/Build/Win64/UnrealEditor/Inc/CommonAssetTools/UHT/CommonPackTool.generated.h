// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "PackageTool/CommonPackTool.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UObject;
struct FAssetPackageConfig;
#ifdef COMMONASSETTOOLS_CommonPackTool_generated_h
#error "CommonPackTool.generated.h already included, missing '#pragma once' in CommonPackTool.h"
#endif
#define COMMONASSETTOOLS_CommonPackTool_generated_h

#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackTool_h_15_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAssetPackageConfig_Statics; \
	COMMONASSETTOOLS_API static class UScriptStruct* StaticStruct();


template<> COMMONASSETTOOLS_API UScriptStruct* StaticStruct<struct FAssetPackageConfig>();

#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackTool_h_29_SPARSE_DATA
#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackTool_h_29_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execLoadAssetPackFromFBX); \
	DECLARE_FUNCTION(execLoadAsset); \
	DECLARE_FUNCTION(execLoadAssetPackage);


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackTool_h_29_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execLoadAssetPackFromFBX); \
	DECLARE_FUNCTION(execLoadAsset); \
	DECLARE_FUNCTION(execLoadAssetPackage);


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackTool_h_29_ACCESSORS
#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackTool_h_29_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonPackTool(); \
	friend struct Z_Construct_UClass_UCommonPackTool_Statics; \
public: \
	DECLARE_CLASS(UCommonPackTool, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonAssetTools"), NO_API) \
	DECLARE_SERIALIZER(UCommonPackTool)


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackTool_h_29_INCLASS \
private: \
	static void StaticRegisterNativesUCommonPackTool(); \
	friend struct Z_Construct_UClass_UCommonPackTool_Statics; \
public: \
	DECLARE_CLASS(UCommonPackTool, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonAssetTools"), NO_API) \
	DECLARE_SERIALIZER(UCommonPackTool)


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackTool_h_29_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonPackTool(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonPackTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonPackTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonPackTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonPackTool(UCommonPackTool&&); \
	NO_API UCommonPackTool(const UCommonPackTool&); \
public:


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackTool_h_29_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonPackTool(UCommonPackTool&&); \
	NO_API UCommonPackTool(const UCommonPackTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonPackTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonPackTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UCommonPackTool)


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackTool_h_26_PROLOG
#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackTool_h_29_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackTool_h_29_SPARSE_DATA \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackTool_h_29_RPC_WRAPPERS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackTool_h_29_ACCESSORS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackTool_h_29_INCLASS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackTool_h_29_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackTool_h_29_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackTool_h_29_SPARSE_DATA \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackTool_h_29_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackTool_h_29_ACCESSORS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackTool_h_29_INCLASS_NO_PURE_DECLS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackTool_h_29_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONASSETTOOLS_API UClass* StaticClass<class UCommonPackTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackTool_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
