// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "TimeTool/CommonTimeTool.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FDateTimeGetter;
#ifdef COMMONASSETTOOLS_CommonTimeTool_generated_h
#error "CommonTimeTool.generated.h already included, missing '#pragma once' in CommonTimeTool.h"
#endif
#define COMMONASSETTOOLS_CommonTimeTool_generated_h

#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_TimeTool_CommonTimeTool_h_12_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDateTimeGetter_Statics; \
	COMMONASSETTOOLS_API static class UScriptStruct* StaticStruct();


template<> COMMONASSETTOOLS_API UScriptStruct* StaticStruct<struct FDateTimeGetter>();

#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_TimeTool_CommonTimeTool_h_61_SPARSE_DATA
#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_TimeTool_CommonTimeTool_h_61_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetDateTimeToString);


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_TimeTool_CommonTimeTool_h_61_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetDateTimeToString);


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_TimeTool_CommonTimeTool_h_61_ACCESSORS
#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_TimeTool_CommonTimeTool_h_61_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonTimeTool(); \
	friend struct Z_Construct_UClass_UCommonTimeTool_Statics; \
public: \
	DECLARE_CLASS(UCommonTimeTool, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonAssetTools"), NO_API) \
	DECLARE_SERIALIZER(UCommonTimeTool)


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_TimeTool_CommonTimeTool_h_61_INCLASS \
private: \
	static void StaticRegisterNativesUCommonTimeTool(); \
	friend struct Z_Construct_UClass_UCommonTimeTool_Statics; \
public: \
	DECLARE_CLASS(UCommonTimeTool, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonAssetTools"), NO_API) \
	DECLARE_SERIALIZER(UCommonTimeTool)


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_TimeTool_CommonTimeTool_h_61_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonTimeTool(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonTimeTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonTimeTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonTimeTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonTimeTool(UCommonTimeTool&&); \
	NO_API UCommonTimeTool(const UCommonTimeTool&); \
public: \
	NO_API virtual ~UCommonTimeTool();


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_TimeTool_CommonTimeTool_h_61_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonTimeTool(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonTimeTool(UCommonTimeTool&&); \
	NO_API UCommonTimeTool(const UCommonTimeTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonTimeTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonTimeTool); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonTimeTool) \
	NO_API virtual ~UCommonTimeTool();


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_TimeTool_CommonTimeTool_h_58_PROLOG
#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_TimeTool_CommonTimeTool_h_61_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_TimeTool_CommonTimeTool_h_61_SPARSE_DATA \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_TimeTool_CommonTimeTool_h_61_RPC_WRAPPERS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_TimeTool_CommonTimeTool_h_61_ACCESSORS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_TimeTool_CommonTimeTool_h_61_INCLASS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_TimeTool_CommonTimeTool_h_61_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_TimeTool_CommonTimeTool_h_61_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_TimeTool_CommonTimeTool_h_61_SPARSE_DATA \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_TimeTool_CommonTimeTool_h_61_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_TimeTool_CommonTimeTool_h_61_ACCESSORS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_TimeTool_CommonTimeTool_h_61_INCLASS_NO_PURE_DECLS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_TimeTool_CommonTimeTool_h_61_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONASSETTOOLS_API UClass* StaticClass<class UCommonTimeTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_TimeTool_CommonTimeTool_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
