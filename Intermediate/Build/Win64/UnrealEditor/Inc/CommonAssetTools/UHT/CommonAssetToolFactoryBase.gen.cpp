// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonAssetToolFactoryBase.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonAssetToolFactoryBase() {}
// Cross Module References
	COMMONASSETTOOLS_API UClass* Z_Construct_UClass_UCommonAssetToolFactoryBase();
	COMMONASSETTOOLS_API UClass* Z_Construct_UClass_UCommonAssetToolFactoryBase_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_CommonAssetTools();
// End Cross Module References
	void UCommonAssetToolFactoryBase::StaticRegisterNativesUCommonAssetToolFactoryBase()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UCommonAssetToolFactoryBase);
	UClass* Z_Construct_UClass_UCommonAssetToolFactoryBase_NoRegister()
	{
		return UCommonAssetToolFactoryBase::StaticClass();
	}
	struct Z_Construct_UClass_UCommonAssetToolFactoryBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonAssetToolFactoryBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonAssetTools,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonAssetToolFactoryBase_Statics::Class_MetaDataParams[] = {
		{ "Category", "CommonAssetTool" },
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "CommonAssetToolFactoryBase.h" },
		{ "ModuleRelativePath", "Public/CommonAssetToolFactoryBase.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonAssetToolFactoryBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonAssetToolFactoryBase>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UCommonAssetToolFactoryBase_Statics::ClassParams = {
		&UCommonAssetToolFactoryBase::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonAssetToolFactoryBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonAssetToolFactoryBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonAssetToolFactoryBase()
	{
		if (!Z_Registration_Info_UClass_UCommonAssetToolFactoryBase.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UCommonAssetToolFactoryBase.OuterSingleton, Z_Construct_UClass_UCommonAssetToolFactoryBase_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UCommonAssetToolFactoryBase.OuterSingleton;
	}
	template<> COMMONASSETTOOLS_API UClass* StaticClass<UCommonAssetToolFactoryBase>()
	{
		return UCommonAssetToolFactoryBase::StaticClass();
	}
	UCommonAssetToolFactoryBase::UCommonAssetToolFactoryBase(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonAssetToolFactoryBase);
	UCommonAssetToolFactoryBase::~UCommonAssetToolFactoryBase() {}
	struct Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolFactoryBase_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolFactoryBase_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UCommonAssetToolFactoryBase, UCommonAssetToolFactoryBase::StaticClass, TEXT("UCommonAssetToolFactoryBase"), &Z_Registration_Info_UClass_UCommonAssetToolFactoryBase, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UCommonAssetToolFactoryBase), 4043814101U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolFactoryBase_h_529085277(TEXT("/Script/CommonAssetTools"),
		Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolFactoryBase_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolFactoryBase_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
