// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "JsonTool/RapidJsonUEFunctionLibrary.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UObject;
struct FColor;
struct FLatentActionInfo;
#ifdef COMMONASSETTOOLS_RapidJsonUEFunctionLibrary_generated_h
#error "RapidJsonUEFunctionLibrary.generated.h already included, missing '#pragma once' in RapidJsonUEFunctionLibrary.h"
#endif
#define COMMONASSETTOOLS_RapidJsonUEFunctionLibrary_generated_h

#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_JsonTool_RapidJsonUEFunctionLibrary_h_79_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCustomStructJsonSerializationParent_Statics; \
	COMMONASSETTOOLS_API static class UScriptStruct* StaticStruct();


template<> COMMONASSETTOOLS_API UScriptStruct* StaticStruct<struct FCustomStructJsonSerializationParent>();

#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_JsonTool_RapidJsonUEFunctionLibrary_h_98_SPARSE_DATA
#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_JsonTool_RapidJsonUEFunctionLibrary_h_98_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execDeserializeTArrayFTransform_Wrapper); \
	DECLARE_FUNCTION(execDeserializeTArrayFRotator_Wrapper); \
	DECLARE_FUNCTION(execDeserializeTArrayFVector_Wrapper); \
	DECLARE_FUNCTION(execDeserializeTArrayFloat_Wrapper); \
	DECLARE_FUNCTION(execDeserializeTArrayFText_Wrapper); \
	DECLARE_FUNCTION(execDeserializeTArrayFName_Wrapper); \
	DECLARE_FUNCTION(execDeserializeTArrayBool_Wrapper); \
	DECLARE_FUNCTION(execDeserializeTArrayFColor_Wrapper); \
	DECLARE_FUNCTION(execDeserializeTArrayFString_Wrapper); \
	DECLARE_FUNCTION(execDeserializeTArrayInt64_Wrapper); \
	DECLARE_FUNCTION(execDeserializeTArrayUInt8_Wrapper); \
	DECLARE_FUNCTION(execDeserializeTArrayInt_Wrapper); \
	DECLARE_FUNCTION(execSerializeTArrayFTransform_Wrapper); \
	DECLARE_FUNCTION(execSerializeTArrayFRotator_Wrapper); \
	DECLARE_FUNCTION(execSerializeTArrayFVector_Wrapper); \
	DECLARE_FUNCTION(execSerializeTArrayFloat_Wrapper); \
	DECLARE_FUNCTION(execSerializeTArrayFText_Wrapper); \
	DECLARE_FUNCTION(execSerializeTArrayFName_Wrapper); \
	DECLARE_FUNCTION(execSerializeTArrayBool_Wrapper); \
	DECLARE_FUNCTION(execSerializeTArrayFColor_Wrapper); \
	DECLARE_FUNCTION(execSerializeTArrayFString_Wrapper); \
	DECLARE_FUNCTION(execSerializeTArrayInt64_Wrapper); \
	DECLARE_FUNCTION(execSerializeTArrayUInt8_Wrapper); \
	DECLARE_FUNCTION(execSerializeTArrayInt_Wrapper); \
	DECLARE_FUNCTION(execDeserializeFTransform_Wrapper); \
	DECLARE_FUNCTION(execDeserializeFRotator_Wrapper); \
	DECLARE_FUNCTION(execDeserializeFVector_Wrapper); \
	DECLARE_FUNCTION(execDeserializeFloat_Wrapper); \
	DECLARE_FUNCTION(execDeserializeFText_Wrapper); \
	DECLARE_FUNCTION(execDeserializeFName_Wrapper); \
	DECLARE_FUNCTION(execDeserializeBool_Wrapper); \
	DECLARE_FUNCTION(execDeserializeFColor_Wrapper); \
	DECLARE_FUNCTION(execDeserializeFString_Wrapper); \
	DECLARE_FUNCTION(execDeserializeInt64_Wrapper); \
	DECLARE_FUNCTION(execDeserializeUInt8_Wrapper); \
	DECLARE_FUNCTION(execDeserializeInt_Wrapper); \
	DECLARE_FUNCTION(execSerializeFTransform_Wrapper); \
	DECLARE_FUNCTION(execSerializeFRotator_Wrapper); \
	DECLARE_FUNCTION(execSerializeFVector_Wrapper); \
	DECLARE_FUNCTION(execSerializeFloat_Wrapper); \
	DECLARE_FUNCTION(execSerializeFText_Wrapper); \
	DECLARE_FUNCTION(execSerializeFName_Wrapper); \
	DECLARE_FUNCTION(execSerializeBool_Wrapper); \
	DECLARE_FUNCTION(execSerializeFColor_Wrapper); \
	DECLARE_FUNCTION(execSerializeFString_Wrapper); \
	DECLARE_FUNCTION(execSerializeInt64_Wrapper); \
	DECLARE_FUNCTION(execSerializeUInt8_Wrapper); \
	DECLARE_FUNCTION(execSerializeInt_Wrapper); \
	DECLARE_FUNCTION(execDeSerializeTransformArrayAsync); \
	DECLARE_FUNCTION(execSerializeTransformArrayAsync); \
	DECLARE_FUNCTION(execDeSerializeRotatorArrayAsync); \
	DECLARE_FUNCTION(execSerializeRotatorArrayAsync); \
	DECLARE_FUNCTION(execDeSerializeVectorArrayAsync); \
	DECLARE_FUNCTION(execSerializeVectorArrayAsync); \
	DECLARE_FUNCTION(execDeSerializeFloatArrayAsync); \
	DECLARE_FUNCTION(execSerializeFloatArrayAsync); \
	DECLARE_FUNCTION(execDeSerializeTextArrayAsync); \
	DECLARE_FUNCTION(execSerializeTextArrayAsync); \
	DECLARE_FUNCTION(execDeSerializeNameArrayAsync); \
	DECLARE_FUNCTION(execSerializeNameArrayAsync); \
	DECLARE_FUNCTION(execDeSerializeBoolArrayAsync); \
	DECLARE_FUNCTION(execSerializeBoolArrayAsync); \
	DECLARE_FUNCTION(execDeSerializeColorArrayAsync); \
	DECLARE_FUNCTION(execSerializeColorArrayAsync); \
	DECLARE_FUNCTION(execDeSerializeStringArrayAsync); \
	DECLARE_FUNCTION(execSerializeStringArrayAsync); \
	DECLARE_FUNCTION(execDeSerializeInt64ArrayAsync); \
	DECLARE_FUNCTION(execSerializeInt64ArrayAsync); \
	DECLARE_FUNCTION(execDeSerializeUInt8ArrayAsync); \
	DECLARE_FUNCTION(execSerializeUint8ArrayAsync); \
	DECLARE_FUNCTION(execDeSerializeInt32ArrayAsync); \
	DECLARE_FUNCTION(execSerializeInt32ArrayAsync);


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_JsonTool_RapidJsonUEFunctionLibrary_h_98_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execDeserializeTArrayFTransform_Wrapper); \
	DECLARE_FUNCTION(execDeserializeTArrayFRotator_Wrapper); \
	DECLARE_FUNCTION(execDeserializeTArrayFVector_Wrapper); \
	DECLARE_FUNCTION(execDeserializeTArrayFloat_Wrapper); \
	DECLARE_FUNCTION(execDeserializeTArrayFText_Wrapper); \
	DECLARE_FUNCTION(execDeserializeTArrayFName_Wrapper); \
	DECLARE_FUNCTION(execDeserializeTArrayBool_Wrapper); \
	DECLARE_FUNCTION(execDeserializeTArrayFColor_Wrapper); \
	DECLARE_FUNCTION(execDeserializeTArrayFString_Wrapper); \
	DECLARE_FUNCTION(execDeserializeTArrayInt64_Wrapper); \
	DECLARE_FUNCTION(execDeserializeTArrayUInt8_Wrapper); \
	DECLARE_FUNCTION(execDeserializeTArrayInt_Wrapper); \
	DECLARE_FUNCTION(execSerializeTArrayFTransform_Wrapper); \
	DECLARE_FUNCTION(execSerializeTArrayFRotator_Wrapper); \
	DECLARE_FUNCTION(execSerializeTArrayFVector_Wrapper); \
	DECLARE_FUNCTION(execSerializeTArrayFloat_Wrapper); \
	DECLARE_FUNCTION(execSerializeTArrayFText_Wrapper); \
	DECLARE_FUNCTION(execSerializeTArrayFName_Wrapper); \
	DECLARE_FUNCTION(execSerializeTArrayBool_Wrapper); \
	DECLARE_FUNCTION(execSerializeTArrayFColor_Wrapper); \
	DECLARE_FUNCTION(execSerializeTArrayFString_Wrapper); \
	DECLARE_FUNCTION(execSerializeTArrayInt64_Wrapper); \
	DECLARE_FUNCTION(execSerializeTArrayUInt8_Wrapper); \
	DECLARE_FUNCTION(execSerializeTArrayInt_Wrapper); \
	DECLARE_FUNCTION(execDeserializeFTransform_Wrapper); \
	DECLARE_FUNCTION(execDeserializeFRotator_Wrapper); \
	DECLARE_FUNCTION(execDeserializeFVector_Wrapper); \
	DECLARE_FUNCTION(execDeserializeFloat_Wrapper); \
	DECLARE_FUNCTION(execDeserializeFText_Wrapper); \
	DECLARE_FUNCTION(execDeserializeFName_Wrapper); \
	DECLARE_FUNCTION(execDeserializeBool_Wrapper); \
	DECLARE_FUNCTION(execDeserializeFColor_Wrapper); \
	DECLARE_FUNCTION(execDeserializeFString_Wrapper); \
	DECLARE_FUNCTION(execDeserializeInt64_Wrapper); \
	DECLARE_FUNCTION(execDeserializeUInt8_Wrapper); \
	DECLARE_FUNCTION(execDeserializeInt_Wrapper); \
	DECLARE_FUNCTION(execSerializeFTransform_Wrapper); \
	DECLARE_FUNCTION(execSerializeFRotator_Wrapper); \
	DECLARE_FUNCTION(execSerializeFVector_Wrapper); \
	DECLARE_FUNCTION(execSerializeFloat_Wrapper); \
	DECLARE_FUNCTION(execSerializeFText_Wrapper); \
	DECLARE_FUNCTION(execSerializeFName_Wrapper); \
	DECLARE_FUNCTION(execSerializeBool_Wrapper); \
	DECLARE_FUNCTION(execSerializeFColor_Wrapper); \
	DECLARE_FUNCTION(execSerializeFString_Wrapper); \
	DECLARE_FUNCTION(execSerializeInt64_Wrapper); \
	DECLARE_FUNCTION(execSerializeUInt8_Wrapper); \
	DECLARE_FUNCTION(execSerializeInt_Wrapper); \
	DECLARE_FUNCTION(execDeSerializeTransformArrayAsync); \
	DECLARE_FUNCTION(execSerializeTransformArrayAsync); \
	DECLARE_FUNCTION(execDeSerializeRotatorArrayAsync); \
	DECLARE_FUNCTION(execSerializeRotatorArrayAsync); \
	DECLARE_FUNCTION(execDeSerializeVectorArrayAsync); \
	DECLARE_FUNCTION(execSerializeVectorArrayAsync); \
	DECLARE_FUNCTION(execDeSerializeFloatArrayAsync); \
	DECLARE_FUNCTION(execSerializeFloatArrayAsync); \
	DECLARE_FUNCTION(execDeSerializeTextArrayAsync); \
	DECLARE_FUNCTION(execSerializeTextArrayAsync); \
	DECLARE_FUNCTION(execDeSerializeNameArrayAsync); \
	DECLARE_FUNCTION(execSerializeNameArrayAsync); \
	DECLARE_FUNCTION(execDeSerializeBoolArrayAsync); \
	DECLARE_FUNCTION(execSerializeBoolArrayAsync); \
	DECLARE_FUNCTION(execDeSerializeColorArrayAsync); \
	DECLARE_FUNCTION(execSerializeColorArrayAsync); \
	DECLARE_FUNCTION(execDeSerializeStringArrayAsync); \
	DECLARE_FUNCTION(execSerializeStringArrayAsync); \
	DECLARE_FUNCTION(execDeSerializeInt64ArrayAsync); \
	DECLARE_FUNCTION(execSerializeInt64ArrayAsync); \
	DECLARE_FUNCTION(execDeSerializeUInt8ArrayAsync); \
	DECLARE_FUNCTION(execSerializeUint8ArrayAsync); \
	DECLARE_FUNCTION(execDeSerializeInt32ArrayAsync); \
	DECLARE_FUNCTION(execSerializeInt32ArrayAsync);


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_JsonTool_RapidJsonUEFunctionLibrary_h_98_ACCESSORS
#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_JsonTool_RapidJsonUEFunctionLibrary_h_98_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURapidJsonUEFunctionLibrary(); \
	friend struct Z_Construct_UClass_URapidJsonUEFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(URapidJsonUEFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonAssetTools"), NO_API) \
	DECLARE_SERIALIZER(URapidJsonUEFunctionLibrary)


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_JsonTool_RapidJsonUEFunctionLibrary_h_98_INCLASS \
private: \
	static void StaticRegisterNativesURapidJsonUEFunctionLibrary(); \
	friend struct Z_Construct_UClass_URapidJsonUEFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(URapidJsonUEFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonAssetTools"), NO_API) \
	DECLARE_SERIALIZER(URapidJsonUEFunctionLibrary)


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_JsonTool_RapidJsonUEFunctionLibrary_h_98_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URapidJsonUEFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URapidJsonUEFunctionLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URapidJsonUEFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URapidJsonUEFunctionLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URapidJsonUEFunctionLibrary(URapidJsonUEFunctionLibrary&&); \
	NO_API URapidJsonUEFunctionLibrary(const URapidJsonUEFunctionLibrary&); \
public: \
	NO_API virtual ~URapidJsonUEFunctionLibrary();


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_JsonTool_RapidJsonUEFunctionLibrary_h_98_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URapidJsonUEFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URapidJsonUEFunctionLibrary(URapidJsonUEFunctionLibrary&&); \
	NO_API URapidJsonUEFunctionLibrary(const URapidJsonUEFunctionLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URapidJsonUEFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URapidJsonUEFunctionLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URapidJsonUEFunctionLibrary) \
	NO_API virtual ~URapidJsonUEFunctionLibrary();


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_JsonTool_RapidJsonUEFunctionLibrary_h_95_PROLOG
#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_JsonTool_RapidJsonUEFunctionLibrary_h_98_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_JsonTool_RapidJsonUEFunctionLibrary_h_98_SPARSE_DATA \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_JsonTool_RapidJsonUEFunctionLibrary_h_98_RPC_WRAPPERS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_JsonTool_RapidJsonUEFunctionLibrary_h_98_ACCESSORS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_JsonTool_RapidJsonUEFunctionLibrary_h_98_INCLASS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_JsonTool_RapidJsonUEFunctionLibrary_h_98_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_JsonTool_RapidJsonUEFunctionLibrary_h_98_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_JsonTool_RapidJsonUEFunctionLibrary_h_98_SPARSE_DATA \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_JsonTool_RapidJsonUEFunctionLibrary_h_98_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_JsonTool_RapidJsonUEFunctionLibrary_h_98_ACCESSORS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_JsonTool_RapidJsonUEFunctionLibrary_h_98_INCLASS_NO_PURE_DECLS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_JsonTool_RapidJsonUEFunctionLibrary_h_98_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONASSETTOOLS_API UClass* StaticClass<class URapidJsonUEFunctionLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_JsonTool_RapidJsonUEFunctionLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
