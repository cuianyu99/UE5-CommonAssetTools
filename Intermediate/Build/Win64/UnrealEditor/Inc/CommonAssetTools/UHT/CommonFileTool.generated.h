// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "FileTool/CommonFileTool.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FFileSuffixType;
#ifdef COMMONASSETTOOLS_CommonFileTool_generated_h
#error "CommonFileTool.generated.h already included, missing '#pragma once' in CommonFileTool.h"
#endif
#define COMMONASSETTOOLS_CommonFileTool_generated_h

#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_FileTool_CommonFileTool_h_24_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FFileSuffixType_Statics; \
	COMMONASSETTOOLS_API static class UScriptStruct* StaticStruct();


template<> COMMONASSETTOOLS_API UScriptStruct* StaticStruct<struct FFileSuffixType>();

#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_FileTool_CommonFileTool_h_36_SPARSE_DATA
#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_FileTool_CommonFileTool_h_36_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execRemoveFilesWithinDirectory); \
	DECLARE_FUNCTION(execIsDirectoryExists); \
	DECLARE_FUNCTION(execReadFileToString); \
	DECLARE_FUNCTION(execGetFileName); \
	DECLARE_FUNCTION(execCopyFile); \
	DECLARE_FUNCTION(execRenameFile); \
	DECLARE_FUNCTION(execCreateFolder); \
	DECLARE_FUNCTION(execGetFilePathWithinFileDialog);


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_FileTool_CommonFileTool_h_36_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execRemoveFilesWithinDirectory); \
	DECLARE_FUNCTION(execIsDirectoryExists); \
	DECLARE_FUNCTION(execReadFileToString); \
	DECLARE_FUNCTION(execGetFileName); \
	DECLARE_FUNCTION(execCopyFile); \
	DECLARE_FUNCTION(execRenameFile); \
	DECLARE_FUNCTION(execCreateFolder); \
	DECLARE_FUNCTION(execGetFilePathWithinFileDialog);


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_FileTool_CommonFileTool_h_36_ACCESSORS
#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_FileTool_CommonFileTool_h_36_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonFileTool(); \
	friend struct Z_Construct_UClass_UCommonFileTool_Statics; \
public: \
	DECLARE_CLASS(UCommonFileTool, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonAssetTools"), NO_API) \
	DECLARE_SERIALIZER(UCommonFileTool)


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_FileTool_CommonFileTool_h_36_INCLASS \
private: \
	static void StaticRegisterNativesUCommonFileTool(); \
	friend struct Z_Construct_UClass_UCommonFileTool_Statics; \
public: \
	DECLARE_CLASS(UCommonFileTool, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonAssetTools"), NO_API) \
	DECLARE_SERIALIZER(UCommonFileTool)


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_FileTool_CommonFileTool_h_36_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonFileTool(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonFileTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonFileTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonFileTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonFileTool(UCommonFileTool&&); \
	NO_API UCommonFileTool(const UCommonFileTool&); \
public:


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_FileTool_CommonFileTool_h_36_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonFileTool(UCommonFileTool&&); \
	NO_API UCommonFileTool(const UCommonFileTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonFileTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonFileTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UCommonFileTool)


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_FileTool_CommonFileTool_h_33_PROLOG
#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_FileTool_CommonFileTool_h_36_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_FileTool_CommonFileTool_h_36_SPARSE_DATA \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_FileTool_CommonFileTool_h_36_RPC_WRAPPERS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_FileTool_CommonFileTool_h_36_ACCESSORS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_FileTool_CommonFileTool_h_36_INCLASS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_FileTool_CommonFileTool_h_36_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_FileTool_CommonFileTool_h_36_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_FileTool_CommonFileTool_h_36_SPARSE_DATA \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_FileTool_CommonFileTool_h_36_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_FileTool_CommonFileTool_h_36_ACCESSORS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_FileTool_CommonFileTool_h_36_INCLASS_NO_PURE_DECLS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_FileTool_CommonFileTool_h_36_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONASSETTOOLS_API UClass* StaticClass<class UCommonFileTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_FileTool_CommonFileTool_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
