// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "JsonTool/RapidJsonUEFunctionLibrary.h"
#include "../../Source/Runtime/Engine/Classes/Engine/LatentActionManager.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRapidJsonUEFunctionLibrary() {}
// Cross Module References
	COMMONASSETTOOLS_API UClass* Z_Construct_UClass_URapidJsonUEFunctionLibrary();
	COMMONASSETTOOLS_API UClass* Z_Construct_UClass_URapidJsonUEFunctionLibrary_NoRegister();
	COMMONASSETTOOLS_API UScriptStruct* Z_Construct_UScriptStruct_FCustomStructJsonSerializationParent();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FColor();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FLatentActionInfo();
	UPackage* Z_Construct_UPackage__Script_CommonAssetTools();
// End Cross Module References
	static FStructRegistrationInfo Z_Registration_Info_UScriptStruct_CustomStructJsonSerializationParent;
class UScriptStruct* FCustomStructJsonSerializationParent::StaticStruct()
{
	if (!Z_Registration_Info_UScriptStruct_CustomStructJsonSerializationParent.OuterSingleton)
	{
		Z_Registration_Info_UScriptStruct_CustomStructJsonSerializationParent.OuterSingleton = GetStaticStruct(Z_Construct_UScriptStruct_FCustomStructJsonSerializationParent, (UObject*)Z_Construct_UPackage__Script_CommonAssetTools(), TEXT("CustomStructJsonSerializationParent"));
	}
	return Z_Registration_Info_UScriptStruct_CustomStructJsonSerializationParent.OuterSingleton;
}
template<> COMMONASSETTOOLS_API UScriptStruct* StaticStruct<FCustomStructJsonSerializationParent>()
{
	return FCustomStructJsonSerializationParent::StaticStruct();
}
	struct Z_Construct_UScriptStruct_FCustomStructJsonSerializationParent_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UECodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCustomStructJsonSerializationParent_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "// Custom Struct Json Serialization Parent\n" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
		{ "ToolTip", "Custom Struct Json Serialization Parent" },
	};
#endif
	void* Z_Construct_UScriptStruct_FCustomStructJsonSerializationParent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCustomStructJsonSerializationParent>();
	}
	const UECodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCustomStructJsonSerializationParent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CommonAssetTools,
		nullptr,
		&NewStructOps,
		"CustomStructJsonSerializationParent",
		sizeof(FCustomStructJsonSerializationParent),
		alignof(FCustomStructJsonSerializationParent),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCustomStructJsonSerializationParent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCustomStructJsonSerializationParent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCustomStructJsonSerializationParent()
	{
		if (!Z_Registration_Info_UScriptStruct_CustomStructJsonSerializationParent.InnerSingleton)
		{
			UECodeGen_Private::ConstructUScriptStruct(Z_Registration_Info_UScriptStruct_CustomStructJsonSerializationParent.InnerSingleton, Z_Construct_UScriptStruct_FCustomStructJsonSerializationParent_Statics::ReturnStructParams);
		}
		return Z_Registration_Info_UScriptStruct_CustomStructJsonSerializationParent.InnerSingleton;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execDeserializeTArrayFTransform_Wrapper)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_JsonString);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FTransform>*)Z_Param__Result=URapidJsonUEFunctionLibrary::DeserializeTArrayFTransform_Wrapper(Z_Param_JsonString);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execDeserializeTArrayFRotator_Wrapper)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_JsonString);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FRotator>*)Z_Param__Result=URapidJsonUEFunctionLibrary::DeserializeTArrayFRotator_Wrapper(Z_Param_JsonString);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execDeserializeTArrayFVector_Wrapper)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_JsonString);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FVector>*)Z_Param__Result=URapidJsonUEFunctionLibrary::DeserializeTArrayFVector_Wrapper(Z_Param_JsonString);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execDeserializeTArrayFloat_Wrapper)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_JsonString);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<float>*)Z_Param__Result=URapidJsonUEFunctionLibrary::DeserializeTArrayFloat_Wrapper(Z_Param_JsonString);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execDeserializeTArrayFText_Wrapper)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_JsonString);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FText>*)Z_Param__Result=URapidJsonUEFunctionLibrary::DeserializeTArrayFText_Wrapper(Z_Param_JsonString);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execDeserializeTArrayFName_Wrapper)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_JsonString);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FName>*)Z_Param__Result=URapidJsonUEFunctionLibrary::DeserializeTArrayFName_Wrapper(Z_Param_JsonString);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execDeserializeTArrayBool_Wrapper)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_JsonString);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<bool>*)Z_Param__Result=URapidJsonUEFunctionLibrary::DeserializeTArrayBool_Wrapper(Z_Param_JsonString);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execDeserializeTArrayFColor_Wrapper)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_JsonString);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FColor>*)Z_Param__Result=URapidJsonUEFunctionLibrary::DeserializeTArrayFColor_Wrapper(Z_Param_JsonString);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execDeserializeTArrayFString_Wrapper)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_JsonString);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FString>*)Z_Param__Result=URapidJsonUEFunctionLibrary::DeserializeTArrayFString_Wrapper(Z_Param_JsonString);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execDeserializeTArrayInt64_Wrapper)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_JsonString);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<int64>*)Z_Param__Result=URapidJsonUEFunctionLibrary::DeserializeTArrayInt64_Wrapper(Z_Param_JsonString);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execDeserializeTArrayUInt8_Wrapper)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_JsonString);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<uint8>*)Z_Param__Result=URapidJsonUEFunctionLibrary::DeserializeTArrayUInt8_Wrapper(Z_Param_JsonString);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execDeserializeTArrayInt_Wrapper)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_JsonString);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<int32>*)Z_Param__Result=URapidJsonUEFunctionLibrary::DeserializeTArrayInt_Wrapper(Z_Param_JsonString);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execSerializeTArrayFTransform_Wrapper)
	{
		P_GET_TARRAY_REF(FTransform,Z_Param_Out_inTransform);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=URapidJsonUEFunctionLibrary::SerializeTArrayFTransform_Wrapper(Z_Param_Out_inTransform);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execSerializeTArrayFRotator_Wrapper)
	{
		P_GET_TARRAY_REF(FRotator,Z_Param_Out_inRotator);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=URapidJsonUEFunctionLibrary::SerializeTArrayFRotator_Wrapper(Z_Param_Out_inRotator);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execSerializeTArrayFVector_Wrapper)
	{
		P_GET_TARRAY_REF(FVector,Z_Param_Out_inFVector);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=URapidJsonUEFunctionLibrary::SerializeTArrayFVector_Wrapper(Z_Param_Out_inFVector);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execSerializeTArrayFloat_Wrapper)
	{
		P_GET_TARRAY_REF(float,Z_Param_Out_inFloats);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=URapidJsonUEFunctionLibrary::SerializeTArrayFloat_Wrapper(Z_Param_Out_inFloats);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execSerializeTArrayFText_Wrapper)
	{
		P_GET_TARRAY_REF(FText,Z_Param_Out_InTexts);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=URapidJsonUEFunctionLibrary::SerializeTArrayFText_Wrapper(Z_Param_Out_InTexts);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execSerializeTArrayFName_Wrapper)
	{
		P_GET_TARRAY_REF(FName,Z_Param_Out_InNames);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=URapidJsonUEFunctionLibrary::SerializeTArrayFName_Wrapper(Z_Param_Out_InNames);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execSerializeTArrayBool_Wrapper)
	{
		P_GET_TARRAY_REF(bool,Z_Param_Out_InBools);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=URapidJsonUEFunctionLibrary::SerializeTArrayBool_Wrapper(Z_Param_Out_InBools);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execSerializeTArrayFColor_Wrapper)
	{
		P_GET_TARRAY_REF(FColor,Z_Param_Out_Colors);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=URapidJsonUEFunctionLibrary::SerializeTArrayFColor_Wrapper(Z_Param_Out_Colors);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execSerializeTArrayFString_Wrapper)
	{
		P_GET_TARRAY_REF(FString,Z_Param_Out_InStrings);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=URapidJsonUEFunctionLibrary::SerializeTArrayFString_Wrapper(Z_Param_Out_InStrings);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execSerializeTArrayInt64_Wrapper)
	{
		P_GET_TARRAY_REF(int64,Z_Param_Out_InInt64s);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=URapidJsonUEFunctionLibrary::SerializeTArrayInt64_Wrapper(Z_Param_Out_InInt64s);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execSerializeTArrayUInt8_Wrapper)
	{
		P_GET_TARRAY_REF(uint8,Z_Param_Out_InUInt8s);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=URapidJsonUEFunctionLibrary::SerializeTArrayUInt8_Wrapper(Z_Param_Out_InUInt8s);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execSerializeTArrayInt_Wrapper)
	{
		P_GET_TARRAY_REF(int32,Z_Param_Out_InInts);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=URapidJsonUEFunctionLibrary::SerializeTArrayInt_Wrapper(Z_Param_Out_InInts);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execDeserializeFTransform_Wrapper)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_JsonString);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FTransform*)Z_Param__Result=URapidJsonUEFunctionLibrary::DeserializeFTransform_Wrapper(Z_Param_JsonString);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execDeserializeFRotator_Wrapper)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_JsonString);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FRotator*)Z_Param__Result=URapidJsonUEFunctionLibrary::DeserializeFRotator_Wrapper(Z_Param_JsonString);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execDeserializeFVector_Wrapper)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_JsonString);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FVector*)Z_Param__Result=URapidJsonUEFunctionLibrary::DeserializeFVector_Wrapper(Z_Param_JsonString);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execDeserializeFloat_Wrapper)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_JsonString);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=URapidJsonUEFunctionLibrary::DeserializeFloat_Wrapper(Z_Param_JsonString);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execDeserializeFText_Wrapper)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_JsonString);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FText*)Z_Param__Result=URapidJsonUEFunctionLibrary::DeserializeFText_Wrapper(Z_Param_JsonString);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execDeserializeFName_Wrapper)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_JsonString);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FName*)Z_Param__Result=URapidJsonUEFunctionLibrary::DeserializeFName_Wrapper(Z_Param_JsonString);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execDeserializeBool_Wrapper)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_JsonString);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=URapidJsonUEFunctionLibrary::DeserializeBool_Wrapper(Z_Param_JsonString);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execDeserializeFColor_Wrapper)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_JsonString);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FColor*)Z_Param__Result=URapidJsonUEFunctionLibrary::DeserializeFColor_Wrapper(Z_Param_JsonString);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execDeserializeFString_Wrapper)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_JsonString);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=URapidJsonUEFunctionLibrary::DeserializeFString_Wrapper(Z_Param_JsonString);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execDeserializeInt64_Wrapper)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_JsonString);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int64*)Z_Param__Result=URapidJsonUEFunctionLibrary::DeserializeInt64_Wrapper(Z_Param_JsonString);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execDeserializeUInt8_Wrapper)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_JsonString);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(uint8*)Z_Param__Result=URapidJsonUEFunctionLibrary::DeserializeUInt8_Wrapper(Z_Param_JsonString);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execDeserializeInt_Wrapper)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_JsonString);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=URapidJsonUEFunctionLibrary::DeserializeInt_Wrapper(Z_Param_JsonString);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execSerializeFTransform_Wrapper)
	{
		P_GET_STRUCT(FTransform,Z_Param_TransformValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=URapidJsonUEFunctionLibrary::SerializeFTransform_Wrapper(Z_Param_TransformValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execSerializeFRotator_Wrapper)
	{
		P_GET_STRUCT(FRotator,Z_Param_RotatorValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=URapidJsonUEFunctionLibrary::SerializeFRotator_Wrapper(Z_Param_RotatorValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execSerializeFVector_Wrapper)
	{
		P_GET_STRUCT(FVector,Z_Param_VectorValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=URapidJsonUEFunctionLibrary::SerializeFVector_Wrapper(Z_Param_VectorValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execSerializeFloat_Wrapper)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_FloatValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=URapidJsonUEFunctionLibrary::SerializeFloat_Wrapper(Z_Param_FloatValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execSerializeFText_Wrapper)
	{
		P_GET_PROPERTY(FTextProperty,Z_Param_TextValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=URapidJsonUEFunctionLibrary::SerializeFText_Wrapper(Z_Param_TextValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execSerializeFName_Wrapper)
	{
		P_GET_PROPERTY(FNameProperty,Z_Param_NameValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=URapidJsonUEFunctionLibrary::SerializeFName_Wrapper(Z_Param_NameValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execSerializeBool_Wrapper)
	{
		P_GET_UBOOL(Z_Param_BoolValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=URapidJsonUEFunctionLibrary::SerializeBool_Wrapper(Z_Param_BoolValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execSerializeFColor_Wrapper)
	{
		P_GET_STRUCT(FColor,Z_Param_ColorValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=URapidJsonUEFunctionLibrary::SerializeFColor_Wrapper(Z_Param_ColorValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execSerializeFString_Wrapper)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_StringValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=URapidJsonUEFunctionLibrary::SerializeFString_Wrapper(Z_Param_StringValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execSerializeInt64_Wrapper)
	{
		P_GET_PROPERTY(FInt64Property,Z_Param_Int64Value);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=URapidJsonUEFunctionLibrary::SerializeInt64_Wrapper(Z_Param_Int64Value);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execSerializeUInt8_Wrapper)
	{
		P_GET_PROPERTY(FByteProperty,Z_Param_UInt8Value);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=URapidJsonUEFunctionLibrary::SerializeUInt8_Wrapper(Z_Param_UInt8Value);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execSerializeInt_Wrapper)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_IntValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=URapidJsonUEFunctionLibrary::SerializeInt_Wrapper(Z_Param_IntValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execDeSerializeTransformArrayAsync)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_STRUCT(FLatentActionInfo,Z_Param_LatentInfo);
		P_GET_PROPERTY(FStrProperty,Z_Param_stringToDeSerialize);
		P_GET_TARRAY_REF(FTransform,Z_Param_Out_deSerializedResult);
		P_FINISH;
		P_NATIVE_BEGIN;
		URapidJsonUEFunctionLibrary::DeSerializeTransformArrayAsync(Z_Param_WorldContextObject,Z_Param_LatentInfo,Z_Param_stringToDeSerialize,Z_Param_Out_deSerializedResult);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execSerializeTransformArrayAsync)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_STRUCT(FLatentActionInfo,Z_Param_LatentInfo);
		P_GET_TARRAY(FTransform,Z_Param_arrayToSerialize);
		P_GET_PROPERTY_REF(FStrProperty,Z_Param_Out_serializedString);
		P_FINISH;
		P_NATIVE_BEGIN;
		URapidJsonUEFunctionLibrary::SerializeTransformArrayAsync(Z_Param_WorldContextObject,Z_Param_LatentInfo,Z_Param_arrayToSerialize,Z_Param_Out_serializedString);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execDeSerializeRotatorArrayAsync)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_STRUCT(FLatentActionInfo,Z_Param_LatentInfo);
		P_GET_PROPERTY(FStrProperty,Z_Param_stringToDeSerialize);
		P_GET_TARRAY_REF(FRotator,Z_Param_Out_deSerializedResult);
		P_FINISH;
		P_NATIVE_BEGIN;
		URapidJsonUEFunctionLibrary::DeSerializeRotatorArrayAsync(Z_Param_WorldContextObject,Z_Param_LatentInfo,Z_Param_stringToDeSerialize,Z_Param_Out_deSerializedResult);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execSerializeRotatorArrayAsync)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_STRUCT(FLatentActionInfo,Z_Param_LatentInfo);
		P_GET_TARRAY(FRotator,Z_Param_arrayToSerialize);
		P_GET_PROPERTY_REF(FStrProperty,Z_Param_Out_serializedString);
		P_FINISH;
		P_NATIVE_BEGIN;
		URapidJsonUEFunctionLibrary::SerializeRotatorArrayAsync(Z_Param_WorldContextObject,Z_Param_LatentInfo,Z_Param_arrayToSerialize,Z_Param_Out_serializedString);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execDeSerializeVectorArrayAsync)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_STRUCT(FLatentActionInfo,Z_Param_LatentInfo);
		P_GET_PROPERTY(FStrProperty,Z_Param_stringToDeSerialize);
		P_GET_TARRAY_REF(FVector,Z_Param_Out_deSerializedResult);
		P_FINISH;
		P_NATIVE_BEGIN;
		URapidJsonUEFunctionLibrary::DeSerializeVectorArrayAsync(Z_Param_WorldContextObject,Z_Param_LatentInfo,Z_Param_stringToDeSerialize,Z_Param_Out_deSerializedResult);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execSerializeVectorArrayAsync)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_STRUCT(FLatentActionInfo,Z_Param_LatentInfo);
		P_GET_TARRAY(FVector,Z_Param_arrayToSerialize);
		P_GET_PROPERTY_REF(FStrProperty,Z_Param_Out_serializedString);
		P_FINISH;
		P_NATIVE_BEGIN;
		URapidJsonUEFunctionLibrary::SerializeVectorArrayAsync(Z_Param_WorldContextObject,Z_Param_LatentInfo,Z_Param_arrayToSerialize,Z_Param_Out_serializedString);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execDeSerializeFloatArrayAsync)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_STRUCT(FLatentActionInfo,Z_Param_LatentInfo);
		P_GET_PROPERTY(FStrProperty,Z_Param_stringToDeSerialize);
		P_GET_TARRAY_REF(float,Z_Param_Out_deSerializedResult);
		P_FINISH;
		P_NATIVE_BEGIN;
		URapidJsonUEFunctionLibrary::DeSerializeFloatArrayAsync(Z_Param_WorldContextObject,Z_Param_LatentInfo,Z_Param_stringToDeSerialize,Z_Param_Out_deSerializedResult);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execSerializeFloatArrayAsync)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_STRUCT(FLatentActionInfo,Z_Param_LatentInfo);
		P_GET_TARRAY(float,Z_Param_arrayToSerialize);
		P_GET_PROPERTY_REF(FStrProperty,Z_Param_Out_serializedString);
		P_FINISH;
		P_NATIVE_BEGIN;
		URapidJsonUEFunctionLibrary::SerializeFloatArrayAsync(Z_Param_WorldContextObject,Z_Param_LatentInfo,Z_Param_arrayToSerialize,Z_Param_Out_serializedString);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execDeSerializeTextArrayAsync)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_STRUCT(FLatentActionInfo,Z_Param_LatentInfo);
		P_GET_PROPERTY(FStrProperty,Z_Param_stringToDeSerialize);
		P_GET_TARRAY_REF(FText,Z_Param_Out_deSerializedResult);
		P_FINISH;
		P_NATIVE_BEGIN;
		URapidJsonUEFunctionLibrary::DeSerializeTextArrayAsync(Z_Param_WorldContextObject,Z_Param_LatentInfo,Z_Param_stringToDeSerialize,Z_Param_Out_deSerializedResult);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execSerializeTextArrayAsync)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_STRUCT(FLatentActionInfo,Z_Param_LatentInfo);
		P_GET_TARRAY(FText,Z_Param_arrayToSerialize);
		P_GET_PROPERTY_REF(FStrProperty,Z_Param_Out_serializedString);
		P_FINISH;
		P_NATIVE_BEGIN;
		URapidJsonUEFunctionLibrary::SerializeTextArrayAsync(Z_Param_WorldContextObject,Z_Param_LatentInfo,Z_Param_arrayToSerialize,Z_Param_Out_serializedString);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execDeSerializeNameArrayAsync)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_STRUCT(FLatentActionInfo,Z_Param_LatentInfo);
		P_GET_PROPERTY(FStrProperty,Z_Param_stringToDeSerialize);
		P_GET_TARRAY_REF(FName,Z_Param_Out_deSerializedResult);
		P_FINISH;
		P_NATIVE_BEGIN;
		URapidJsonUEFunctionLibrary::DeSerializeNameArrayAsync(Z_Param_WorldContextObject,Z_Param_LatentInfo,Z_Param_stringToDeSerialize,Z_Param_Out_deSerializedResult);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execSerializeNameArrayAsync)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_STRUCT(FLatentActionInfo,Z_Param_LatentInfo);
		P_GET_TARRAY(FName,Z_Param_arrayToSerialize);
		P_GET_PROPERTY_REF(FStrProperty,Z_Param_Out_serializedString);
		P_FINISH;
		P_NATIVE_BEGIN;
		URapidJsonUEFunctionLibrary::SerializeNameArrayAsync(Z_Param_WorldContextObject,Z_Param_LatentInfo,Z_Param_arrayToSerialize,Z_Param_Out_serializedString);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execDeSerializeBoolArrayAsync)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_STRUCT(FLatentActionInfo,Z_Param_LatentInfo);
		P_GET_PROPERTY(FStrProperty,Z_Param_stringToDeSerialize);
		P_GET_TARRAY_REF(bool,Z_Param_Out_deSerializedResult);
		P_FINISH;
		P_NATIVE_BEGIN;
		URapidJsonUEFunctionLibrary::DeSerializeBoolArrayAsync(Z_Param_WorldContextObject,Z_Param_LatentInfo,Z_Param_stringToDeSerialize,Z_Param_Out_deSerializedResult);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execSerializeBoolArrayAsync)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_STRUCT(FLatentActionInfo,Z_Param_LatentInfo);
		P_GET_TARRAY(bool,Z_Param_arrayToSerialize);
		P_GET_PROPERTY_REF(FStrProperty,Z_Param_Out_serializedString);
		P_FINISH;
		P_NATIVE_BEGIN;
		URapidJsonUEFunctionLibrary::SerializeBoolArrayAsync(Z_Param_WorldContextObject,Z_Param_LatentInfo,Z_Param_arrayToSerialize,Z_Param_Out_serializedString);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execDeSerializeColorArrayAsync)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_STRUCT(FLatentActionInfo,Z_Param_LatentInfo);
		P_GET_PROPERTY(FStrProperty,Z_Param_stringToDeSerialize);
		P_GET_TARRAY_REF(FColor,Z_Param_Out_deSerializedResult);
		P_FINISH;
		P_NATIVE_BEGIN;
		URapidJsonUEFunctionLibrary::DeSerializeColorArrayAsync(Z_Param_WorldContextObject,Z_Param_LatentInfo,Z_Param_stringToDeSerialize,Z_Param_Out_deSerializedResult);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execSerializeColorArrayAsync)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_STRUCT(FLatentActionInfo,Z_Param_LatentInfo);
		P_GET_TARRAY(FColor,Z_Param_arrayToSerialize);
		P_GET_PROPERTY_REF(FStrProperty,Z_Param_Out_serializedString);
		P_FINISH;
		P_NATIVE_BEGIN;
		URapidJsonUEFunctionLibrary::SerializeColorArrayAsync(Z_Param_WorldContextObject,Z_Param_LatentInfo,Z_Param_arrayToSerialize,Z_Param_Out_serializedString);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execDeSerializeStringArrayAsync)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_STRUCT(FLatentActionInfo,Z_Param_LatentInfo);
		P_GET_PROPERTY(FStrProperty,Z_Param_stringToDeSerialize);
		P_GET_TARRAY_REF(FString,Z_Param_Out_deSerializedResult);
		P_FINISH;
		P_NATIVE_BEGIN;
		URapidJsonUEFunctionLibrary::DeSerializeStringArrayAsync(Z_Param_WorldContextObject,Z_Param_LatentInfo,Z_Param_stringToDeSerialize,Z_Param_Out_deSerializedResult);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execSerializeStringArrayAsync)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_STRUCT(FLatentActionInfo,Z_Param_LatentInfo);
		P_GET_TARRAY(FString,Z_Param_arrayToSerialize);
		P_GET_PROPERTY_REF(FStrProperty,Z_Param_Out_serializedString);
		P_FINISH;
		P_NATIVE_BEGIN;
		URapidJsonUEFunctionLibrary::SerializeStringArrayAsync(Z_Param_WorldContextObject,Z_Param_LatentInfo,Z_Param_arrayToSerialize,Z_Param_Out_serializedString);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execDeSerializeInt64ArrayAsync)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_STRUCT(FLatentActionInfo,Z_Param_LatentInfo);
		P_GET_PROPERTY(FStrProperty,Z_Param_stringToDeSerialize);
		P_GET_TARRAY_REF(int64,Z_Param_Out_deSerializedResult);
		P_FINISH;
		P_NATIVE_BEGIN;
		URapidJsonUEFunctionLibrary::DeSerializeInt64ArrayAsync(Z_Param_WorldContextObject,Z_Param_LatentInfo,Z_Param_stringToDeSerialize,Z_Param_Out_deSerializedResult);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execSerializeInt64ArrayAsync)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_STRUCT(FLatentActionInfo,Z_Param_LatentInfo);
		P_GET_TARRAY(int64,Z_Param_arrayToSerialize);
		P_GET_PROPERTY_REF(FStrProperty,Z_Param_Out_serializedString);
		P_FINISH;
		P_NATIVE_BEGIN;
		URapidJsonUEFunctionLibrary::SerializeInt64ArrayAsync(Z_Param_WorldContextObject,Z_Param_LatentInfo,Z_Param_arrayToSerialize,Z_Param_Out_serializedString);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execDeSerializeUInt8ArrayAsync)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_STRUCT(FLatentActionInfo,Z_Param_LatentInfo);
		P_GET_PROPERTY(FStrProperty,Z_Param_stringToDeSerialize);
		P_GET_TARRAY_REF(uint8,Z_Param_Out_deSerializedResult);
		P_FINISH;
		P_NATIVE_BEGIN;
		URapidJsonUEFunctionLibrary::DeSerializeUInt8ArrayAsync(Z_Param_WorldContextObject,Z_Param_LatentInfo,Z_Param_stringToDeSerialize,Z_Param_Out_deSerializedResult);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execSerializeUint8ArrayAsync)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_STRUCT(FLatentActionInfo,Z_Param_LatentInfo);
		P_GET_TARRAY(uint8,Z_Param_arrayToSerialize);
		P_GET_PROPERTY_REF(FStrProperty,Z_Param_Out_serializedString);
		P_FINISH;
		P_NATIVE_BEGIN;
		URapidJsonUEFunctionLibrary::SerializeUint8ArrayAsync(Z_Param_WorldContextObject,Z_Param_LatentInfo,Z_Param_arrayToSerialize,Z_Param_Out_serializedString);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execDeSerializeInt32ArrayAsync)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_STRUCT(FLatentActionInfo,Z_Param_LatentInfo);
		P_GET_PROPERTY(FStrProperty,Z_Param_stringToDeSerialize);
		P_GET_TARRAY_REF(int32,Z_Param_Out_deSerializedResult);
		P_FINISH;
		P_NATIVE_BEGIN;
		URapidJsonUEFunctionLibrary::DeSerializeInt32ArrayAsync(Z_Param_WorldContextObject,Z_Param_LatentInfo,Z_Param_stringToDeSerialize,Z_Param_Out_deSerializedResult);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URapidJsonUEFunctionLibrary::execSerializeInt32ArrayAsync)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_STRUCT(FLatentActionInfo,Z_Param_LatentInfo);
		P_GET_TARRAY(int32,Z_Param_arrayToSerialize);
		P_GET_PROPERTY_REF(FStrProperty,Z_Param_Out_serializedString);
		P_FINISH;
		P_NATIVE_BEGIN;
		URapidJsonUEFunctionLibrary::SerializeInt32ArrayAsync(Z_Param_WorldContextObject,Z_Param_LatentInfo,Z_Param_arrayToSerialize,Z_Param_Out_serializedString);
		P_NATIVE_END;
	}
	void URapidJsonUEFunctionLibrary::StaticRegisterNativesURapidJsonUEFunctionLibrary()
	{
		UClass* Class = URapidJsonUEFunctionLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "DeserializeBool_Wrapper", &URapidJsonUEFunctionLibrary::execDeserializeBool_Wrapper },
			{ "DeSerializeBoolArrayAsync", &URapidJsonUEFunctionLibrary::execDeSerializeBoolArrayAsync },
			{ "DeSerializeColorArrayAsync", &URapidJsonUEFunctionLibrary::execDeSerializeColorArrayAsync },
			{ "DeserializeFColor_Wrapper", &URapidJsonUEFunctionLibrary::execDeserializeFColor_Wrapper },
			{ "DeserializeFloat_Wrapper", &URapidJsonUEFunctionLibrary::execDeserializeFloat_Wrapper },
			{ "DeSerializeFloatArrayAsync", &URapidJsonUEFunctionLibrary::execDeSerializeFloatArrayAsync },
			{ "DeserializeFName_Wrapper", &URapidJsonUEFunctionLibrary::execDeserializeFName_Wrapper },
			{ "DeserializeFRotator_Wrapper", &URapidJsonUEFunctionLibrary::execDeserializeFRotator_Wrapper },
			{ "DeserializeFString_Wrapper", &URapidJsonUEFunctionLibrary::execDeserializeFString_Wrapper },
			{ "DeserializeFText_Wrapper", &URapidJsonUEFunctionLibrary::execDeserializeFText_Wrapper },
			{ "DeserializeFTransform_Wrapper", &URapidJsonUEFunctionLibrary::execDeserializeFTransform_Wrapper },
			{ "DeserializeFVector_Wrapper", &URapidJsonUEFunctionLibrary::execDeserializeFVector_Wrapper },
			{ "DeSerializeInt32ArrayAsync", &URapidJsonUEFunctionLibrary::execDeSerializeInt32ArrayAsync },
			{ "DeserializeInt64_Wrapper", &URapidJsonUEFunctionLibrary::execDeserializeInt64_Wrapper },
			{ "DeSerializeInt64ArrayAsync", &URapidJsonUEFunctionLibrary::execDeSerializeInt64ArrayAsync },
			{ "DeserializeInt_Wrapper", &URapidJsonUEFunctionLibrary::execDeserializeInt_Wrapper },
			{ "DeSerializeNameArrayAsync", &URapidJsonUEFunctionLibrary::execDeSerializeNameArrayAsync },
			{ "DeSerializeRotatorArrayAsync", &URapidJsonUEFunctionLibrary::execDeSerializeRotatorArrayAsync },
			{ "DeSerializeStringArrayAsync", &URapidJsonUEFunctionLibrary::execDeSerializeStringArrayAsync },
			{ "DeserializeTArrayBool_Wrapper", &URapidJsonUEFunctionLibrary::execDeserializeTArrayBool_Wrapper },
			{ "DeserializeTArrayFColor_Wrapper", &URapidJsonUEFunctionLibrary::execDeserializeTArrayFColor_Wrapper },
			{ "DeserializeTArrayFloat_Wrapper", &URapidJsonUEFunctionLibrary::execDeserializeTArrayFloat_Wrapper },
			{ "DeserializeTArrayFName_Wrapper", &URapidJsonUEFunctionLibrary::execDeserializeTArrayFName_Wrapper },
			{ "DeserializeTArrayFRotator_Wrapper", &URapidJsonUEFunctionLibrary::execDeserializeTArrayFRotator_Wrapper },
			{ "DeserializeTArrayFString_Wrapper", &URapidJsonUEFunctionLibrary::execDeserializeTArrayFString_Wrapper },
			{ "DeserializeTArrayFText_Wrapper", &URapidJsonUEFunctionLibrary::execDeserializeTArrayFText_Wrapper },
			{ "DeserializeTArrayFTransform_Wrapper", &URapidJsonUEFunctionLibrary::execDeserializeTArrayFTransform_Wrapper },
			{ "DeserializeTArrayFVector_Wrapper", &URapidJsonUEFunctionLibrary::execDeserializeTArrayFVector_Wrapper },
			{ "DeserializeTArrayInt64_Wrapper", &URapidJsonUEFunctionLibrary::execDeserializeTArrayInt64_Wrapper },
			{ "DeserializeTArrayInt_Wrapper", &URapidJsonUEFunctionLibrary::execDeserializeTArrayInt_Wrapper },
			{ "DeserializeTArrayUInt8_Wrapper", &URapidJsonUEFunctionLibrary::execDeserializeTArrayUInt8_Wrapper },
			{ "DeSerializeTextArrayAsync", &URapidJsonUEFunctionLibrary::execDeSerializeTextArrayAsync },
			{ "DeSerializeTransformArrayAsync", &URapidJsonUEFunctionLibrary::execDeSerializeTransformArrayAsync },
			{ "DeserializeUInt8_Wrapper", &URapidJsonUEFunctionLibrary::execDeserializeUInt8_Wrapper },
			{ "DeSerializeUInt8ArrayAsync", &URapidJsonUEFunctionLibrary::execDeSerializeUInt8ArrayAsync },
			{ "DeSerializeVectorArrayAsync", &URapidJsonUEFunctionLibrary::execDeSerializeVectorArrayAsync },
			{ "SerializeBool_Wrapper", &URapidJsonUEFunctionLibrary::execSerializeBool_Wrapper },
			{ "SerializeBoolArrayAsync", &URapidJsonUEFunctionLibrary::execSerializeBoolArrayAsync },
			{ "SerializeColorArrayAsync", &URapidJsonUEFunctionLibrary::execSerializeColorArrayAsync },
			{ "SerializeFColor_Wrapper", &URapidJsonUEFunctionLibrary::execSerializeFColor_Wrapper },
			{ "SerializeFloat_Wrapper", &URapidJsonUEFunctionLibrary::execSerializeFloat_Wrapper },
			{ "SerializeFloatArrayAsync", &URapidJsonUEFunctionLibrary::execSerializeFloatArrayAsync },
			{ "SerializeFName_Wrapper", &URapidJsonUEFunctionLibrary::execSerializeFName_Wrapper },
			{ "SerializeFRotator_Wrapper", &URapidJsonUEFunctionLibrary::execSerializeFRotator_Wrapper },
			{ "SerializeFString_Wrapper", &URapidJsonUEFunctionLibrary::execSerializeFString_Wrapper },
			{ "SerializeFText_Wrapper", &URapidJsonUEFunctionLibrary::execSerializeFText_Wrapper },
			{ "SerializeFTransform_Wrapper", &URapidJsonUEFunctionLibrary::execSerializeFTransform_Wrapper },
			{ "SerializeFVector_Wrapper", &URapidJsonUEFunctionLibrary::execSerializeFVector_Wrapper },
			{ "SerializeInt32ArrayAsync", &URapidJsonUEFunctionLibrary::execSerializeInt32ArrayAsync },
			{ "SerializeInt64_Wrapper", &URapidJsonUEFunctionLibrary::execSerializeInt64_Wrapper },
			{ "SerializeInt64ArrayAsync", &URapidJsonUEFunctionLibrary::execSerializeInt64ArrayAsync },
			{ "SerializeInt_Wrapper", &URapidJsonUEFunctionLibrary::execSerializeInt_Wrapper },
			{ "SerializeNameArrayAsync", &URapidJsonUEFunctionLibrary::execSerializeNameArrayAsync },
			{ "SerializeRotatorArrayAsync", &URapidJsonUEFunctionLibrary::execSerializeRotatorArrayAsync },
			{ "SerializeStringArrayAsync", &URapidJsonUEFunctionLibrary::execSerializeStringArrayAsync },
			{ "SerializeTArrayBool_Wrapper", &URapidJsonUEFunctionLibrary::execSerializeTArrayBool_Wrapper },
			{ "SerializeTArrayFColor_Wrapper", &URapidJsonUEFunctionLibrary::execSerializeTArrayFColor_Wrapper },
			{ "SerializeTArrayFloat_Wrapper", &URapidJsonUEFunctionLibrary::execSerializeTArrayFloat_Wrapper },
			{ "SerializeTArrayFName_Wrapper", &URapidJsonUEFunctionLibrary::execSerializeTArrayFName_Wrapper },
			{ "SerializeTArrayFRotator_Wrapper", &URapidJsonUEFunctionLibrary::execSerializeTArrayFRotator_Wrapper },
			{ "SerializeTArrayFString_Wrapper", &URapidJsonUEFunctionLibrary::execSerializeTArrayFString_Wrapper },
			{ "SerializeTArrayFText_Wrapper", &URapidJsonUEFunctionLibrary::execSerializeTArrayFText_Wrapper },
			{ "SerializeTArrayFTransform_Wrapper", &URapidJsonUEFunctionLibrary::execSerializeTArrayFTransform_Wrapper },
			{ "SerializeTArrayFVector_Wrapper", &URapidJsonUEFunctionLibrary::execSerializeTArrayFVector_Wrapper },
			{ "SerializeTArrayInt64_Wrapper", &URapidJsonUEFunctionLibrary::execSerializeTArrayInt64_Wrapper },
			{ "SerializeTArrayInt_Wrapper", &URapidJsonUEFunctionLibrary::execSerializeTArrayInt_Wrapper },
			{ "SerializeTArrayUInt8_Wrapper", &URapidJsonUEFunctionLibrary::execSerializeTArrayUInt8_Wrapper },
			{ "SerializeTextArrayAsync", &URapidJsonUEFunctionLibrary::execSerializeTextArrayAsync },
			{ "SerializeTransformArrayAsync", &URapidJsonUEFunctionLibrary::execSerializeTransformArrayAsync },
			{ "SerializeUInt8_Wrapper", &URapidJsonUEFunctionLibrary::execSerializeUInt8_Wrapper },
			{ "SerializeUint8ArrayAsync", &URapidJsonUEFunctionLibrary::execSerializeUint8ArrayAsync },
			{ "SerializeVectorArrayAsync", &URapidJsonUEFunctionLibrary::execSerializeVectorArrayAsync },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeBool_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventDeserializeBool_Wrapper_Parms
		{
			FString JsonString;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_JsonString_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_JsonString;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeBool_Wrapper_Statics::NewProp_JsonString_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeBool_Wrapper_Statics::NewProp_JsonString = { "JsonString", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeBool_Wrapper_Parms, JsonString), METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeBool_Wrapper_Statics::NewProp_JsonString_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeBool_Wrapper_Statics::NewProp_JsonString_MetaData)) };
	void Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeBool_Wrapper_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((RapidJsonUEFunctionLibrary_eventDeserializeBool_Wrapper_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeBool_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(RapidJsonUEFunctionLibrary_eventDeserializeBool_Wrapper_Parms), &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeBool_Wrapper_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeBool_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeBool_Wrapper_Statics::NewProp_JsonString,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeBool_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeBool_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|De-Serialize" },
		{ "DisplayName", "De-Serialize Bool" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeBool_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "DeserializeBool_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeBool_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventDeserializeBool_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeBool_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeBool_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeBool_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeBool_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeBool_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeBool_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeBoolArrayAsync_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventDeSerializeBoolArrayAsync_Parms
		{
			UObject* WorldContextObject;
			FLatentActionInfo LatentInfo;
			FString stringToDeSerialize;
			TArray<bool> deSerializedResult;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UECodeGen_Private::FStructPropertyParams NewProp_LatentInfo;
		static const UECodeGen_Private::FStrPropertyParams NewProp_stringToDeSerialize;
		static const UECodeGen_Private::FBoolPropertyParams NewProp_deSerializedResult_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_deSerializedResult;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeBoolArrayAsync_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeBoolArrayAsync_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeBoolArrayAsync_Statics::NewProp_LatentInfo = { "LatentInfo", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeBoolArrayAsync_Parms, LatentInfo), Z_Construct_UScriptStruct_FLatentActionInfo, METADATA_PARAMS(nullptr, 0) }; // 2194022737
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeBoolArrayAsync_Statics::NewProp_stringToDeSerialize = { "stringToDeSerialize", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeBoolArrayAsync_Parms, stringToDeSerialize), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeBoolArrayAsync_Statics::NewProp_deSerializedResult_Inner = { "deSerializedResult", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeBoolArrayAsync_Statics::NewProp_deSerializedResult = { "deSerializedResult", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeBoolArrayAsync_Parms, deSerializedResult), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeBoolArrayAsync_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeBoolArrayAsync_Statics::NewProp_WorldContextObject,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeBoolArrayAsync_Statics::NewProp_LatentInfo,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeBoolArrayAsync_Statics::NewProp_stringToDeSerialize,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeBoolArrayAsync_Statics::NewProp_deSerializedResult_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeBoolArrayAsync_Statics::NewProp_deSerializedResult,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeBoolArrayAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|De-Serialize Array" },
		{ "Latent", "" },
		{ "LatentInfo", "LatentInfo" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
		{ "ToolTip", "De-Serializes the Array using Async Task so eventhough it's very large the FPS doesn't get affected since it's not run on the Game Thread" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeBoolArrayAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "DeSerializeBoolArrayAsync", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeBoolArrayAsync_Statics::RapidJsonUEFunctionLibrary_eventDeSerializeBoolArrayAsync_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeBoolArrayAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeBoolArrayAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeBoolArrayAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeBoolArrayAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeBoolArrayAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeBoolArrayAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeColorArrayAsync_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventDeSerializeColorArrayAsync_Parms
		{
			UObject* WorldContextObject;
			FLatentActionInfo LatentInfo;
			FString stringToDeSerialize;
			TArray<FColor> deSerializedResult;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UECodeGen_Private::FStructPropertyParams NewProp_LatentInfo;
		static const UECodeGen_Private::FStrPropertyParams NewProp_stringToDeSerialize;
		static const UECodeGen_Private::FStructPropertyParams NewProp_deSerializedResult_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_deSerializedResult;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeColorArrayAsync_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeColorArrayAsync_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeColorArrayAsync_Statics::NewProp_LatentInfo = { "LatentInfo", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeColorArrayAsync_Parms, LatentInfo), Z_Construct_UScriptStruct_FLatentActionInfo, METADATA_PARAMS(nullptr, 0) }; // 2194022737
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeColorArrayAsync_Statics::NewProp_stringToDeSerialize = { "stringToDeSerialize", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeColorArrayAsync_Parms, stringToDeSerialize), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeColorArrayAsync_Statics::NewProp_deSerializedResult_Inner = { "deSerializedResult", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeColorArrayAsync_Statics::NewProp_deSerializedResult = { "deSerializedResult", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeColorArrayAsync_Parms, deSerializedResult), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeColorArrayAsync_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeColorArrayAsync_Statics::NewProp_WorldContextObject,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeColorArrayAsync_Statics::NewProp_LatentInfo,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeColorArrayAsync_Statics::NewProp_stringToDeSerialize,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeColorArrayAsync_Statics::NewProp_deSerializedResult_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeColorArrayAsync_Statics::NewProp_deSerializedResult,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeColorArrayAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|De-Serialize Array" },
		{ "Latent", "" },
		{ "LatentInfo", "LatentInfo" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
		{ "ToolTip", "De-Serializes the Array using Async Task so eventhough it's very large the FPS doesn't get affected since it's not run on the Game Thread" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeColorArrayAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "DeSerializeColorArrayAsync", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeColorArrayAsync_Statics::RapidJsonUEFunctionLibrary_eventDeSerializeColorArrayAsync_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeColorArrayAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeColorArrayAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeColorArrayAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeColorArrayAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeColorArrayAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeColorArrayAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFColor_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventDeserializeFColor_Wrapper_Parms
		{
			FString JsonString;
			FColor ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_JsonString_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_JsonString;
		static const UECodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFColor_Wrapper_Statics::NewProp_JsonString_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFColor_Wrapper_Statics::NewProp_JsonString = { "JsonString", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeFColor_Wrapper_Parms, JsonString), METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFColor_Wrapper_Statics::NewProp_JsonString_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFColor_Wrapper_Statics::NewProp_JsonString_MetaData)) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFColor_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeFColor_Wrapper_Parms, ReturnValue), Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFColor_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFColor_Wrapper_Statics::NewProp_JsonString,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFColor_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFColor_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|De-Serialize" },
		{ "DisplayName", "De-Serialize Color" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFColor_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "DeserializeFColor_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFColor_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventDeserializeFColor_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFColor_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFColor_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04822401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFColor_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFColor_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFColor_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFColor_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFloat_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventDeserializeFloat_Wrapper_Parms
		{
			FString JsonString;
			float ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_JsonString_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_JsonString;
		static const UECodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFloat_Wrapper_Statics::NewProp_JsonString_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFloat_Wrapper_Statics::NewProp_JsonString = { "JsonString", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeFloat_Wrapper_Parms, JsonString), METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFloat_Wrapper_Statics::NewProp_JsonString_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFloat_Wrapper_Statics::NewProp_JsonString_MetaData)) };
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFloat_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeFloat_Wrapper_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFloat_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFloat_Wrapper_Statics::NewProp_JsonString,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFloat_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFloat_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|De-Serialize" },
		{ "DisplayName", "De-Serialize Float" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFloat_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "DeserializeFloat_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFloat_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventDeserializeFloat_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFloat_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFloat_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFloat_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFloat_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFloat_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFloat_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeFloatArrayAsync_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventDeSerializeFloatArrayAsync_Parms
		{
			UObject* WorldContextObject;
			FLatentActionInfo LatentInfo;
			FString stringToDeSerialize;
			TArray<float> deSerializedResult;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UECodeGen_Private::FStructPropertyParams NewProp_LatentInfo;
		static const UECodeGen_Private::FStrPropertyParams NewProp_stringToDeSerialize;
		static const UECodeGen_Private::FFloatPropertyParams NewProp_deSerializedResult_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_deSerializedResult;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeFloatArrayAsync_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeFloatArrayAsync_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeFloatArrayAsync_Statics::NewProp_LatentInfo = { "LatentInfo", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeFloatArrayAsync_Parms, LatentInfo), Z_Construct_UScriptStruct_FLatentActionInfo, METADATA_PARAMS(nullptr, 0) }; // 2194022737
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeFloatArrayAsync_Statics::NewProp_stringToDeSerialize = { "stringToDeSerialize", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeFloatArrayAsync_Parms, stringToDeSerialize), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeFloatArrayAsync_Statics::NewProp_deSerializedResult_Inner = { "deSerializedResult", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeFloatArrayAsync_Statics::NewProp_deSerializedResult = { "deSerializedResult", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeFloatArrayAsync_Parms, deSerializedResult), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeFloatArrayAsync_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeFloatArrayAsync_Statics::NewProp_WorldContextObject,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeFloatArrayAsync_Statics::NewProp_LatentInfo,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeFloatArrayAsync_Statics::NewProp_stringToDeSerialize,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeFloatArrayAsync_Statics::NewProp_deSerializedResult_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeFloatArrayAsync_Statics::NewProp_deSerializedResult,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeFloatArrayAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|De-Serialize Array" },
		{ "Latent", "" },
		{ "LatentInfo", "LatentInfo" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
		{ "ToolTip", "De-Serializes the Array using Async Task so eventhough it's very large the FPS doesn't get affected since it's not run on the Game Thread" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeFloatArrayAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "DeSerializeFloatArrayAsync", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeFloatArrayAsync_Statics::RapidJsonUEFunctionLibrary_eventDeSerializeFloatArrayAsync_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeFloatArrayAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeFloatArrayAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeFloatArrayAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeFloatArrayAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeFloatArrayAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeFloatArrayAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFName_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventDeserializeFName_Wrapper_Parms
		{
			FString JsonString;
			FName ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_JsonString_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_JsonString;
		static const UECodeGen_Private::FNamePropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFName_Wrapper_Statics::NewProp_JsonString_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFName_Wrapper_Statics::NewProp_JsonString = { "JsonString", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeFName_Wrapper_Parms, JsonString), METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFName_Wrapper_Statics::NewProp_JsonString_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFName_Wrapper_Statics::NewProp_JsonString_MetaData)) };
	const UECodeGen_Private::FNamePropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFName_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeFName_Wrapper_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFName_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFName_Wrapper_Statics::NewProp_JsonString,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFName_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFName_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|De-Serialize" },
		{ "DisplayName", "De-Serialize Name" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFName_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "DeserializeFName_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFName_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventDeserializeFName_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFName_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFName_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFName_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFName_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFName_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFName_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFRotator_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventDeserializeFRotator_Wrapper_Parms
		{
			FString JsonString;
			FRotator ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_JsonString_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_JsonString;
		static const UECodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFRotator_Wrapper_Statics::NewProp_JsonString_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFRotator_Wrapper_Statics::NewProp_JsonString = { "JsonString", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeFRotator_Wrapper_Parms, JsonString), METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFRotator_Wrapper_Statics::NewProp_JsonString_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFRotator_Wrapper_Statics::NewProp_JsonString_MetaData)) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFRotator_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeFRotator_Wrapper_Parms, ReturnValue), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFRotator_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFRotator_Wrapper_Statics::NewProp_JsonString,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFRotator_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFRotator_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|De-Serialize" },
		{ "DisplayName", "De-Serialize Rotator" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFRotator_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "DeserializeFRotator_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFRotator_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventDeserializeFRotator_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFRotator_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFRotator_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04822401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFRotator_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFRotator_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFRotator_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFRotator_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFString_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventDeserializeFString_Wrapper_Parms
		{
			FString JsonString;
			FString ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_JsonString_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_JsonString;
		static const UECodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFString_Wrapper_Statics::NewProp_JsonString_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFString_Wrapper_Statics::NewProp_JsonString = { "JsonString", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeFString_Wrapper_Parms, JsonString), METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFString_Wrapper_Statics::NewProp_JsonString_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFString_Wrapper_Statics::NewProp_JsonString_MetaData)) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFString_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeFString_Wrapper_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFString_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFString_Wrapper_Statics::NewProp_JsonString,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFString_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFString_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|De-Serialize" },
		{ "DisplayName", "De-Serialize String" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFString_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "DeserializeFString_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFString_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventDeserializeFString_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFString_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFString_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFString_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFString_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFString_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFString_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFText_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventDeserializeFText_Wrapper_Parms
		{
			FString JsonString;
			FText ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_JsonString_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_JsonString;
		static const UECodeGen_Private::FTextPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFText_Wrapper_Statics::NewProp_JsonString_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFText_Wrapper_Statics::NewProp_JsonString = { "JsonString", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeFText_Wrapper_Parms, JsonString), METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFText_Wrapper_Statics::NewProp_JsonString_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFText_Wrapper_Statics::NewProp_JsonString_MetaData)) };
	const UECodeGen_Private::FTextPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFText_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeFText_Wrapper_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFText_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFText_Wrapper_Statics::NewProp_JsonString,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFText_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFText_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|De-Serialize" },
		{ "DisplayName", "De-Serialize Text" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFText_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "DeserializeFText_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFText_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventDeserializeFText_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFText_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFText_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFText_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFText_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFText_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFText_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFTransform_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventDeserializeFTransform_Wrapper_Parms
		{
			FString JsonString;
			FTransform ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_JsonString_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_JsonString;
		static const UECodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFTransform_Wrapper_Statics::NewProp_JsonString_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFTransform_Wrapper_Statics::NewProp_JsonString = { "JsonString", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeFTransform_Wrapper_Parms, JsonString), METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFTransform_Wrapper_Statics::NewProp_JsonString_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFTransform_Wrapper_Statics::NewProp_JsonString_MetaData)) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFTransform_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeFTransform_Wrapper_Parms, ReturnValue), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFTransform_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFTransform_Wrapper_Statics::NewProp_JsonString,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFTransform_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFTransform_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|De-Serialize" },
		{ "DisplayName", "De-Serialize Transform" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFTransform_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "DeserializeFTransform_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFTransform_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventDeserializeFTransform_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFTransform_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFTransform_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04822401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFTransform_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFTransform_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFTransform_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFTransform_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFVector_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventDeserializeFVector_Wrapper_Parms
		{
			FString JsonString;
			FVector ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_JsonString_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_JsonString;
		static const UECodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFVector_Wrapper_Statics::NewProp_JsonString_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFVector_Wrapper_Statics::NewProp_JsonString = { "JsonString", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeFVector_Wrapper_Parms, JsonString), METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFVector_Wrapper_Statics::NewProp_JsonString_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFVector_Wrapper_Statics::NewProp_JsonString_MetaData)) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFVector_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeFVector_Wrapper_Parms, ReturnValue), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFVector_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFVector_Wrapper_Statics::NewProp_JsonString,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFVector_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFVector_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|De-Serialize" },
		{ "DisplayName", "De-Serialize Vector" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFVector_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "DeserializeFVector_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFVector_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventDeserializeFVector_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFVector_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFVector_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04822401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFVector_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFVector_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFVector_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFVector_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt32ArrayAsync_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventDeSerializeInt32ArrayAsync_Parms
		{
			UObject* WorldContextObject;
			FLatentActionInfo LatentInfo;
			FString stringToDeSerialize;
			TArray<int32> deSerializedResult;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UECodeGen_Private::FStructPropertyParams NewProp_LatentInfo;
		static const UECodeGen_Private::FStrPropertyParams NewProp_stringToDeSerialize;
		static const UECodeGen_Private::FIntPropertyParams NewProp_deSerializedResult_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_deSerializedResult;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt32ArrayAsync_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeInt32ArrayAsync_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt32ArrayAsync_Statics::NewProp_LatentInfo = { "LatentInfo", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeInt32ArrayAsync_Parms, LatentInfo), Z_Construct_UScriptStruct_FLatentActionInfo, METADATA_PARAMS(nullptr, 0) }; // 2194022737
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt32ArrayAsync_Statics::NewProp_stringToDeSerialize = { "stringToDeSerialize", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeInt32ArrayAsync_Parms, stringToDeSerialize), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FIntPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt32ArrayAsync_Statics::NewProp_deSerializedResult_Inner = { "deSerializedResult", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt32ArrayAsync_Statics::NewProp_deSerializedResult = { "deSerializedResult", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeInt32ArrayAsync_Parms, deSerializedResult), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt32ArrayAsync_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt32ArrayAsync_Statics::NewProp_WorldContextObject,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt32ArrayAsync_Statics::NewProp_LatentInfo,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt32ArrayAsync_Statics::NewProp_stringToDeSerialize,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt32ArrayAsync_Statics::NewProp_deSerializedResult_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt32ArrayAsync_Statics::NewProp_deSerializedResult,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt32ArrayAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|De-Serialize Array" },
		{ "Latent", "" },
		{ "LatentInfo", "LatentInfo" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
		{ "ToolTip", "De-Serializes the Array using Async Task so eventhough it's very large the FPS doesn't get affected since it's not run on the Game Thread" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt32ArrayAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "DeSerializeInt32ArrayAsync", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt32ArrayAsync_Statics::RapidJsonUEFunctionLibrary_eventDeSerializeInt32ArrayAsync_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt32ArrayAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt32ArrayAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt32ArrayAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt32ArrayAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt32ArrayAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt32ArrayAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeInt64_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventDeserializeInt64_Wrapper_Parms
		{
			FString JsonString;
			int64 ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_JsonString_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_JsonString;
		static const UECodeGen_Private::FInt64PropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeInt64_Wrapper_Statics::NewProp_JsonString_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeInt64_Wrapper_Statics::NewProp_JsonString = { "JsonString", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeInt64_Wrapper_Parms, JsonString), METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeInt64_Wrapper_Statics::NewProp_JsonString_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeInt64_Wrapper_Statics::NewProp_JsonString_MetaData)) };
	const UECodeGen_Private::FInt64PropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeInt64_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Int64, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeInt64_Wrapper_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeInt64_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeInt64_Wrapper_Statics::NewProp_JsonString,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeInt64_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeInt64_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|De-Serialize" },
		{ "DisplayName", "De-Serialize Int64" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeInt64_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "DeserializeInt64_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeInt64_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventDeserializeInt64_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeInt64_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeInt64_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeInt64_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeInt64_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeInt64_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeInt64_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt64ArrayAsync_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventDeSerializeInt64ArrayAsync_Parms
		{
			UObject* WorldContextObject;
			FLatentActionInfo LatentInfo;
			FString stringToDeSerialize;
			TArray<int64> deSerializedResult;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UECodeGen_Private::FStructPropertyParams NewProp_LatentInfo;
		static const UECodeGen_Private::FStrPropertyParams NewProp_stringToDeSerialize;
		static const UECodeGen_Private::FInt64PropertyParams NewProp_deSerializedResult_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_deSerializedResult;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt64ArrayAsync_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeInt64ArrayAsync_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt64ArrayAsync_Statics::NewProp_LatentInfo = { "LatentInfo", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeInt64ArrayAsync_Parms, LatentInfo), Z_Construct_UScriptStruct_FLatentActionInfo, METADATA_PARAMS(nullptr, 0) }; // 2194022737
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt64ArrayAsync_Statics::NewProp_stringToDeSerialize = { "stringToDeSerialize", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeInt64ArrayAsync_Parms, stringToDeSerialize), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FInt64PropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt64ArrayAsync_Statics::NewProp_deSerializedResult_Inner = { "deSerializedResult", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Int64, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt64ArrayAsync_Statics::NewProp_deSerializedResult = { "deSerializedResult", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeInt64ArrayAsync_Parms, deSerializedResult), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt64ArrayAsync_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt64ArrayAsync_Statics::NewProp_WorldContextObject,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt64ArrayAsync_Statics::NewProp_LatentInfo,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt64ArrayAsync_Statics::NewProp_stringToDeSerialize,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt64ArrayAsync_Statics::NewProp_deSerializedResult_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt64ArrayAsync_Statics::NewProp_deSerializedResult,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt64ArrayAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|De-Serialize Array" },
		{ "Latent", "" },
		{ "LatentInfo", "LatentInfo" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
		{ "ToolTip", "De-Serializes the Array using Async Task so eventhough it's very large the FPS doesn't get affected since it's not run on the Game Thread" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt64ArrayAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "DeSerializeInt64ArrayAsync", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt64ArrayAsync_Statics::RapidJsonUEFunctionLibrary_eventDeSerializeInt64ArrayAsync_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt64ArrayAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt64ArrayAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt64ArrayAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt64ArrayAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt64ArrayAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt64ArrayAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeInt_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventDeserializeInt_Wrapper_Parms
		{
			FString JsonString;
			int32 ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_JsonString_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_JsonString;
		static const UECodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeInt_Wrapper_Statics::NewProp_JsonString_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeInt_Wrapper_Statics::NewProp_JsonString = { "JsonString", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeInt_Wrapper_Parms, JsonString), METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeInt_Wrapper_Statics::NewProp_JsonString_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeInt_Wrapper_Statics::NewProp_JsonString_MetaData)) };
	const UECodeGen_Private::FIntPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeInt_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeInt_Wrapper_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeInt_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeInt_Wrapper_Statics::NewProp_JsonString,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeInt_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeInt_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|De-Serialize" },
		{ "Comment", "//------- De-SERIALIZE --------//\n" },
		{ "DisplayName", "De-Serialize Int" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
		{ "ToolTip", "------- De-SERIALIZE --------" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeInt_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "DeserializeInt_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeInt_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventDeserializeInt_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeInt_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeInt_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeInt_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeInt_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeInt_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeInt_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeNameArrayAsync_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventDeSerializeNameArrayAsync_Parms
		{
			UObject* WorldContextObject;
			FLatentActionInfo LatentInfo;
			FString stringToDeSerialize;
			TArray<FName> deSerializedResult;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UECodeGen_Private::FStructPropertyParams NewProp_LatentInfo;
		static const UECodeGen_Private::FStrPropertyParams NewProp_stringToDeSerialize;
		static const UECodeGen_Private::FNamePropertyParams NewProp_deSerializedResult_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_deSerializedResult;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeNameArrayAsync_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeNameArrayAsync_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeNameArrayAsync_Statics::NewProp_LatentInfo = { "LatentInfo", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeNameArrayAsync_Parms, LatentInfo), Z_Construct_UScriptStruct_FLatentActionInfo, METADATA_PARAMS(nullptr, 0) }; // 2194022737
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeNameArrayAsync_Statics::NewProp_stringToDeSerialize = { "stringToDeSerialize", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeNameArrayAsync_Parms, stringToDeSerialize), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FNamePropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeNameArrayAsync_Statics::NewProp_deSerializedResult_Inner = { "deSerializedResult", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeNameArrayAsync_Statics::NewProp_deSerializedResult = { "deSerializedResult", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeNameArrayAsync_Parms, deSerializedResult), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeNameArrayAsync_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeNameArrayAsync_Statics::NewProp_WorldContextObject,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeNameArrayAsync_Statics::NewProp_LatentInfo,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeNameArrayAsync_Statics::NewProp_stringToDeSerialize,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeNameArrayAsync_Statics::NewProp_deSerializedResult_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeNameArrayAsync_Statics::NewProp_deSerializedResult,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeNameArrayAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|De-Serialize Array" },
		{ "Latent", "" },
		{ "LatentInfo", "LatentInfo" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
		{ "ToolTip", "De-Serializes the Array using Async Task so eventhough it's very large the FPS doesn't get affected since it's not run on the Game Thread" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeNameArrayAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "DeSerializeNameArrayAsync", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeNameArrayAsync_Statics::RapidJsonUEFunctionLibrary_eventDeSerializeNameArrayAsync_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeNameArrayAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeNameArrayAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeNameArrayAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeNameArrayAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeNameArrayAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeNameArrayAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeRotatorArrayAsync_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventDeSerializeRotatorArrayAsync_Parms
		{
			UObject* WorldContextObject;
			FLatentActionInfo LatentInfo;
			FString stringToDeSerialize;
			TArray<FRotator> deSerializedResult;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UECodeGen_Private::FStructPropertyParams NewProp_LatentInfo;
		static const UECodeGen_Private::FStrPropertyParams NewProp_stringToDeSerialize;
		static const UECodeGen_Private::FStructPropertyParams NewProp_deSerializedResult_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_deSerializedResult;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeRotatorArrayAsync_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeRotatorArrayAsync_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeRotatorArrayAsync_Statics::NewProp_LatentInfo = { "LatentInfo", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeRotatorArrayAsync_Parms, LatentInfo), Z_Construct_UScriptStruct_FLatentActionInfo, METADATA_PARAMS(nullptr, 0) }; // 2194022737
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeRotatorArrayAsync_Statics::NewProp_stringToDeSerialize = { "stringToDeSerialize", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeRotatorArrayAsync_Parms, stringToDeSerialize), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeRotatorArrayAsync_Statics::NewProp_deSerializedResult_Inner = { "deSerializedResult", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeRotatorArrayAsync_Statics::NewProp_deSerializedResult = { "deSerializedResult", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeRotatorArrayAsync_Parms, deSerializedResult), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeRotatorArrayAsync_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeRotatorArrayAsync_Statics::NewProp_WorldContextObject,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeRotatorArrayAsync_Statics::NewProp_LatentInfo,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeRotatorArrayAsync_Statics::NewProp_stringToDeSerialize,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeRotatorArrayAsync_Statics::NewProp_deSerializedResult_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeRotatorArrayAsync_Statics::NewProp_deSerializedResult,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeRotatorArrayAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|De-Serialize Array" },
		{ "Latent", "" },
		{ "LatentInfo", "LatentInfo" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
		{ "ToolTip", "De-Serializes the Array using Async Task so eventhough it's very large the FPS doesn't get affected since it's not run on the Game Thread" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeRotatorArrayAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "DeSerializeRotatorArrayAsync", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeRotatorArrayAsync_Statics::RapidJsonUEFunctionLibrary_eventDeSerializeRotatorArrayAsync_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeRotatorArrayAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeRotatorArrayAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeRotatorArrayAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeRotatorArrayAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeRotatorArrayAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeRotatorArrayAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeStringArrayAsync_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventDeSerializeStringArrayAsync_Parms
		{
			UObject* WorldContextObject;
			FLatentActionInfo LatentInfo;
			FString stringToDeSerialize;
			TArray<FString> deSerializedResult;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UECodeGen_Private::FStructPropertyParams NewProp_LatentInfo;
		static const UECodeGen_Private::FStrPropertyParams NewProp_stringToDeSerialize;
		static const UECodeGen_Private::FStrPropertyParams NewProp_deSerializedResult_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_deSerializedResult;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeStringArrayAsync_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeStringArrayAsync_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeStringArrayAsync_Statics::NewProp_LatentInfo = { "LatentInfo", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeStringArrayAsync_Parms, LatentInfo), Z_Construct_UScriptStruct_FLatentActionInfo, METADATA_PARAMS(nullptr, 0) }; // 2194022737
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeStringArrayAsync_Statics::NewProp_stringToDeSerialize = { "stringToDeSerialize", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeStringArrayAsync_Parms, stringToDeSerialize), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeStringArrayAsync_Statics::NewProp_deSerializedResult_Inner = { "deSerializedResult", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeStringArrayAsync_Statics::NewProp_deSerializedResult = { "deSerializedResult", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeStringArrayAsync_Parms, deSerializedResult), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeStringArrayAsync_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeStringArrayAsync_Statics::NewProp_WorldContextObject,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeStringArrayAsync_Statics::NewProp_LatentInfo,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeStringArrayAsync_Statics::NewProp_stringToDeSerialize,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeStringArrayAsync_Statics::NewProp_deSerializedResult_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeStringArrayAsync_Statics::NewProp_deSerializedResult,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeStringArrayAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|De-Serialize Array" },
		{ "Latent", "" },
		{ "LatentInfo", "LatentInfo" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
		{ "ToolTip", "De-Serializes the Array using Async Task so eventhough it's very large the FPS doesn't get affected since it's not run on the Game Thread" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeStringArrayAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "DeSerializeStringArrayAsync", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeStringArrayAsync_Statics::RapidJsonUEFunctionLibrary_eventDeSerializeStringArrayAsync_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeStringArrayAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeStringArrayAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeStringArrayAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeStringArrayAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeStringArrayAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeStringArrayAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayBool_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventDeserializeTArrayBool_Wrapper_Parms
		{
			FString JsonString;
			TArray<bool> ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_JsonString_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_JsonString;
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayBool_Wrapper_Statics::NewProp_JsonString_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayBool_Wrapper_Statics::NewProp_JsonString = { "JsonString", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeTArrayBool_Wrapper_Parms, JsonString), METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayBool_Wrapper_Statics::NewProp_JsonString_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayBool_Wrapper_Statics::NewProp_JsonString_MetaData)) };
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayBool_Wrapper_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayBool_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeTArrayBool_Wrapper_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayBool_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayBool_Wrapper_Statics::NewProp_JsonString,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayBool_Wrapper_Statics::NewProp_ReturnValue_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayBool_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayBool_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|De-Serialize Array" },
		{ "DisplayName", "De-Serialize Bool Array" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayBool_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "DeserializeTArrayBool_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayBool_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventDeserializeTArrayBool_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayBool_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayBool_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayBool_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayBool_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayBool_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayBool_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFColor_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventDeserializeTArrayFColor_Wrapper_Parms
		{
			FString JsonString;
			TArray<FColor> ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_JsonString_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_JsonString;
		static const UECodeGen_Private::FStructPropertyParams NewProp_ReturnValue_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFColor_Wrapper_Statics::NewProp_JsonString_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFColor_Wrapper_Statics::NewProp_JsonString = { "JsonString", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeTArrayFColor_Wrapper_Parms, JsonString), METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFColor_Wrapper_Statics::NewProp_JsonString_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFColor_Wrapper_Statics::NewProp_JsonString_MetaData)) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFColor_Wrapper_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFColor_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeTArrayFColor_Wrapper_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFColor_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFColor_Wrapper_Statics::NewProp_JsonString,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFColor_Wrapper_Statics::NewProp_ReturnValue_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFColor_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFColor_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|De-Serialize Array" },
		{ "DisplayName", "De-Serialize Color Array" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFColor_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "DeserializeTArrayFColor_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFColor_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventDeserializeTArrayFColor_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFColor_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFColor_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFColor_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFColor_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFColor_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFColor_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFloat_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventDeserializeTArrayFloat_Wrapper_Parms
		{
			FString JsonString;
			TArray<float> ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_JsonString_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_JsonString;
		static const UECodeGen_Private::FFloatPropertyParams NewProp_ReturnValue_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFloat_Wrapper_Statics::NewProp_JsonString_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFloat_Wrapper_Statics::NewProp_JsonString = { "JsonString", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeTArrayFloat_Wrapper_Parms, JsonString), METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFloat_Wrapper_Statics::NewProp_JsonString_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFloat_Wrapper_Statics::NewProp_JsonString_MetaData)) };
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFloat_Wrapper_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFloat_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeTArrayFloat_Wrapper_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFloat_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFloat_Wrapper_Statics::NewProp_JsonString,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFloat_Wrapper_Statics::NewProp_ReturnValue_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFloat_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFloat_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|De-Serialize Array" },
		{ "DisplayName", "De-Serialize Float Array" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFloat_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "DeserializeTArrayFloat_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFloat_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventDeserializeTArrayFloat_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFloat_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFloat_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFloat_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFloat_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFloat_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFloat_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFName_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventDeserializeTArrayFName_Wrapper_Parms
		{
			FString JsonString;
			TArray<FName> ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_JsonString_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_JsonString;
		static const UECodeGen_Private::FNamePropertyParams NewProp_ReturnValue_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFName_Wrapper_Statics::NewProp_JsonString_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFName_Wrapper_Statics::NewProp_JsonString = { "JsonString", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeTArrayFName_Wrapper_Parms, JsonString), METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFName_Wrapper_Statics::NewProp_JsonString_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFName_Wrapper_Statics::NewProp_JsonString_MetaData)) };
	const UECodeGen_Private::FNamePropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFName_Wrapper_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFName_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeTArrayFName_Wrapper_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFName_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFName_Wrapper_Statics::NewProp_JsonString,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFName_Wrapper_Statics::NewProp_ReturnValue_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFName_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFName_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|De-Serialize Array" },
		{ "DisplayName", "De-Serialize Name Array" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFName_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "DeserializeTArrayFName_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFName_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventDeserializeTArrayFName_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFName_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFName_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFName_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFName_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFName_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFName_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFRotator_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventDeserializeTArrayFRotator_Wrapper_Parms
		{
			FString JsonString;
			TArray<FRotator> ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_JsonString_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_JsonString;
		static const UECodeGen_Private::FStructPropertyParams NewProp_ReturnValue_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFRotator_Wrapper_Statics::NewProp_JsonString_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFRotator_Wrapper_Statics::NewProp_JsonString = { "JsonString", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeTArrayFRotator_Wrapper_Parms, JsonString), METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFRotator_Wrapper_Statics::NewProp_JsonString_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFRotator_Wrapper_Statics::NewProp_JsonString_MetaData)) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFRotator_Wrapper_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFRotator_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeTArrayFRotator_Wrapper_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFRotator_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFRotator_Wrapper_Statics::NewProp_JsonString,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFRotator_Wrapper_Statics::NewProp_ReturnValue_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFRotator_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFRotator_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|De-Serialize Array" },
		{ "DisplayName", "De-Serialize Rotator Array" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFRotator_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "DeserializeTArrayFRotator_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFRotator_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventDeserializeTArrayFRotator_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFRotator_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFRotator_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFRotator_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFRotator_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFRotator_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFRotator_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFString_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventDeserializeTArrayFString_Wrapper_Parms
		{
			FString JsonString;
			TArray<FString> ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_JsonString_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_JsonString;
		static const UECodeGen_Private::FStrPropertyParams NewProp_ReturnValue_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFString_Wrapper_Statics::NewProp_JsonString_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFString_Wrapper_Statics::NewProp_JsonString = { "JsonString", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeTArrayFString_Wrapper_Parms, JsonString), METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFString_Wrapper_Statics::NewProp_JsonString_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFString_Wrapper_Statics::NewProp_JsonString_MetaData)) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFString_Wrapper_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFString_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeTArrayFString_Wrapper_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFString_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFString_Wrapper_Statics::NewProp_JsonString,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFString_Wrapper_Statics::NewProp_ReturnValue_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFString_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFString_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|De-Serialize Array" },
		{ "DisplayName", "De-Serialize String Array" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFString_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "DeserializeTArrayFString_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFString_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventDeserializeTArrayFString_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFString_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFString_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFString_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFString_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFString_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFString_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFText_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventDeserializeTArrayFText_Wrapper_Parms
		{
			FString JsonString;
			TArray<FText> ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_JsonString_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_JsonString;
		static const UECodeGen_Private::FTextPropertyParams NewProp_ReturnValue_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFText_Wrapper_Statics::NewProp_JsonString_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFText_Wrapper_Statics::NewProp_JsonString = { "JsonString", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeTArrayFText_Wrapper_Parms, JsonString), METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFText_Wrapper_Statics::NewProp_JsonString_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFText_Wrapper_Statics::NewProp_JsonString_MetaData)) };
	const UECodeGen_Private::FTextPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFText_Wrapper_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFText_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeTArrayFText_Wrapper_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFText_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFText_Wrapper_Statics::NewProp_JsonString,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFText_Wrapper_Statics::NewProp_ReturnValue_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFText_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFText_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|De-Serialize Array" },
		{ "DisplayName", "De-Serialize Text Array" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFText_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "DeserializeTArrayFText_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFText_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventDeserializeTArrayFText_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFText_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFText_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFText_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFText_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFText_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFText_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFTransform_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventDeserializeTArrayFTransform_Wrapper_Parms
		{
			FString JsonString;
			TArray<FTransform> ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_JsonString_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_JsonString;
		static const UECodeGen_Private::FStructPropertyParams NewProp_ReturnValue_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFTransform_Wrapper_Statics::NewProp_JsonString_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFTransform_Wrapper_Statics::NewProp_JsonString = { "JsonString", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeTArrayFTransform_Wrapper_Parms, JsonString), METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFTransform_Wrapper_Statics::NewProp_JsonString_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFTransform_Wrapper_Statics::NewProp_JsonString_MetaData)) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFTransform_Wrapper_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFTransform_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeTArrayFTransform_Wrapper_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFTransform_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFTransform_Wrapper_Statics::NewProp_JsonString,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFTransform_Wrapper_Statics::NewProp_ReturnValue_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFTransform_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFTransform_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|De-Serialize Array" },
		{ "DisplayName", "De-Serialize Transform Array" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFTransform_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "DeserializeTArrayFTransform_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFTransform_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventDeserializeTArrayFTransform_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFTransform_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFTransform_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFTransform_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFTransform_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFTransform_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFTransform_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFVector_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventDeserializeTArrayFVector_Wrapper_Parms
		{
			FString JsonString;
			TArray<FVector> ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_JsonString_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_JsonString;
		static const UECodeGen_Private::FStructPropertyParams NewProp_ReturnValue_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFVector_Wrapper_Statics::NewProp_JsonString_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFVector_Wrapper_Statics::NewProp_JsonString = { "JsonString", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeTArrayFVector_Wrapper_Parms, JsonString), METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFVector_Wrapper_Statics::NewProp_JsonString_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFVector_Wrapper_Statics::NewProp_JsonString_MetaData)) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFVector_Wrapper_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFVector_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeTArrayFVector_Wrapper_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFVector_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFVector_Wrapper_Statics::NewProp_JsonString,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFVector_Wrapper_Statics::NewProp_ReturnValue_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFVector_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFVector_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|De-Serialize Array" },
		{ "DisplayName", "De-Serialize Vector Array" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFVector_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "DeserializeTArrayFVector_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFVector_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventDeserializeTArrayFVector_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFVector_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFVector_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFVector_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFVector_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFVector_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFVector_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt64_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventDeserializeTArrayInt64_Wrapper_Parms
		{
			FString JsonString;
			TArray<int64> ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_JsonString_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_JsonString;
		static const UECodeGen_Private::FInt64PropertyParams NewProp_ReturnValue_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt64_Wrapper_Statics::NewProp_JsonString_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt64_Wrapper_Statics::NewProp_JsonString = { "JsonString", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeTArrayInt64_Wrapper_Parms, JsonString), METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt64_Wrapper_Statics::NewProp_JsonString_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt64_Wrapper_Statics::NewProp_JsonString_MetaData)) };
	const UECodeGen_Private::FInt64PropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt64_Wrapper_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Int64, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt64_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeTArrayInt64_Wrapper_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt64_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt64_Wrapper_Statics::NewProp_JsonString,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt64_Wrapper_Statics::NewProp_ReturnValue_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt64_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt64_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|De-Serialize Array" },
		{ "DisplayName", "De-Serialize Int64 Array" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt64_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "DeserializeTArrayInt64_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt64_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventDeserializeTArrayInt64_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt64_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt64_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt64_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt64_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt64_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt64_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventDeserializeTArrayInt_Wrapper_Parms
		{
			FString JsonString;
			TArray<int32> ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_JsonString_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_JsonString;
		static const UECodeGen_Private::FIntPropertyParams NewProp_ReturnValue_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt_Wrapper_Statics::NewProp_JsonString_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt_Wrapper_Statics::NewProp_JsonString = { "JsonString", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeTArrayInt_Wrapper_Parms, JsonString), METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt_Wrapper_Statics::NewProp_JsonString_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt_Wrapper_Statics::NewProp_JsonString_MetaData)) };
	const UECodeGen_Private::FIntPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt_Wrapper_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeTArrayInt_Wrapper_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt_Wrapper_Statics::NewProp_JsonString,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt_Wrapper_Statics::NewProp_ReturnValue_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|De-Serialize Array" },
		{ "Comment", "//------- DE-SERIALIZE ARRAYS --------//\n" },
		{ "DisplayName", "De-Serialize Int Array" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
		{ "ToolTip", "------- DE-SERIALIZE ARRAYS --------" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "DeserializeTArrayInt_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventDeserializeTArrayInt_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayUInt8_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventDeserializeTArrayUInt8_Wrapper_Parms
		{
			FString JsonString;
			TArray<uint8> ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_JsonString_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_JsonString;
		static const UECodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayUInt8_Wrapper_Statics::NewProp_JsonString_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayUInt8_Wrapper_Statics::NewProp_JsonString = { "JsonString", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeTArrayUInt8_Wrapper_Parms, JsonString), METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayUInt8_Wrapper_Statics::NewProp_JsonString_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayUInt8_Wrapper_Statics::NewProp_JsonString_MetaData)) };
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayUInt8_Wrapper_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayUInt8_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeTArrayUInt8_Wrapper_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayUInt8_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayUInt8_Wrapper_Statics::NewProp_JsonString,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayUInt8_Wrapper_Statics::NewProp_ReturnValue_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayUInt8_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayUInt8_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|De-Serialize Array" },
		{ "DisplayName", "De-Serialize Byte Array" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayUInt8_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "DeserializeTArrayUInt8_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayUInt8_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventDeserializeTArrayUInt8_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayUInt8_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayUInt8_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayUInt8_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayUInt8_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayUInt8_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayUInt8_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTextArrayAsync_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventDeSerializeTextArrayAsync_Parms
		{
			UObject* WorldContextObject;
			FLatentActionInfo LatentInfo;
			FString stringToDeSerialize;
			TArray<FText> deSerializedResult;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UECodeGen_Private::FStructPropertyParams NewProp_LatentInfo;
		static const UECodeGen_Private::FStrPropertyParams NewProp_stringToDeSerialize;
		static const UECodeGen_Private::FTextPropertyParams NewProp_deSerializedResult_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_deSerializedResult;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTextArrayAsync_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeTextArrayAsync_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTextArrayAsync_Statics::NewProp_LatentInfo = { "LatentInfo", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeTextArrayAsync_Parms, LatentInfo), Z_Construct_UScriptStruct_FLatentActionInfo, METADATA_PARAMS(nullptr, 0) }; // 2194022737
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTextArrayAsync_Statics::NewProp_stringToDeSerialize = { "stringToDeSerialize", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeTextArrayAsync_Parms, stringToDeSerialize), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FTextPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTextArrayAsync_Statics::NewProp_deSerializedResult_Inner = { "deSerializedResult", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTextArrayAsync_Statics::NewProp_deSerializedResult = { "deSerializedResult", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeTextArrayAsync_Parms, deSerializedResult), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTextArrayAsync_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTextArrayAsync_Statics::NewProp_WorldContextObject,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTextArrayAsync_Statics::NewProp_LatentInfo,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTextArrayAsync_Statics::NewProp_stringToDeSerialize,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTextArrayAsync_Statics::NewProp_deSerializedResult_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTextArrayAsync_Statics::NewProp_deSerializedResult,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTextArrayAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|De-Serialize Array" },
		{ "Latent", "" },
		{ "LatentInfo", "LatentInfo" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
		{ "ToolTip", "De-Serializes the Array using Async Task so eventhough it's very large the FPS doesn't get affected since it's not run on the Game Thread" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTextArrayAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "DeSerializeTextArrayAsync", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTextArrayAsync_Statics::RapidJsonUEFunctionLibrary_eventDeSerializeTextArrayAsync_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTextArrayAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTextArrayAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTextArrayAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTextArrayAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTextArrayAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTextArrayAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTransformArrayAsync_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventDeSerializeTransformArrayAsync_Parms
		{
			UObject* WorldContextObject;
			FLatentActionInfo LatentInfo;
			FString stringToDeSerialize;
			TArray<FTransform> deSerializedResult;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UECodeGen_Private::FStructPropertyParams NewProp_LatentInfo;
		static const UECodeGen_Private::FStrPropertyParams NewProp_stringToDeSerialize;
		static const UECodeGen_Private::FStructPropertyParams NewProp_deSerializedResult_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_deSerializedResult;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTransformArrayAsync_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeTransformArrayAsync_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTransformArrayAsync_Statics::NewProp_LatentInfo = { "LatentInfo", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeTransformArrayAsync_Parms, LatentInfo), Z_Construct_UScriptStruct_FLatentActionInfo, METADATA_PARAMS(nullptr, 0) }; // 2194022737
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTransformArrayAsync_Statics::NewProp_stringToDeSerialize = { "stringToDeSerialize", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeTransformArrayAsync_Parms, stringToDeSerialize), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTransformArrayAsync_Statics::NewProp_deSerializedResult_Inner = { "deSerializedResult", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTransformArrayAsync_Statics::NewProp_deSerializedResult = { "deSerializedResult", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeTransformArrayAsync_Parms, deSerializedResult), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTransformArrayAsync_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTransformArrayAsync_Statics::NewProp_WorldContextObject,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTransformArrayAsync_Statics::NewProp_LatentInfo,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTransformArrayAsync_Statics::NewProp_stringToDeSerialize,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTransformArrayAsync_Statics::NewProp_deSerializedResult_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTransformArrayAsync_Statics::NewProp_deSerializedResult,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTransformArrayAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|De-Serialize Array" },
		{ "Latent", "" },
		{ "LatentInfo", "LatentInfo" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
		{ "ToolTip", "De-Serializes the Array using Async Task so eventhough it's very large the FPS doesn't get affected since it's not run on the Game Thread" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTransformArrayAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "DeSerializeTransformArrayAsync", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTransformArrayAsync_Statics::RapidJsonUEFunctionLibrary_eventDeSerializeTransformArrayAsync_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTransformArrayAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTransformArrayAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTransformArrayAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTransformArrayAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTransformArrayAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTransformArrayAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeUInt8_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventDeserializeUInt8_Wrapper_Parms
		{
			FString JsonString;
			uint8 ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_JsonString_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_JsonString;
		static const UECodeGen_Private::FBytePropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeUInt8_Wrapper_Statics::NewProp_JsonString_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeUInt8_Wrapper_Statics::NewProp_JsonString = { "JsonString", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeUInt8_Wrapper_Parms, JsonString), METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeUInt8_Wrapper_Statics::NewProp_JsonString_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeUInt8_Wrapper_Statics::NewProp_JsonString_MetaData)) };
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeUInt8_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeserializeUInt8_Wrapper_Parms, ReturnValue), nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeUInt8_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeUInt8_Wrapper_Statics::NewProp_JsonString,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeUInt8_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeUInt8_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|De-Serialize" },
		{ "DisplayName", "De-Serialize Byte" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeUInt8_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "DeserializeUInt8_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeUInt8_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventDeserializeUInt8_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeUInt8_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeUInt8_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeUInt8_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeUInt8_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeUInt8_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeUInt8_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeUInt8ArrayAsync_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventDeSerializeUInt8ArrayAsync_Parms
		{
			UObject* WorldContextObject;
			FLatentActionInfo LatentInfo;
			FString stringToDeSerialize;
			TArray<uint8> deSerializedResult;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UECodeGen_Private::FStructPropertyParams NewProp_LatentInfo;
		static const UECodeGen_Private::FStrPropertyParams NewProp_stringToDeSerialize;
		static const UECodeGen_Private::FBytePropertyParams NewProp_deSerializedResult_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_deSerializedResult;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeUInt8ArrayAsync_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeUInt8ArrayAsync_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeUInt8ArrayAsync_Statics::NewProp_LatentInfo = { "LatentInfo", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeUInt8ArrayAsync_Parms, LatentInfo), Z_Construct_UScriptStruct_FLatentActionInfo, METADATA_PARAMS(nullptr, 0) }; // 2194022737
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeUInt8ArrayAsync_Statics::NewProp_stringToDeSerialize = { "stringToDeSerialize", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeUInt8ArrayAsync_Parms, stringToDeSerialize), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeUInt8ArrayAsync_Statics::NewProp_deSerializedResult_Inner = { "deSerializedResult", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeUInt8ArrayAsync_Statics::NewProp_deSerializedResult = { "deSerializedResult", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeUInt8ArrayAsync_Parms, deSerializedResult), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeUInt8ArrayAsync_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeUInt8ArrayAsync_Statics::NewProp_WorldContextObject,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeUInt8ArrayAsync_Statics::NewProp_LatentInfo,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeUInt8ArrayAsync_Statics::NewProp_stringToDeSerialize,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeUInt8ArrayAsync_Statics::NewProp_deSerializedResult_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeUInt8ArrayAsync_Statics::NewProp_deSerializedResult,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeUInt8ArrayAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|De-Serialize Array" },
		{ "Latent", "" },
		{ "LatentInfo", "LatentInfo" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
		{ "ToolTip", "De-Serializes the Array using Async Task so eventhough it's very large the FPS doesn't get affected since it's not run on the Game Thread" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeUInt8ArrayAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "DeSerializeUInt8ArrayAsync", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeUInt8ArrayAsync_Statics::RapidJsonUEFunctionLibrary_eventDeSerializeUInt8ArrayAsync_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeUInt8ArrayAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeUInt8ArrayAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeUInt8ArrayAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeUInt8ArrayAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeUInt8ArrayAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeUInt8ArrayAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeVectorArrayAsync_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventDeSerializeVectorArrayAsync_Parms
		{
			UObject* WorldContextObject;
			FLatentActionInfo LatentInfo;
			FString stringToDeSerialize;
			TArray<FVector> deSerializedResult;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UECodeGen_Private::FStructPropertyParams NewProp_LatentInfo;
		static const UECodeGen_Private::FStrPropertyParams NewProp_stringToDeSerialize;
		static const UECodeGen_Private::FStructPropertyParams NewProp_deSerializedResult_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_deSerializedResult;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeVectorArrayAsync_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeVectorArrayAsync_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeVectorArrayAsync_Statics::NewProp_LatentInfo = { "LatentInfo", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeVectorArrayAsync_Parms, LatentInfo), Z_Construct_UScriptStruct_FLatentActionInfo, METADATA_PARAMS(nullptr, 0) }; // 2194022737
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeVectorArrayAsync_Statics::NewProp_stringToDeSerialize = { "stringToDeSerialize", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeVectorArrayAsync_Parms, stringToDeSerialize), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeVectorArrayAsync_Statics::NewProp_deSerializedResult_Inner = { "deSerializedResult", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeVectorArrayAsync_Statics::NewProp_deSerializedResult = { "deSerializedResult", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventDeSerializeVectorArrayAsync_Parms, deSerializedResult), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeVectorArrayAsync_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeVectorArrayAsync_Statics::NewProp_WorldContextObject,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeVectorArrayAsync_Statics::NewProp_LatentInfo,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeVectorArrayAsync_Statics::NewProp_stringToDeSerialize,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeVectorArrayAsync_Statics::NewProp_deSerializedResult_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeVectorArrayAsync_Statics::NewProp_deSerializedResult,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeVectorArrayAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|De-Serialize Array" },
		{ "Latent", "" },
		{ "LatentInfo", "LatentInfo" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
		{ "ToolTip", "De-Serializes the Array using Async Task so eventhough it's very large the FPS doesn't get affected since it's not run on the Game Thread" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeVectorArrayAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "DeSerializeVectorArrayAsync", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeVectorArrayAsync_Statics::RapidJsonUEFunctionLibrary_eventDeSerializeVectorArrayAsync_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeVectorArrayAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeVectorArrayAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeVectorArrayAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeVectorArrayAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeVectorArrayAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeVectorArrayAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBool_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventSerializeBool_Wrapper_Parms
		{
			bool BoolValue;
			FString ReturnValue;
		};
		static void NewProp_BoolValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_BoolValue;
		static const UECodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBool_Wrapper_Statics::NewProp_BoolValue_SetBit(void* Obj)
	{
		((RapidJsonUEFunctionLibrary_eventSerializeBool_Wrapper_Parms*)Obj)->BoolValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBool_Wrapper_Statics::NewProp_BoolValue = { "BoolValue", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(RapidJsonUEFunctionLibrary_eventSerializeBool_Wrapper_Parms), &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBool_Wrapper_Statics::NewProp_BoolValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBool_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeBool_Wrapper_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBool_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBool_Wrapper_Statics::NewProp_BoolValue,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBool_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBool_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|Serialize" },
		{ "DisplayName", "Serialize Bool" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBool_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "SerializeBool_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBool_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventSerializeBool_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBool_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBool_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBool_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBool_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBool_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBool_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBoolArrayAsync_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventSerializeBoolArrayAsync_Parms
		{
			UObject* WorldContextObject;
			FLatentActionInfo LatentInfo;
			TArray<bool> arrayToSerialize;
			FString serializedString;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UECodeGen_Private::FStructPropertyParams NewProp_LatentInfo;
		static const UECodeGen_Private::FBoolPropertyParams NewProp_arrayToSerialize_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_arrayToSerialize;
		static const UECodeGen_Private::FStrPropertyParams NewProp_serializedString;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBoolArrayAsync_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeBoolArrayAsync_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBoolArrayAsync_Statics::NewProp_LatentInfo = { "LatentInfo", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeBoolArrayAsync_Parms, LatentInfo), Z_Construct_UScriptStruct_FLatentActionInfo, METADATA_PARAMS(nullptr, 0) }; // 2194022737
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBoolArrayAsync_Statics::NewProp_arrayToSerialize_Inner = { "arrayToSerialize", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBoolArrayAsync_Statics::NewProp_arrayToSerialize = { "arrayToSerialize", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeBoolArrayAsync_Parms, arrayToSerialize), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBoolArrayAsync_Statics::NewProp_serializedString = { "serializedString", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeBoolArrayAsync_Parms, serializedString), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBoolArrayAsync_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBoolArrayAsync_Statics::NewProp_WorldContextObject,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBoolArrayAsync_Statics::NewProp_LatentInfo,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBoolArrayAsync_Statics::NewProp_arrayToSerialize_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBoolArrayAsync_Statics::NewProp_arrayToSerialize,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBoolArrayAsync_Statics::NewProp_serializedString,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBoolArrayAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|Serialize Array" },
		{ "Latent", "" },
		{ "LatentInfo", "LatentInfo" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
		{ "ToolTip", "Serializes the Array using Async Task so eventhough it's very large the FPS doesn't get affected since it's not run on the Game Thread" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBoolArrayAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "SerializeBoolArrayAsync", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBoolArrayAsync_Statics::RapidJsonUEFunctionLibrary_eventSerializeBoolArrayAsync_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBoolArrayAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBoolArrayAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBoolArrayAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBoolArrayAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBoolArrayAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBoolArrayAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeColorArrayAsync_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventSerializeColorArrayAsync_Parms
		{
			UObject* WorldContextObject;
			FLatentActionInfo LatentInfo;
			TArray<FColor> arrayToSerialize;
			FString serializedString;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UECodeGen_Private::FStructPropertyParams NewProp_LatentInfo;
		static const UECodeGen_Private::FStructPropertyParams NewProp_arrayToSerialize_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_arrayToSerialize;
		static const UECodeGen_Private::FStrPropertyParams NewProp_serializedString;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeColorArrayAsync_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeColorArrayAsync_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeColorArrayAsync_Statics::NewProp_LatentInfo = { "LatentInfo", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeColorArrayAsync_Parms, LatentInfo), Z_Construct_UScriptStruct_FLatentActionInfo, METADATA_PARAMS(nullptr, 0) }; // 2194022737
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeColorArrayAsync_Statics::NewProp_arrayToSerialize_Inner = { "arrayToSerialize", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeColorArrayAsync_Statics::NewProp_arrayToSerialize = { "arrayToSerialize", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeColorArrayAsync_Parms, arrayToSerialize), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeColorArrayAsync_Statics::NewProp_serializedString = { "serializedString", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeColorArrayAsync_Parms, serializedString), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeColorArrayAsync_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeColorArrayAsync_Statics::NewProp_WorldContextObject,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeColorArrayAsync_Statics::NewProp_LatentInfo,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeColorArrayAsync_Statics::NewProp_arrayToSerialize_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeColorArrayAsync_Statics::NewProp_arrayToSerialize,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeColorArrayAsync_Statics::NewProp_serializedString,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeColorArrayAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|Serialize Array" },
		{ "Latent", "" },
		{ "LatentInfo", "LatentInfo" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
		{ "ToolTip", "Serializes the Array using Async Task so eventhough it's very large the FPS doesn't get affected since it's not run on the Game Thread" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeColorArrayAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "SerializeColorArrayAsync", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeColorArrayAsync_Statics::RapidJsonUEFunctionLibrary_eventSerializeColorArrayAsync_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeColorArrayAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeColorArrayAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeColorArrayAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeColorArrayAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeColorArrayAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeColorArrayAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFColor_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventSerializeFColor_Wrapper_Parms
		{
			FColor ColorValue;
			FString ReturnValue;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_ColorValue;
		static const UECodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFColor_Wrapper_Statics::NewProp_ColorValue = { "ColorValue", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeFColor_Wrapper_Parms, ColorValue), Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFColor_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeFColor_Wrapper_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFColor_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFColor_Wrapper_Statics::NewProp_ColorValue,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFColor_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFColor_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|Serialize" },
		{ "DisplayName", "Serialize Color" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFColor_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "SerializeFColor_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFColor_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventSerializeFColor_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFColor_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFColor_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04822401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFColor_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFColor_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFColor_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFColor_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFloat_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventSerializeFloat_Wrapper_Parms
		{
			float FloatValue;
			FString ReturnValue;
		};
		static const UECodeGen_Private::FFloatPropertyParams NewProp_FloatValue;
		static const UECodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFloat_Wrapper_Statics::NewProp_FloatValue = { "FloatValue", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeFloat_Wrapper_Parms, FloatValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFloat_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeFloat_Wrapper_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFloat_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFloat_Wrapper_Statics::NewProp_FloatValue,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFloat_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFloat_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|Serialize" },
		{ "DisplayName", "Serialize Float" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFloat_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "SerializeFloat_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFloat_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventSerializeFloat_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFloat_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFloat_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFloat_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFloat_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFloat_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFloat_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFloatArrayAsync_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventSerializeFloatArrayAsync_Parms
		{
			UObject* WorldContextObject;
			FLatentActionInfo LatentInfo;
			TArray<float> arrayToSerialize;
			FString serializedString;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UECodeGen_Private::FStructPropertyParams NewProp_LatentInfo;
		static const UECodeGen_Private::FFloatPropertyParams NewProp_arrayToSerialize_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_arrayToSerialize;
		static const UECodeGen_Private::FStrPropertyParams NewProp_serializedString;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFloatArrayAsync_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeFloatArrayAsync_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFloatArrayAsync_Statics::NewProp_LatentInfo = { "LatentInfo", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeFloatArrayAsync_Parms, LatentInfo), Z_Construct_UScriptStruct_FLatentActionInfo, METADATA_PARAMS(nullptr, 0) }; // 2194022737
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFloatArrayAsync_Statics::NewProp_arrayToSerialize_Inner = { "arrayToSerialize", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFloatArrayAsync_Statics::NewProp_arrayToSerialize = { "arrayToSerialize", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeFloatArrayAsync_Parms, arrayToSerialize), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFloatArrayAsync_Statics::NewProp_serializedString = { "serializedString", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeFloatArrayAsync_Parms, serializedString), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFloatArrayAsync_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFloatArrayAsync_Statics::NewProp_WorldContextObject,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFloatArrayAsync_Statics::NewProp_LatentInfo,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFloatArrayAsync_Statics::NewProp_arrayToSerialize_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFloatArrayAsync_Statics::NewProp_arrayToSerialize,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFloatArrayAsync_Statics::NewProp_serializedString,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFloatArrayAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|Serialize Array" },
		{ "Latent", "" },
		{ "LatentInfo", "LatentInfo" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
		{ "ToolTip", "Serializes the Array using Async Task so eventhough it's very large the FPS doesn't get affected since it's not run on the Game Thread" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFloatArrayAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "SerializeFloatArrayAsync", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFloatArrayAsync_Statics::RapidJsonUEFunctionLibrary_eventSerializeFloatArrayAsync_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFloatArrayAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFloatArrayAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFloatArrayAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFloatArrayAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFloatArrayAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFloatArrayAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFName_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventSerializeFName_Wrapper_Parms
		{
			FName NameValue;
			FString ReturnValue;
		};
		static const UECodeGen_Private::FNamePropertyParams NewProp_NameValue;
		static const UECodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FNamePropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFName_Wrapper_Statics::NewProp_NameValue = { "NameValue", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeFName_Wrapper_Parms, NameValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFName_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeFName_Wrapper_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFName_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFName_Wrapper_Statics::NewProp_NameValue,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFName_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFName_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|Serialize" },
		{ "DisplayName", "Serialize Name" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFName_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "SerializeFName_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFName_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventSerializeFName_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFName_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFName_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFName_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFName_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFName_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFName_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFRotator_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventSerializeFRotator_Wrapper_Parms
		{
			FRotator RotatorValue;
			FString ReturnValue;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_RotatorValue;
		static const UECodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFRotator_Wrapper_Statics::NewProp_RotatorValue = { "RotatorValue", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeFRotator_Wrapper_Parms, RotatorValue), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFRotator_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeFRotator_Wrapper_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFRotator_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFRotator_Wrapper_Statics::NewProp_RotatorValue,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFRotator_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFRotator_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|Serialize" },
		{ "DisplayName", "Serialize Rotator" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFRotator_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "SerializeFRotator_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFRotator_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventSerializeFRotator_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFRotator_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFRotator_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04822401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFRotator_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFRotator_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFRotator_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFRotator_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFString_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventSerializeFString_Wrapper_Parms
		{
			FString StringValue;
			FString ReturnValue;
		};
		static const UECodeGen_Private::FStrPropertyParams NewProp_StringValue;
		static const UECodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFString_Wrapper_Statics::NewProp_StringValue = { "StringValue", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeFString_Wrapper_Parms, StringValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFString_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeFString_Wrapper_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFString_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFString_Wrapper_Statics::NewProp_StringValue,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFString_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFString_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|Serialize" },
		{ "DisplayName", "Serialize String" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFString_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "SerializeFString_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFString_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventSerializeFString_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFString_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFString_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFString_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFString_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFString_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFString_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFText_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventSerializeFText_Wrapper_Parms
		{
			FText TextValue;
			FString ReturnValue;
		};
		static const UECodeGen_Private::FTextPropertyParams NewProp_TextValue;
		static const UECodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FTextPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFText_Wrapper_Statics::NewProp_TextValue = { "TextValue", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeFText_Wrapper_Parms, TextValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFText_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeFText_Wrapper_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFText_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFText_Wrapper_Statics::NewProp_TextValue,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFText_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFText_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|Serialize" },
		{ "DisplayName", "Serialize Text" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFText_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "SerializeFText_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFText_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventSerializeFText_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFText_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFText_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFText_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFText_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFText_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFText_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFTransform_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventSerializeFTransform_Wrapper_Parms
		{
			FTransform TransformValue;
			FString ReturnValue;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_TransformValue;
		static const UECodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFTransform_Wrapper_Statics::NewProp_TransformValue = { "TransformValue", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeFTransform_Wrapper_Parms, TransformValue), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFTransform_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeFTransform_Wrapper_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFTransform_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFTransform_Wrapper_Statics::NewProp_TransformValue,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFTransform_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFTransform_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|Serialize" },
		{ "DisplayName", "Serialize Transform" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFTransform_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "SerializeFTransform_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFTransform_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventSerializeFTransform_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFTransform_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFTransform_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04822401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFTransform_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFTransform_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFTransform_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFTransform_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFVector_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventSerializeFVector_Wrapper_Parms
		{
			FVector VectorValue;
			FString ReturnValue;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_VectorValue;
		static const UECodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFVector_Wrapper_Statics::NewProp_VectorValue = { "VectorValue", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeFVector_Wrapper_Parms, VectorValue), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFVector_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeFVector_Wrapper_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFVector_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFVector_Wrapper_Statics::NewProp_VectorValue,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFVector_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFVector_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|Serialize" },
		{ "DisplayName", "Serialize Vector" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFVector_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "SerializeFVector_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFVector_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventSerializeFVector_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFVector_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFVector_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04822401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFVector_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFVector_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFVector_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFVector_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt32ArrayAsync_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventSerializeInt32ArrayAsync_Parms
		{
			UObject* WorldContextObject;
			FLatentActionInfo LatentInfo;
			TArray<int32> arrayToSerialize;
			FString serializedString;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UECodeGen_Private::FStructPropertyParams NewProp_LatentInfo;
		static const UECodeGen_Private::FIntPropertyParams NewProp_arrayToSerialize_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_arrayToSerialize;
		static const UECodeGen_Private::FStrPropertyParams NewProp_serializedString;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt32ArrayAsync_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeInt32ArrayAsync_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt32ArrayAsync_Statics::NewProp_LatentInfo = { "LatentInfo", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeInt32ArrayAsync_Parms, LatentInfo), Z_Construct_UScriptStruct_FLatentActionInfo, METADATA_PARAMS(nullptr, 0) }; // 2194022737
	const UECodeGen_Private::FIntPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt32ArrayAsync_Statics::NewProp_arrayToSerialize_Inner = { "arrayToSerialize", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt32ArrayAsync_Statics::NewProp_arrayToSerialize = { "arrayToSerialize", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeInt32ArrayAsync_Parms, arrayToSerialize), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt32ArrayAsync_Statics::NewProp_serializedString = { "serializedString", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeInt32ArrayAsync_Parms, serializedString), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt32ArrayAsync_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt32ArrayAsync_Statics::NewProp_WorldContextObject,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt32ArrayAsync_Statics::NewProp_LatentInfo,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt32ArrayAsync_Statics::NewProp_arrayToSerialize_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt32ArrayAsync_Statics::NewProp_arrayToSerialize,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt32ArrayAsync_Statics::NewProp_serializedString,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt32ArrayAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|Serialize Array" },
		{ "Latent", "" },
		{ "LatentInfo", "LatentInfo" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
		{ "ToolTip", "Serializes the Array using Async Task so eventhough it's very large the FPS doesn't get affected since it's not run on the Game Thread" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt32ArrayAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "SerializeInt32ArrayAsync", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt32ArrayAsync_Statics::RapidJsonUEFunctionLibrary_eventSerializeInt32ArrayAsync_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt32ArrayAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt32ArrayAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt32ArrayAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt32ArrayAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt32ArrayAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt32ArrayAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt64_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventSerializeInt64_Wrapper_Parms
		{
			int64 Int64Value;
			FString ReturnValue;
		};
		static const UECodeGen_Private::FInt64PropertyParams NewProp_Int64Value;
		static const UECodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FInt64PropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt64_Wrapper_Statics::NewProp_Int64Value = { "Int64Value", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Int64, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeInt64_Wrapper_Parms, Int64Value), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt64_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeInt64_Wrapper_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt64_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt64_Wrapper_Statics::NewProp_Int64Value,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt64_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt64_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|Serialize" },
		{ "DisplayName", "Serialize Int64" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt64_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "SerializeInt64_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt64_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventSerializeInt64_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt64_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt64_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt64_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt64_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt64_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt64_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt64ArrayAsync_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventSerializeInt64ArrayAsync_Parms
		{
			UObject* WorldContextObject;
			FLatentActionInfo LatentInfo;
			TArray<int64> arrayToSerialize;
			FString serializedString;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UECodeGen_Private::FStructPropertyParams NewProp_LatentInfo;
		static const UECodeGen_Private::FInt64PropertyParams NewProp_arrayToSerialize_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_arrayToSerialize;
		static const UECodeGen_Private::FStrPropertyParams NewProp_serializedString;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt64ArrayAsync_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeInt64ArrayAsync_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt64ArrayAsync_Statics::NewProp_LatentInfo = { "LatentInfo", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeInt64ArrayAsync_Parms, LatentInfo), Z_Construct_UScriptStruct_FLatentActionInfo, METADATA_PARAMS(nullptr, 0) }; // 2194022737
	const UECodeGen_Private::FInt64PropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt64ArrayAsync_Statics::NewProp_arrayToSerialize_Inner = { "arrayToSerialize", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Int64, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt64ArrayAsync_Statics::NewProp_arrayToSerialize = { "arrayToSerialize", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeInt64ArrayAsync_Parms, arrayToSerialize), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt64ArrayAsync_Statics::NewProp_serializedString = { "serializedString", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeInt64ArrayAsync_Parms, serializedString), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt64ArrayAsync_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt64ArrayAsync_Statics::NewProp_WorldContextObject,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt64ArrayAsync_Statics::NewProp_LatentInfo,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt64ArrayAsync_Statics::NewProp_arrayToSerialize_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt64ArrayAsync_Statics::NewProp_arrayToSerialize,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt64ArrayAsync_Statics::NewProp_serializedString,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt64ArrayAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|Serialize Array" },
		{ "Latent", "" },
		{ "LatentInfo", "LatentInfo" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
		{ "ToolTip", "Serializes the Array using Async Task so eventhough it's very large the FPS doesn't get affected since it's not run on the Game Thread" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt64ArrayAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "SerializeInt64ArrayAsync", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt64ArrayAsync_Statics::RapidJsonUEFunctionLibrary_eventSerializeInt64ArrayAsync_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt64ArrayAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt64ArrayAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt64ArrayAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt64ArrayAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt64ArrayAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt64ArrayAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventSerializeInt_Wrapper_Parms
		{
			int32 IntValue;
			FString ReturnValue;
		};
		static const UECodeGen_Private::FIntPropertyParams NewProp_IntValue;
		static const UECodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FIntPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt_Wrapper_Statics::NewProp_IntValue = { "IntValue", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeInt_Wrapper_Parms, IntValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeInt_Wrapper_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt_Wrapper_Statics::NewProp_IntValue,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|Serialize" },
		{ "Comment", "//------- SERIALIZE --------//\n" },
		{ "DisplayName", "Serialize Int" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
		{ "ToolTip", "------- SERIALIZE --------" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "SerializeInt_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventSerializeInt_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeNameArrayAsync_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventSerializeNameArrayAsync_Parms
		{
			UObject* WorldContextObject;
			FLatentActionInfo LatentInfo;
			TArray<FName> arrayToSerialize;
			FString serializedString;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UECodeGen_Private::FStructPropertyParams NewProp_LatentInfo;
		static const UECodeGen_Private::FNamePropertyParams NewProp_arrayToSerialize_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_arrayToSerialize;
		static const UECodeGen_Private::FStrPropertyParams NewProp_serializedString;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeNameArrayAsync_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeNameArrayAsync_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeNameArrayAsync_Statics::NewProp_LatentInfo = { "LatentInfo", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeNameArrayAsync_Parms, LatentInfo), Z_Construct_UScriptStruct_FLatentActionInfo, METADATA_PARAMS(nullptr, 0) }; // 2194022737
	const UECodeGen_Private::FNamePropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeNameArrayAsync_Statics::NewProp_arrayToSerialize_Inner = { "arrayToSerialize", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeNameArrayAsync_Statics::NewProp_arrayToSerialize = { "arrayToSerialize", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeNameArrayAsync_Parms, arrayToSerialize), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeNameArrayAsync_Statics::NewProp_serializedString = { "serializedString", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeNameArrayAsync_Parms, serializedString), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeNameArrayAsync_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeNameArrayAsync_Statics::NewProp_WorldContextObject,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeNameArrayAsync_Statics::NewProp_LatentInfo,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeNameArrayAsync_Statics::NewProp_arrayToSerialize_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeNameArrayAsync_Statics::NewProp_arrayToSerialize,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeNameArrayAsync_Statics::NewProp_serializedString,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeNameArrayAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|Serialize Array" },
		{ "Latent", "" },
		{ "LatentInfo", "LatentInfo" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
		{ "ToolTip", "Serializes the Array using Async Task so eventhough it's very large the FPS doesn't get affected since it's not run on the Game Thread" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeNameArrayAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "SerializeNameArrayAsync", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeNameArrayAsync_Statics::RapidJsonUEFunctionLibrary_eventSerializeNameArrayAsync_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeNameArrayAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeNameArrayAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeNameArrayAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeNameArrayAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeNameArrayAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeNameArrayAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeRotatorArrayAsync_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventSerializeRotatorArrayAsync_Parms
		{
			UObject* WorldContextObject;
			FLatentActionInfo LatentInfo;
			TArray<FRotator> arrayToSerialize;
			FString serializedString;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UECodeGen_Private::FStructPropertyParams NewProp_LatentInfo;
		static const UECodeGen_Private::FStructPropertyParams NewProp_arrayToSerialize_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_arrayToSerialize;
		static const UECodeGen_Private::FStrPropertyParams NewProp_serializedString;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeRotatorArrayAsync_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeRotatorArrayAsync_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeRotatorArrayAsync_Statics::NewProp_LatentInfo = { "LatentInfo", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeRotatorArrayAsync_Parms, LatentInfo), Z_Construct_UScriptStruct_FLatentActionInfo, METADATA_PARAMS(nullptr, 0) }; // 2194022737
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeRotatorArrayAsync_Statics::NewProp_arrayToSerialize_Inner = { "arrayToSerialize", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeRotatorArrayAsync_Statics::NewProp_arrayToSerialize = { "arrayToSerialize", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeRotatorArrayAsync_Parms, arrayToSerialize), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeRotatorArrayAsync_Statics::NewProp_serializedString = { "serializedString", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeRotatorArrayAsync_Parms, serializedString), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeRotatorArrayAsync_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeRotatorArrayAsync_Statics::NewProp_WorldContextObject,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeRotatorArrayAsync_Statics::NewProp_LatentInfo,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeRotatorArrayAsync_Statics::NewProp_arrayToSerialize_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeRotatorArrayAsync_Statics::NewProp_arrayToSerialize,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeRotatorArrayAsync_Statics::NewProp_serializedString,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeRotatorArrayAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|Serialize Array" },
		{ "Latent", "" },
		{ "LatentInfo", "LatentInfo" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
		{ "ToolTip", "Serializes the Array using Async Task so eventhough it's very large the FPS doesn't get affected since it's not run on the Game Thread" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeRotatorArrayAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "SerializeRotatorArrayAsync", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeRotatorArrayAsync_Statics::RapidJsonUEFunctionLibrary_eventSerializeRotatorArrayAsync_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeRotatorArrayAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeRotatorArrayAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeRotatorArrayAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeRotatorArrayAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeRotatorArrayAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeRotatorArrayAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeStringArrayAsync_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventSerializeStringArrayAsync_Parms
		{
			UObject* WorldContextObject;
			FLatentActionInfo LatentInfo;
			TArray<FString> arrayToSerialize;
			FString serializedString;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UECodeGen_Private::FStructPropertyParams NewProp_LatentInfo;
		static const UECodeGen_Private::FStrPropertyParams NewProp_arrayToSerialize_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_arrayToSerialize;
		static const UECodeGen_Private::FStrPropertyParams NewProp_serializedString;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeStringArrayAsync_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeStringArrayAsync_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeStringArrayAsync_Statics::NewProp_LatentInfo = { "LatentInfo", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeStringArrayAsync_Parms, LatentInfo), Z_Construct_UScriptStruct_FLatentActionInfo, METADATA_PARAMS(nullptr, 0) }; // 2194022737
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeStringArrayAsync_Statics::NewProp_arrayToSerialize_Inner = { "arrayToSerialize", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeStringArrayAsync_Statics::NewProp_arrayToSerialize = { "arrayToSerialize", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeStringArrayAsync_Parms, arrayToSerialize), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeStringArrayAsync_Statics::NewProp_serializedString = { "serializedString", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeStringArrayAsync_Parms, serializedString), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeStringArrayAsync_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeStringArrayAsync_Statics::NewProp_WorldContextObject,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeStringArrayAsync_Statics::NewProp_LatentInfo,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeStringArrayAsync_Statics::NewProp_arrayToSerialize_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeStringArrayAsync_Statics::NewProp_arrayToSerialize,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeStringArrayAsync_Statics::NewProp_serializedString,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeStringArrayAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|Serialize Array" },
		{ "Latent", "" },
		{ "LatentInfo", "LatentInfo" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
		{ "ToolTip", "Serializes the Array using Async Task so eventhough it's very large the FPS doesn't get affected since it's not run on the Game Thread" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeStringArrayAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "SerializeStringArrayAsync", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeStringArrayAsync_Statics::RapidJsonUEFunctionLibrary_eventSerializeStringArrayAsync_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeStringArrayAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeStringArrayAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeStringArrayAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeStringArrayAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeStringArrayAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeStringArrayAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayBool_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventSerializeTArrayBool_Wrapper_Parms
		{
			TArray<bool> InBools;
			FString ReturnValue;
		};
		static const UECodeGen_Private::FBoolPropertyParams NewProp_InBools_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_InBools_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_InBools;
		static const UECodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayBool_Wrapper_Statics::NewProp_InBools_Inner = { "InBools", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayBool_Wrapper_Statics::NewProp_InBools_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayBool_Wrapper_Statics::NewProp_InBools = { "InBools", nullptr, (EPropertyFlags)0x0010000008000182, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeTArrayBool_Wrapper_Parms, InBools), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayBool_Wrapper_Statics::NewProp_InBools_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayBool_Wrapper_Statics::NewProp_InBools_MetaData)) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayBool_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeTArrayBool_Wrapper_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayBool_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayBool_Wrapper_Statics::NewProp_InBools_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayBool_Wrapper_Statics::NewProp_InBools,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayBool_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayBool_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|Serialize Array" },
		{ "DisplayName", "Serialize Bool Array" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayBool_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "SerializeTArrayBool_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayBool_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventSerializeTArrayBool_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayBool_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayBool_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayBool_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayBool_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayBool_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayBool_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFColor_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventSerializeTArrayFColor_Wrapper_Parms
		{
			TArray<FColor> Colors;
			FString ReturnValue;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_Colors_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Colors_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_Colors;
		static const UECodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFColor_Wrapper_Statics::NewProp_Colors_Inner = { "Colors", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFColor_Wrapper_Statics::NewProp_Colors_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFColor_Wrapper_Statics::NewProp_Colors = { "Colors", nullptr, (EPropertyFlags)0x0010000008000182, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeTArrayFColor_Wrapper_Parms, Colors), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFColor_Wrapper_Statics::NewProp_Colors_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFColor_Wrapper_Statics::NewProp_Colors_MetaData)) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFColor_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeTArrayFColor_Wrapper_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFColor_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFColor_Wrapper_Statics::NewProp_Colors_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFColor_Wrapper_Statics::NewProp_Colors,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFColor_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFColor_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|Serialize Array" },
		{ "DisplayName", "Serialize Color Array" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFColor_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "SerializeTArrayFColor_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFColor_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventSerializeTArrayFColor_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFColor_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFColor_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFColor_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFColor_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFColor_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFColor_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFloat_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventSerializeTArrayFloat_Wrapper_Parms
		{
			TArray<float> inFloats;
			FString ReturnValue;
		};
		static const UECodeGen_Private::FFloatPropertyParams NewProp_inFloats_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_inFloats_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_inFloats;
		static const UECodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFloat_Wrapper_Statics::NewProp_inFloats_Inner = { "inFloats", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFloat_Wrapper_Statics::NewProp_inFloats_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFloat_Wrapper_Statics::NewProp_inFloats = { "inFloats", nullptr, (EPropertyFlags)0x0010000008000182, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeTArrayFloat_Wrapper_Parms, inFloats), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFloat_Wrapper_Statics::NewProp_inFloats_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFloat_Wrapper_Statics::NewProp_inFloats_MetaData)) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFloat_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeTArrayFloat_Wrapper_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFloat_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFloat_Wrapper_Statics::NewProp_inFloats_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFloat_Wrapper_Statics::NewProp_inFloats,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFloat_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFloat_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|Serialize Array" },
		{ "DisplayName", "Serialize Float Array" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFloat_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "SerializeTArrayFloat_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFloat_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventSerializeTArrayFloat_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFloat_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFloat_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFloat_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFloat_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFloat_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFloat_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFName_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventSerializeTArrayFName_Wrapper_Parms
		{
			TArray<FName> InNames;
			FString ReturnValue;
		};
		static const UECodeGen_Private::FNamePropertyParams NewProp_InNames_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_InNames_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_InNames;
		static const UECodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FNamePropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFName_Wrapper_Statics::NewProp_InNames_Inner = { "InNames", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFName_Wrapper_Statics::NewProp_InNames_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFName_Wrapper_Statics::NewProp_InNames = { "InNames", nullptr, (EPropertyFlags)0x0010000008000182, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeTArrayFName_Wrapper_Parms, InNames), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFName_Wrapper_Statics::NewProp_InNames_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFName_Wrapper_Statics::NewProp_InNames_MetaData)) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFName_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeTArrayFName_Wrapper_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFName_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFName_Wrapper_Statics::NewProp_InNames_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFName_Wrapper_Statics::NewProp_InNames,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFName_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFName_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|Serialize Array" },
		{ "DisplayName", "Serialize Name Array" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFName_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "SerializeTArrayFName_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFName_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventSerializeTArrayFName_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFName_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFName_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFName_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFName_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFName_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFName_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFRotator_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventSerializeTArrayFRotator_Wrapper_Parms
		{
			TArray<FRotator> inRotator;
			FString ReturnValue;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_inRotator_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_inRotator_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_inRotator;
		static const UECodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFRotator_Wrapper_Statics::NewProp_inRotator_Inner = { "inRotator", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFRotator_Wrapper_Statics::NewProp_inRotator_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFRotator_Wrapper_Statics::NewProp_inRotator = { "inRotator", nullptr, (EPropertyFlags)0x0010000008000182, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeTArrayFRotator_Wrapper_Parms, inRotator), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFRotator_Wrapper_Statics::NewProp_inRotator_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFRotator_Wrapper_Statics::NewProp_inRotator_MetaData)) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFRotator_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeTArrayFRotator_Wrapper_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFRotator_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFRotator_Wrapper_Statics::NewProp_inRotator_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFRotator_Wrapper_Statics::NewProp_inRotator,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFRotator_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFRotator_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|Serialize Array" },
		{ "DisplayName", "Serialize Rotator Array" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFRotator_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "SerializeTArrayFRotator_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFRotator_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventSerializeTArrayFRotator_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFRotator_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFRotator_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFRotator_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFRotator_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFRotator_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFRotator_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFString_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventSerializeTArrayFString_Wrapper_Parms
		{
			TArray<FString> InStrings;
			FString ReturnValue;
		};
		static const UECodeGen_Private::FStrPropertyParams NewProp_InStrings_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_InStrings_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_InStrings;
		static const UECodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFString_Wrapper_Statics::NewProp_InStrings_Inner = { "InStrings", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFString_Wrapper_Statics::NewProp_InStrings_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFString_Wrapper_Statics::NewProp_InStrings = { "InStrings", nullptr, (EPropertyFlags)0x0010000008000182, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeTArrayFString_Wrapper_Parms, InStrings), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFString_Wrapper_Statics::NewProp_InStrings_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFString_Wrapper_Statics::NewProp_InStrings_MetaData)) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFString_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeTArrayFString_Wrapper_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFString_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFString_Wrapper_Statics::NewProp_InStrings_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFString_Wrapper_Statics::NewProp_InStrings,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFString_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFString_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|Serialize Array" },
		{ "DisplayName", "Serialize String Array" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFString_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "SerializeTArrayFString_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFString_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventSerializeTArrayFString_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFString_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFString_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFString_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFString_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFString_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFString_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFText_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventSerializeTArrayFText_Wrapper_Parms
		{
			TArray<FText> InTexts;
			FString ReturnValue;
		};
		static const UECodeGen_Private::FTextPropertyParams NewProp_InTexts_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_InTexts_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_InTexts;
		static const UECodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FTextPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFText_Wrapper_Statics::NewProp_InTexts_Inner = { "InTexts", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFText_Wrapper_Statics::NewProp_InTexts_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFText_Wrapper_Statics::NewProp_InTexts = { "InTexts", nullptr, (EPropertyFlags)0x0010000008000182, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeTArrayFText_Wrapper_Parms, InTexts), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFText_Wrapper_Statics::NewProp_InTexts_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFText_Wrapper_Statics::NewProp_InTexts_MetaData)) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFText_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeTArrayFText_Wrapper_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFText_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFText_Wrapper_Statics::NewProp_InTexts_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFText_Wrapper_Statics::NewProp_InTexts,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFText_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFText_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|Serialize Array" },
		{ "DisplayName", "Serialize Text Array" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFText_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "SerializeTArrayFText_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFText_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventSerializeTArrayFText_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFText_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFText_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFText_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFText_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFText_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFText_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFTransform_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventSerializeTArrayFTransform_Wrapper_Parms
		{
			TArray<FTransform> inTransform;
			FString ReturnValue;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_inTransform_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_inTransform_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_inTransform;
		static const UECodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFTransform_Wrapper_Statics::NewProp_inTransform_Inner = { "inTransform", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFTransform_Wrapper_Statics::NewProp_inTransform_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFTransform_Wrapper_Statics::NewProp_inTransform = { "inTransform", nullptr, (EPropertyFlags)0x0010000008000182, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeTArrayFTransform_Wrapper_Parms, inTransform), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFTransform_Wrapper_Statics::NewProp_inTransform_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFTransform_Wrapper_Statics::NewProp_inTransform_MetaData)) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFTransform_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeTArrayFTransform_Wrapper_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFTransform_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFTransform_Wrapper_Statics::NewProp_inTransform_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFTransform_Wrapper_Statics::NewProp_inTransform,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFTransform_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFTransform_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|Serialize Array" },
		{ "DisplayName", "Serialize Transform Array" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFTransform_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "SerializeTArrayFTransform_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFTransform_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventSerializeTArrayFTransform_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFTransform_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFTransform_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFTransform_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFTransform_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFTransform_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFTransform_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFVector_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventSerializeTArrayFVector_Wrapper_Parms
		{
			TArray<FVector> inFVector;
			FString ReturnValue;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_inFVector_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_inFVector_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_inFVector;
		static const UECodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFVector_Wrapper_Statics::NewProp_inFVector_Inner = { "inFVector", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFVector_Wrapper_Statics::NewProp_inFVector_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFVector_Wrapper_Statics::NewProp_inFVector = { "inFVector", nullptr, (EPropertyFlags)0x0010000008000182, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeTArrayFVector_Wrapper_Parms, inFVector), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFVector_Wrapper_Statics::NewProp_inFVector_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFVector_Wrapper_Statics::NewProp_inFVector_MetaData)) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFVector_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeTArrayFVector_Wrapper_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFVector_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFVector_Wrapper_Statics::NewProp_inFVector_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFVector_Wrapper_Statics::NewProp_inFVector,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFVector_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFVector_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|Serialize Array" },
		{ "DisplayName", "Serialize Vector Array" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFVector_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "SerializeTArrayFVector_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFVector_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventSerializeTArrayFVector_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFVector_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFVector_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFVector_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFVector_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFVector_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFVector_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt64_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventSerializeTArrayInt64_Wrapper_Parms
		{
			TArray<int64> InInt64s;
			FString ReturnValue;
		};
		static const UECodeGen_Private::FInt64PropertyParams NewProp_InInt64s_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_InInt64s_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_InInt64s;
		static const UECodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FInt64PropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt64_Wrapper_Statics::NewProp_InInt64s_Inner = { "InInt64s", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Int64, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt64_Wrapper_Statics::NewProp_InInt64s_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt64_Wrapper_Statics::NewProp_InInt64s = { "InInt64s", nullptr, (EPropertyFlags)0x0010000008000182, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeTArrayInt64_Wrapper_Parms, InInt64s), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt64_Wrapper_Statics::NewProp_InInt64s_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt64_Wrapper_Statics::NewProp_InInt64s_MetaData)) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt64_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeTArrayInt64_Wrapper_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt64_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt64_Wrapper_Statics::NewProp_InInt64s_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt64_Wrapper_Statics::NewProp_InInt64s,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt64_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt64_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|Serialize Array" },
		{ "DisplayName", "Serialize Int64 Array" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt64_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "SerializeTArrayInt64_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt64_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventSerializeTArrayInt64_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt64_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt64_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt64_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt64_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt64_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt64_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventSerializeTArrayInt_Wrapper_Parms
		{
			TArray<int32> InInts;
			FString ReturnValue;
		};
		static const UECodeGen_Private::FIntPropertyParams NewProp_InInts_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_InInts_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_InInts;
		static const UECodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FIntPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt_Wrapper_Statics::NewProp_InInts_Inner = { "InInts", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt_Wrapper_Statics::NewProp_InInts_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt_Wrapper_Statics::NewProp_InInts = { "InInts", nullptr, (EPropertyFlags)0x0010000008000182, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeTArrayInt_Wrapper_Parms, InInts), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt_Wrapper_Statics::NewProp_InInts_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt_Wrapper_Statics::NewProp_InInts_MetaData)) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeTArrayInt_Wrapper_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt_Wrapper_Statics::NewProp_InInts_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt_Wrapper_Statics::NewProp_InInts,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|Serialize Array" },
		{ "Comment", "//------- SERIALIZE ARRAYS --------//\n" },
		{ "DisplayName", "Serialize Int Array" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
		{ "ToolTip", "------- SERIALIZE ARRAYS --------" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "SerializeTArrayInt_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventSerializeTArrayInt_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayUInt8_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventSerializeTArrayUInt8_Wrapper_Parms
		{
			TArray<uint8> InUInt8s;
			FString ReturnValue;
		};
		static const UECodeGen_Private::FBytePropertyParams NewProp_InUInt8s_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_InUInt8s_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_InUInt8s;
		static const UECodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayUInt8_Wrapper_Statics::NewProp_InUInt8s_Inner = { "InUInt8s", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayUInt8_Wrapper_Statics::NewProp_InUInt8s_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayUInt8_Wrapper_Statics::NewProp_InUInt8s = { "InUInt8s", nullptr, (EPropertyFlags)0x0010000008000182, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeTArrayUInt8_Wrapper_Parms, InUInt8s), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayUInt8_Wrapper_Statics::NewProp_InUInt8s_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayUInt8_Wrapper_Statics::NewProp_InUInt8s_MetaData)) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayUInt8_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeTArrayUInt8_Wrapper_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayUInt8_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayUInt8_Wrapper_Statics::NewProp_InUInt8s_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayUInt8_Wrapper_Statics::NewProp_InUInt8s,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayUInt8_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayUInt8_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|Serialize Array" },
		{ "DisplayName", "Serialize Byte Array" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayUInt8_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "SerializeTArrayUInt8_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayUInt8_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventSerializeTArrayUInt8_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayUInt8_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayUInt8_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayUInt8_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayUInt8_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayUInt8_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayUInt8_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTextArrayAsync_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventSerializeTextArrayAsync_Parms
		{
			UObject* WorldContextObject;
			FLatentActionInfo LatentInfo;
			TArray<FText> arrayToSerialize;
			FString serializedString;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UECodeGen_Private::FStructPropertyParams NewProp_LatentInfo;
		static const UECodeGen_Private::FTextPropertyParams NewProp_arrayToSerialize_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_arrayToSerialize;
		static const UECodeGen_Private::FStrPropertyParams NewProp_serializedString;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTextArrayAsync_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeTextArrayAsync_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTextArrayAsync_Statics::NewProp_LatentInfo = { "LatentInfo", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeTextArrayAsync_Parms, LatentInfo), Z_Construct_UScriptStruct_FLatentActionInfo, METADATA_PARAMS(nullptr, 0) }; // 2194022737
	const UECodeGen_Private::FTextPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTextArrayAsync_Statics::NewProp_arrayToSerialize_Inner = { "arrayToSerialize", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTextArrayAsync_Statics::NewProp_arrayToSerialize = { "arrayToSerialize", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeTextArrayAsync_Parms, arrayToSerialize), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTextArrayAsync_Statics::NewProp_serializedString = { "serializedString", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeTextArrayAsync_Parms, serializedString), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTextArrayAsync_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTextArrayAsync_Statics::NewProp_WorldContextObject,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTextArrayAsync_Statics::NewProp_LatentInfo,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTextArrayAsync_Statics::NewProp_arrayToSerialize_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTextArrayAsync_Statics::NewProp_arrayToSerialize,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTextArrayAsync_Statics::NewProp_serializedString,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTextArrayAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|Serialize Array" },
		{ "Latent", "" },
		{ "LatentInfo", "LatentInfo" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
		{ "ToolTip", "Serializes the Array using Async Task so eventhough it's very large the FPS doesn't get affected since it's not run on the Game Thread" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTextArrayAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "SerializeTextArrayAsync", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTextArrayAsync_Statics::RapidJsonUEFunctionLibrary_eventSerializeTextArrayAsync_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTextArrayAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTextArrayAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTextArrayAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTextArrayAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTextArrayAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTextArrayAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTransformArrayAsync_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventSerializeTransformArrayAsync_Parms
		{
			UObject* WorldContextObject;
			FLatentActionInfo LatentInfo;
			TArray<FTransform> arrayToSerialize;
			FString serializedString;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UECodeGen_Private::FStructPropertyParams NewProp_LatentInfo;
		static const UECodeGen_Private::FStructPropertyParams NewProp_arrayToSerialize_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_arrayToSerialize;
		static const UECodeGen_Private::FStrPropertyParams NewProp_serializedString;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTransformArrayAsync_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeTransformArrayAsync_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTransformArrayAsync_Statics::NewProp_LatentInfo = { "LatentInfo", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeTransformArrayAsync_Parms, LatentInfo), Z_Construct_UScriptStruct_FLatentActionInfo, METADATA_PARAMS(nullptr, 0) }; // 2194022737
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTransformArrayAsync_Statics::NewProp_arrayToSerialize_Inner = { "arrayToSerialize", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTransformArrayAsync_Statics::NewProp_arrayToSerialize = { "arrayToSerialize", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeTransformArrayAsync_Parms, arrayToSerialize), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTransformArrayAsync_Statics::NewProp_serializedString = { "serializedString", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeTransformArrayAsync_Parms, serializedString), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTransformArrayAsync_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTransformArrayAsync_Statics::NewProp_WorldContextObject,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTransformArrayAsync_Statics::NewProp_LatentInfo,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTransformArrayAsync_Statics::NewProp_arrayToSerialize_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTransformArrayAsync_Statics::NewProp_arrayToSerialize,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTransformArrayAsync_Statics::NewProp_serializedString,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTransformArrayAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|Serialize Array" },
		{ "Latent", "" },
		{ "LatentInfo", "LatentInfo" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
		{ "ToolTip", "Serializes the Array using Async Task so eventhough it's very large the FPS doesn't get affected since it's not run on the Game Thread" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTransformArrayAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "SerializeTransformArrayAsync", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTransformArrayAsync_Statics::RapidJsonUEFunctionLibrary_eventSerializeTransformArrayAsync_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTransformArrayAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTransformArrayAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTransformArrayAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTransformArrayAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTransformArrayAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTransformArrayAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeUInt8_Wrapper_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventSerializeUInt8_Wrapper_Parms
		{
			uint8 UInt8Value;
			FString ReturnValue;
		};
		static const UECodeGen_Private::FBytePropertyParams NewProp_UInt8Value;
		static const UECodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeUInt8_Wrapper_Statics::NewProp_UInt8Value = { "UInt8Value", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeUInt8_Wrapper_Parms, UInt8Value), nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeUInt8_Wrapper_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeUInt8_Wrapper_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeUInt8_Wrapper_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeUInt8_Wrapper_Statics::NewProp_UInt8Value,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeUInt8_Wrapper_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeUInt8_Wrapper_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|Serialize" },
		{ "DisplayName", "Serialize Byte" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeUInt8_Wrapper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "SerializeUInt8_Wrapper", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeUInt8_Wrapper_Statics::RapidJsonUEFunctionLibrary_eventSerializeUInt8_Wrapper_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeUInt8_Wrapper_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeUInt8_Wrapper_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeUInt8_Wrapper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeUInt8_Wrapper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeUInt8_Wrapper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeUInt8_Wrapper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeUint8ArrayAsync_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventSerializeUint8ArrayAsync_Parms
		{
			UObject* WorldContextObject;
			FLatentActionInfo LatentInfo;
			TArray<uint8> arrayToSerialize;
			FString serializedString;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UECodeGen_Private::FStructPropertyParams NewProp_LatentInfo;
		static const UECodeGen_Private::FBytePropertyParams NewProp_arrayToSerialize_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_arrayToSerialize;
		static const UECodeGen_Private::FStrPropertyParams NewProp_serializedString;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeUint8ArrayAsync_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeUint8ArrayAsync_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeUint8ArrayAsync_Statics::NewProp_LatentInfo = { "LatentInfo", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeUint8ArrayAsync_Parms, LatentInfo), Z_Construct_UScriptStruct_FLatentActionInfo, METADATA_PARAMS(nullptr, 0) }; // 2194022737
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeUint8ArrayAsync_Statics::NewProp_arrayToSerialize_Inner = { "arrayToSerialize", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeUint8ArrayAsync_Statics::NewProp_arrayToSerialize = { "arrayToSerialize", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeUint8ArrayAsync_Parms, arrayToSerialize), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeUint8ArrayAsync_Statics::NewProp_serializedString = { "serializedString", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeUint8ArrayAsync_Parms, serializedString), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeUint8ArrayAsync_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeUint8ArrayAsync_Statics::NewProp_WorldContextObject,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeUint8ArrayAsync_Statics::NewProp_LatentInfo,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeUint8ArrayAsync_Statics::NewProp_arrayToSerialize_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeUint8ArrayAsync_Statics::NewProp_arrayToSerialize,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeUint8ArrayAsync_Statics::NewProp_serializedString,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeUint8ArrayAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|Serialize Array" },
		{ "Latent", "" },
		{ "LatentInfo", "LatentInfo" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
		{ "ToolTip", "Serializes the Array using Async Task so eventhough it's very large the FPS doesn't get affected since it's not run on the Game Thread" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeUint8ArrayAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "SerializeUint8ArrayAsync", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeUint8ArrayAsync_Statics::RapidJsonUEFunctionLibrary_eventSerializeUint8ArrayAsync_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeUint8ArrayAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeUint8ArrayAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeUint8ArrayAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeUint8ArrayAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeUint8ArrayAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeUint8ArrayAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeVectorArrayAsync_Statics
	{
		struct RapidJsonUEFunctionLibrary_eventSerializeVectorArrayAsync_Parms
		{
			UObject* WorldContextObject;
			FLatentActionInfo LatentInfo;
			TArray<FVector> arrayToSerialize;
			FString serializedString;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UECodeGen_Private::FStructPropertyParams NewProp_LatentInfo;
		static const UECodeGen_Private::FStructPropertyParams NewProp_arrayToSerialize_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_arrayToSerialize;
		static const UECodeGen_Private::FStrPropertyParams NewProp_serializedString;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeVectorArrayAsync_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeVectorArrayAsync_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeVectorArrayAsync_Statics::NewProp_LatentInfo = { "LatentInfo", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeVectorArrayAsync_Parms, LatentInfo), Z_Construct_UScriptStruct_FLatentActionInfo, METADATA_PARAMS(nullptr, 0) }; // 2194022737
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeVectorArrayAsync_Statics::NewProp_arrayToSerialize_Inner = { "arrayToSerialize", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeVectorArrayAsync_Statics::NewProp_arrayToSerialize = { "arrayToSerialize", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeVectorArrayAsync_Parms, arrayToSerialize), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeVectorArrayAsync_Statics::NewProp_serializedString = { "serializedString", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RapidJsonUEFunctionLibrary_eventSerializeVectorArrayAsync_Parms, serializedString), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeVectorArrayAsync_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeVectorArrayAsync_Statics::NewProp_WorldContextObject,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeVectorArrayAsync_Statics::NewProp_LatentInfo,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeVectorArrayAsync_Statics::NewProp_arrayToSerialize_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeVectorArrayAsync_Statics::NewProp_arrayToSerialize,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeVectorArrayAsync_Statics::NewProp_serializedString,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeVectorArrayAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "RapidJson|Serialize Array" },
		{ "Latent", "" },
		{ "LatentInfo", "LatentInfo" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
		{ "ToolTip", "Serializes the Array using Async Task so eventhough it's very large the FPS doesn't get affected since it's not run on the Game Thread" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeVectorArrayAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URapidJsonUEFunctionLibrary, nullptr, "SerializeVectorArrayAsync", nullptr, nullptr, sizeof(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeVectorArrayAsync_Statics::RapidJsonUEFunctionLibrary_eventSerializeVectorArrayAsync_Parms), Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeVectorArrayAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeVectorArrayAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeVectorArrayAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeVectorArrayAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeVectorArrayAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeVectorArrayAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(URapidJsonUEFunctionLibrary);
	UClass* Z_Construct_UClass_URapidJsonUEFunctionLibrary_NoRegister()
	{
		return URapidJsonUEFunctionLibrary::StaticClass();
	}
	struct Z_Construct_UClass_URapidJsonUEFunctionLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URapidJsonUEFunctionLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonAssetTools,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_URapidJsonUEFunctionLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeBool_Wrapper, "DeserializeBool_Wrapper" }, // 1056821764
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeBoolArrayAsync, "DeSerializeBoolArrayAsync" }, // 818864608
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeColorArrayAsync, "DeSerializeColorArrayAsync" }, // 810637628
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFColor_Wrapper, "DeserializeFColor_Wrapper" }, // 3882936988
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFloat_Wrapper, "DeserializeFloat_Wrapper" }, // 905052504
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeFloatArrayAsync, "DeSerializeFloatArrayAsync" }, // 1102942619
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFName_Wrapper, "DeserializeFName_Wrapper" }, // 865616377
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFRotator_Wrapper, "DeserializeFRotator_Wrapper" }, // 555753030
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFString_Wrapper, "DeserializeFString_Wrapper" }, // 1470016417
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFText_Wrapper, "DeserializeFText_Wrapper" }, // 1446996454
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFTransform_Wrapper, "DeserializeFTransform_Wrapper" }, // 2375444831
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeFVector_Wrapper, "DeserializeFVector_Wrapper" }, // 1848082959
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt32ArrayAsync, "DeSerializeInt32ArrayAsync" }, // 1095717174
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeInt64_Wrapper, "DeserializeInt64_Wrapper" }, // 2833503837
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeInt64ArrayAsync, "DeSerializeInt64ArrayAsync" }, // 1871799033
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeInt_Wrapper, "DeserializeInt_Wrapper" }, // 2364175613
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeNameArrayAsync, "DeSerializeNameArrayAsync" }, // 2047520179
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeRotatorArrayAsync, "DeSerializeRotatorArrayAsync" }, // 722430463
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeStringArrayAsync, "DeSerializeStringArrayAsync" }, // 354519803
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayBool_Wrapper, "DeserializeTArrayBool_Wrapper" }, // 3194238150
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFColor_Wrapper, "DeserializeTArrayFColor_Wrapper" }, // 1852099581
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFloat_Wrapper, "DeserializeTArrayFloat_Wrapper" }, // 3401694742
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFName_Wrapper, "DeserializeTArrayFName_Wrapper" }, // 3731107913
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFRotator_Wrapper, "DeserializeTArrayFRotator_Wrapper" }, // 271907553
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFString_Wrapper, "DeserializeTArrayFString_Wrapper" }, // 987681015
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFText_Wrapper, "DeserializeTArrayFText_Wrapper" }, // 2587324269
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFTransform_Wrapper, "DeserializeTArrayFTransform_Wrapper" }, // 3041542248
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayFVector_Wrapper, "DeserializeTArrayFVector_Wrapper" }, // 3718142856
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt64_Wrapper, "DeserializeTArrayInt64_Wrapper" }, // 1097777564
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayInt_Wrapper, "DeserializeTArrayInt_Wrapper" }, // 3810909502
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeTArrayUInt8_Wrapper, "DeserializeTArrayUInt8_Wrapper" }, // 224222472
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTextArrayAsync, "DeSerializeTextArrayAsync" }, // 3286574202
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeTransformArrayAsync, "DeSerializeTransformArrayAsync" }, // 340234189
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeserializeUInt8_Wrapper, "DeserializeUInt8_Wrapper" }, // 1704340627
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeUInt8ArrayAsync, "DeSerializeUInt8ArrayAsync" }, // 1372283326
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_DeSerializeVectorArrayAsync, "DeSerializeVectorArrayAsync" }, // 9153887
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBool_Wrapper, "SerializeBool_Wrapper" }, // 210827785
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeBoolArrayAsync, "SerializeBoolArrayAsync" }, // 3108152884
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeColorArrayAsync, "SerializeColorArrayAsync" }, // 2807684870
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFColor_Wrapper, "SerializeFColor_Wrapper" }, // 1437223194
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFloat_Wrapper, "SerializeFloat_Wrapper" }, // 234359459
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFloatArrayAsync, "SerializeFloatArrayAsync" }, // 3871355239
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFName_Wrapper, "SerializeFName_Wrapper" }, // 2326512014
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFRotator_Wrapper, "SerializeFRotator_Wrapper" }, // 3038591281
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFString_Wrapper, "SerializeFString_Wrapper" }, // 3302850526
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFText_Wrapper, "SerializeFText_Wrapper" }, // 2096417503
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFTransform_Wrapper, "SerializeFTransform_Wrapper" }, // 3453429345
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeFVector_Wrapper, "SerializeFVector_Wrapper" }, // 883733672
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt32ArrayAsync, "SerializeInt32ArrayAsync" }, // 2114001203
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt64_Wrapper, "SerializeInt64_Wrapper" }, // 435864792
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt64ArrayAsync, "SerializeInt64ArrayAsync" }, // 1286452892
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeInt_Wrapper, "SerializeInt_Wrapper" }, // 1675985291
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeNameArrayAsync, "SerializeNameArrayAsync" }, // 35638303
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeRotatorArrayAsync, "SerializeRotatorArrayAsync" }, // 2349916579
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeStringArrayAsync, "SerializeStringArrayAsync" }, // 4020275729
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayBool_Wrapper, "SerializeTArrayBool_Wrapper" }, // 4285611517
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFColor_Wrapper, "SerializeTArrayFColor_Wrapper" }, // 3749422824
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFloat_Wrapper, "SerializeTArrayFloat_Wrapper" }, // 1051568307
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFName_Wrapper, "SerializeTArrayFName_Wrapper" }, // 3803591634
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFRotator_Wrapper, "SerializeTArrayFRotator_Wrapper" }, // 1915199591
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFString_Wrapper, "SerializeTArrayFString_Wrapper" }, // 3558584477
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFText_Wrapper, "SerializeTArrayFText_Wrapper" }, // 1353749317
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFTransform_Wrapper, "SerializeTArrayFTransform_Wrapper" }, // 1857825557
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayFVector_Wrapper, "SerializeTArrayFVector_Wrapper" }, // 750249014
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt64_Wrapper, "SerializeTArrayInt64_Wrapper" }, // 2981182526
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayInt_Wrapper, "SerializeTArrayInt_Wrapper" }, // 3995251545
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTArrayUInt8_Wrapper, "SerializeTArrayUInt8_Wrapper" }, // 2250493862
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTextArrayAsync, "SerializeTextArrayAsync" }, // 1084637968
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeTransformArrayAsync, "SerializeTransformArrayAsync" }, // 1336852689
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeUInt8_Wrapper, "SerializeUInt8_Wrapper" }, // 884246332
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeUint8ArrayAsync, "SerializeUint8ArrayAsync" }, // 1231328928
		{ &Z_Construct_UFunction_URapidJsonUEFunctionLibrary_SerializeVectorArrayAsync, "SerializeVectorArrayAsync" }, // 813948072
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URapidJsonUEFunctionLibrary_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "JsonTool/RapidJsonUEFunctionLibrary.h" },
		{ "ModuleRelativePath", "Public/JsonTool/RapidJsonUEFunctionLibrary.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_URapidJsonUEFunctionLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URapidJsonUEFunctionLibrary>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_URapidJsonUEFunctionLibrary_Statics::ClassParams = {
		&URapidJsonUEFunctionLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_URapidJsonUEFunctionLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URapidJsonUEFunctionLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URapidJsonUEFunctionLibrary()
	{
		if (!Z_Registration_Info_UClass_URapidJsonUEFunctionLibrary.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_URapidJsonUEFunctionLibrary.OuterSingleton, Z_Construct_UClass_URapidJsonUEFunctionLibrary_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_URapidJsonUEFunctionLibrary.OuterSingleton;
	}
	template<> COMMONASSETTOOLS_API UClass* StaticClass<URapidJsonUEFunctionLibrary>()
	{
		return URapidJsonUEFunctionLibrary::StaticClass();
	}
	URapidJsonUEFunctionLibrary::URapidJsonUEFunctionLibrary(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(URapidJsonUEFunctionLibrary);
	URapidJsonUEFunctionLibrary::~URapidJsonUEFunctionLibrary() {}
	struct Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_JsonTool_RapidJsonUEFunctionLibrary_h_Statics
	{
		static const FStructRegisterCompiledInInfo ScriptStructInfo[];
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FStructRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_JsonTool_RapidJsonUEFunctionLibrary_h_Statics::ScriptStructInfo[] = {
		{ FCustomStructJsonSerializationParent::StaticStruct, Z_Construct_UScriptStruct_FCustomStructJsonSerializationParent_Statics::NewStructOps, TEXT("CustomStructJsonSerializationParent"), &Z_Registration_Info_UScriptStruct_CustomStructJsonSerializationParent, CONSTRUCT_RELOAD_VERSION_INFO(FStructReloadVersionInfo, sizeof(FCustomStructJsonSerializationParent), 3231690561U) },
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_JsonTool_RapidJsonUEFunctionLibrary_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_URapidJsonUEFunctionLibrary, URapidJsonUEFunctionLibrary::StaticClass, TEXT("URapidJsonUEFunctionLibrary"), &Z_Registration_Info_UClass_URapidJsonUEFunctionLibrary, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(URapidJsonUEFunctionLibrary), 920993477U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_JsonTool_RapidJsonUEFunctionLibrary_h_1631599341(TEXT("/Script/CommonAssetTools"),
		Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_JsonTool_RapidJsonUEFunctionLibrary_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_JsonTool_RapidJsonUEFunctionLibrary_h_Statics::ClassInfo),
		Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_JsonTool_RapidJsonUEFunctionLibrary_h_Statics::ScriptStructInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_JsonTool_RapidJsonUEFunctionLibrary_h_Statics::ScriptStructInfo),
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
