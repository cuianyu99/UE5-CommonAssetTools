// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "NetworkTool/CommonNetWorkToolFactory.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UCommonNetworkTool;
#ifdef COMMONASSETTOOLS_CommonNetWorkToolFactory_generated_h
#error "CommonNetWorkToolFactory.generated.h already included, missing '#pragma once' in CommonNetWorkToolFactory.h"
#endif
#define COMMONASSETTOOLS_CommonNetWorkToolFactory_generated_h

#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_NetworkTool_CommonNetWorkToolFactory_h_12_SPARSE_DATA
#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_NetworkTool_CommonNetWorkToolFactory_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execCreateTool);


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_NetworkTool_CommonNetWorkToolFactory_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execCreateTool);


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_NetworkTool_CommonNetWorkToolFactory_h_12_ACCESSORS
#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_NetworkTool_CommonNetWorkToolFactory_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonNetWorkToolFactory(); \
	friend struct Z_Construct_UClass_UCommonNetWorkToolFactory_Statics; \
public: \
	DECLARE_CLASS(UCommonNetWorkToolFactory, UCommonAssetToolFactoryBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonAssetTools"), NO_API) \
	DECLARE_SERIALIZER(UCommonNetWorkToolFactory)


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_NetworkTool_CommonNetWorkToolFactory_h_12_INCLASS \
private: \
	static void StaticRegisterNativesUCommonNetWorkToolFactory(); \
	friend struct Z_Construct_UClass_UCommonNetWorkToolFactory_Statics; \
public: \
	DECLARE_CLASS(UCommonNetWorkToolFactory, UCommonAssetToolFactoryBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonAssetTools"), NO_API) \
	DECLARE_SERIALIZER(UCommonNetWorkToolFactory)


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_NetworkTool_CommonNetWorkToolFactory_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonNetWorkToolFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonNetWorkToolFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonNetWorkToolFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonNetWorkToolFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonNetWorkToolFactory(UCommonNetWorkToolFactory&&); \
	NO_API UCommonNetWorkToolFactory(const UCommonNetWorkToolFactory&); \
public: \
	NO_API virtual ~UCommonNetWorkToolFactory();


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_NetworkTool_CommonNetWorkToolFactory_h_12_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonNetWorkToolFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonNetWorkToolFactory(UCommonNetWorkToolFactory&&); \
	NO_API UCommonNetWorkToolFactory(const UCommonNetWorkToolFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonNetWorkToolFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonNetWorkToolFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonNetWorkToolFactory) \
	NO_API virtual ~UCommonNetWorkToolFactory();


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_NetworkTool_CommonNetWorkToolFactory_h_9_PROLOG
#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_NetworkTool_CommonNetWorkToolFactory_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_NetworkTool_CommonNetWorkToolFactory_h_12_SPARSE_DATA \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_NetworkTool_CommonNetWorkToolFactory_h_12_RPC_WRAPPERS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_NetworkTool_CommonNetWorkToolFactory_h_12_ACCESSORS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_NetworkTool_CommonNetWorkToolFactory_h_12_INCLASS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_NetworkTool_CommonNetWorkToolFactory_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_NetworkTool_CommonNetWorkToolFactory_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_NetworkTool_CommonNetWorkToolFactory_h_12_SPARSE_DATA \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_NetworkTool_CommonNetWorkToolFactory_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_NetworkTool_CommonNetWorkToolFactory_h_12_ACCESSORS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_NetworkTool_CommonNetWorkToolFactory_h_12_INCLASS_NO_PURE_DECLS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_NetworkTool_CommonNetWorkToolFactory_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONASSETTOOLS_API UClass* StaticClass<class UCommonNetWorkToolFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_NetworkTool_CommonNetWorkToolFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
