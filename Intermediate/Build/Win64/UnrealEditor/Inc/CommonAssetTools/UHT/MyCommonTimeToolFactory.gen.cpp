// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TimeTool/MyCommonTimeToolFactory.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMyCommonTimeToolFactory() {}
// Cross Module References
	COMMONASSETTOOLS_API UClass* Z_Construct_UClass_UCommonAssetToolFactoryBase();
	COMMONASSETTOOLS_API UClass* Z_Construct_UClass_UCommonTimeTool_NoRegister();
	COMMONASSETTOOLS_API UClass* Z_Construct_UClass_UMyCommonTimeToolFactory();
	COMMONASSETTOOLS_API UClass* Z_Construct_UClass_UMyCommonTimeToolFactory_NoRegister();
	UPackage* Z_Construct_UPackage__Script_CommonAssetTools();
// End Cross Module References
	DEFINE_FUNCTION(UMyCommonTimeToolFactory::execCreateTool)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UCommonTimeTool**)Z_Param__Result=P_THIS->CreateTool();
		P_NATIVE_END;
	}
	void UMyCommonTimeToolFactory::StaticRegisterNativesUMyCommonTimeToolFactory()
	{
		UClass* Class = UMyCommonTimeToolFactory::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CreateTool", &UMyCommonTimeToolFactory::execCreateTool },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMyCommonTimeToolFactory_CreateTool_Statics
	{
		struct MyCommonTimeToolFactory_eventCreateTool_Parms
		{
			UCommonTimeTool* ReturnValue;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMyCommonTimeToolFactory_CreateTool_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(MyCommonTimeToolFactory_eventCreateTool_Parms, ReturnValue), Z_Construct_UClass_UCommonTimeTool_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMyCommonTimeToolFactory_CreateTool_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMyCommonTimeToolFactory_CreateTool_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMyCommonTimeToolFactory_CreateTool_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonAssetTool" },
		{ "ModuleRelativePath", "Public/TimeTool/MyCommonTimeToolFactory.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UMyCommonTimeToolFactory_CreateTool_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMyCommonTimeToolFactory, nullptr, "CreateTool", nullptr, nullptr, sizeof(Z_Construct_UFunction_UMyCommonTimeToolFactory_CreateTool_Statics::MyCommonTimeToolFactory_eventCreateTool_Parms), Z_Construct_UFunction_UMyCommonTimeToolFactory_CreateTool_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMyCommonTimeToolFactory_CreateTool_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMyCommonTimeToolFactory_CreateTool_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMyCommonTimeToolFactory_CreateTool_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMyCommonTimeToolFactory_CreateTool()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UMyCommonTimeToolFactory_CreateTool_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UMyCommonTimeToolFactory);
	UClass* Z_Construct_UClass_UMyCommonTimeToolFactory_NoRegister()
	{
		return UMyCommonTimeToolFactory::StaticClass();
	}
	struct Z_Construct_UClass_UMyCommonTimeToolFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_GCPointer_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_GCPointer;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMyCommonTimeToolFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCommonAssetToolFactoryBase,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonAssetTools,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMyCommonTimeToolFactory_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMyCommonTimeToolFactory_CreateTool, "CreateTool" }, // 565320643
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMyCommonTimeToolFactory_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "TimeTool/MyCommonTimeToolFactory.h" },
		{ "ModuleRelativePath", "Public/TimeTool/MyCommonTimeToolFactory.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMyCommonTimeToolFactory_Statics::NewProp_GCPointer_MetaData[] = {
		{ "ModuleRelativePath", "Public/TimeTool/MyCommonTimeToolFactory.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMyCommonTimeToolFactory_Statics::NewProp_GCPointer = { "GCPointer", nullptr, (EPropertyFlags)0x0010000000000000, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UMyCommonTimeToolFactory, GCPointer), Z_Construct_UClass_UCommonTimeTool_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMyCommonTimeToolFactory_Statics::NewProp_GCPointer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMyCommonTimeToolFactory_Statics::NewProp_GCPointer_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMyCommonTimeToolFactory_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMyCommonTimeToolFactory_Statics::NewProp_GCPointer,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMyCommonTimeToolFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMyCommonTimeToolFactory>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UMyCommonTimeToolFactory_Statics::ClassParams = {
		&UMyCommonTimeToolFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UMyCommonTimeToolFactory_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UMyCommonTimeToolFactory_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMyCommonTimeToolFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMyCommonTimeToolFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMyCommonTimeToolFactory()
	{
		if (!Z_Registration_Info_UClass_UMyCommonTimeToolFactory.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UMyCommonTimeToolFactory.OuterSingleton, Z_Construct_UClass_UMyCommonTimeToolFactory_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UMyCommonTimeToolFactory.OuterSingleton;
	}
	template<> COMMONASSETTOOLS_API UClass* StaticClass<UMyCommonTimeToolFactory>()
	{
		return UMyCommonTimeToolFactory::StaticClass();
	}
	UMyCommonTimeToolFactory::UMyCommonTimeToolFactory(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMyCommonTimeToolFactory);
	UMyCommonTimeToolFactory::~UMyCommonTimeToolFactory() {}
	struct Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_TimeTool_MyCommonTimeToolFactory_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_TimeTool_MyCommonTimeToolFactory_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UMyCommonTimeToolFactory, UMyCommonTimeToolFactory::StaticClass, TEXT("UMyCommonTimeToolFactory"), &Z_Registration_Info_UClass_UMyCommonTimeToolFactory, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UMyCommonTimeToolFactory), 1955318179U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_TimeTool_MyCommonTimeToolFactory_h_858153305(TEXT("/Script/CommonAssetTools"),
		Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_TimeTool_MyCommonTimeToolFactory_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_TimeTool_MyCommonTimeToolFactory_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
