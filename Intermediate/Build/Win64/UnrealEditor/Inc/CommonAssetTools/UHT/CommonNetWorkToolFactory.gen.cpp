// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NetworkTool/CommonNetWorkToolFactory.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonNetWorkToolFactory() {}
// Cross Module References
	COMMONASSETTOOLS_API UClass* Z_Construct_UClass_UCommonAssetToolFactoryBase();
	COMMONASSETTOOLS_API UClass* Z_Construct_UClass_UCommonNetworkTool_NoRegister();
	COMMONASSETTOOLS_API UClass* Z_Construct_UClass_UCommonNetWorkToolFactory();
	COMMONASSETTOOLS_API UClass* Z_Construct_UClass_UCommonNetWorkToolFactory_NoRegister();
	UPackage* Z_Construct_UPackage__Script_CommonAssetTools();
// End Cross Module References
	DEFINE_FUNCTION(UCommonNetWorkToolFactory::execCreateTool)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UCommonNetworkTool**)Z_Param__Result=P_THIS->CreateTool();
		P_NATIVE_END;
	}
	void UCommonNetWorkToolFactory::StaticRegisterNativesUCommonNetWorkToolFactory()
	{
		UClass* Class = UCommonNetWorkToolFactory::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CreateTool", &UCommonNetWorkToolFactory::execCreateTool },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCommonNetWorkToolFactory_CreateTool_Statics
	{
		struct CommonNetWorkToolFactory_eventCreateTool_Parms
		{
			UCommonNetworkTool* ReturnValue;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCommonNetWorkToolFactory_CreateTool_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonNetWorkToolFactory_eventCreateTool_Parms, ReturnValue), Z_Construct_UClass_UCommonNetworkTool_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonNetWorkToolFactory_CreateTool_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonNetWorkToolFactory_CreateTool_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonNetWorkToolFactory_CreateTool_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonAssetTool" },
		{ "ModuleRelativePath", "Public/NetworkTool/CommonNetWorkToolFactory.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonNetWorkToolFactory_CreateTool_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonNetWorkToolFactory, nullptr, "CreateTool", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCommonNetWorkToolFactory_CreateTool_Statics::CommonNetWorkToolFactory_eventCreateTool_Parms), Z_Construct_UFunction_UCommonNetWorkToolFactory_CreateTool_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonNetWorkToolFactory_CreateTool_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonNetWorkToolFactory_CreateTool_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonNetWorkToolFactory_CreateTool_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonNetWorkToolFactory_CreateTool()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCommonNetWorkToolFactory_CreateTool_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UCommonNetWorkToolFactory);
	UClass* Z_Construct_UClass_UCommonNetWorkToolFactory_NoRegister()
	{
		return UCommonNetWorkToolFactory::StaticClass();
	}
	struct Z_Construct_UClass_UCommonNetWorkToolFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_GCPointer_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_GCPointer;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonNetWorkToolFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCommonAssetToolFactoryBase,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonAssetTools,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCommonNetWorkToolFactory_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCommonNetWorkToolFactory_CreateTool, "CreateTool" }, // 1257111993
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonNetWorkToolFactory_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Category", "CommonAssetTool" },
		{ "IncludePath", "NetworkTool/CommonNetWorkToolFactory.h" },
		{ "ModuleRelativePath", "Public/NetworkTool/CommonNetWorkToolFactory.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonNetWorkToolFactory_Statics::NewProp_GCPointer_MetaData[] = {
		{ "ModuleRelativePath", "Public/NetworkTool/CommonNetWorkToolFactory.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UCommonNetWorkToolFactory_Statics::NewProp_GCPointer = { "GCPointer", nullptr, (EPropertyFlags)0x0010000000000000, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UCommonNetWorkToolFactory, GCPointer), Z_Construct_UClass_UCommonNetworkTool_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCommonNetWorkToolFactory_Statics::NewProp_GCPointer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonNetWorkToolFactory_Statics::NewProp_GCPointer_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCommonNetWorkToolFactory_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonNetWorkToolFactory_Statics::NewProp_GCPointer,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonNetWorkToolFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonNetWorkToolFactory>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UCommonNetWorkToolFactory_Statics::ClassParams = {
		&UCommonNetWorkToolFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UCommonNetWorkToolFactory_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UCommonNetWorkToolFactory_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonNetWorkToolFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonNetWorkToolFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonNetWorkToolFactory()
	{
		if (!Z_Registration_Info_UClass_UCommonNetWorkToolFactory.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UCommonNetWorkToolFactory.OuterSingleton, Z_Construct_UClass_UCommonNetWorkToolFactory_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UCommonNetWorkToolFactory.OuterSingleton;
	}
	template<> COMMONASSETTOOLS_API UClass* StaticClass<UCommonNetWorkToolFactory>()
	{
		return UCommonNetWorkToolFactory::StaticClass();
	}
	UCommonNetWorkToolFactory::UCommonNetWorkToolFactory(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonNetWorkToolFactory);
	UCommonNetWorkToolFactory::~UCommonNetWorkToolFactory() {}
	struct Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_NetworkTool_CommonNetWorkToolFactory_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_NetworkTool_CommonNetWorkToolFactory_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UCommonNetWorkToolFactory, UCommonNetWorkToolFactory::StaticClass, TEXT("UCommonNetWorkToolFactory"), &Z_Registration_Info_UClass_UCommonNetWorkToolFactory, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UCommonNetWorkToolFactory), 854591287U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_NetworkTool_CommonNetWorkToolFactory_h_801626136(TEXT("/Script/CommonAssetTools"),
		Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_NetworkTool_CommonNetWorkToolFactory_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_NetworkTool_CommonNetWorkToolFactory_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
