// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ImageTool/CommonImageTool.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonImageTool() {}
// Cross Module References
	COMMONASSETTOOLS_API UClass* Z_Construct_UClass_UCommonImageTool();
	COMMONASSETTOOLS_API UClass* Z_Construct_UClass_UCommonImageTool_NoRegister();
	COMMONASSETTOOLS_API UEnum* Z_Construct_UEnum_CommonAssetTools_FEImageFormat();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	ENGINE_API UClass* Z_Construct_UClass_UTexture2D_NoRegister();
	UPackage* Z_Construct_UPackage__Script_CommonAssetTools();
// End Cross Module References
	static FEnumRegistrationInfo Z_Registration_Info_UEnum_FEImageFormat;
	static UEnum* FEImageFormat_StaticEnum()
	{
		if (!Z_Registration_Info_UEnum_FEImageFormat.OuterSingleton)
		{
			Z_Registration_Info_UEnum_FEImageFormat.OuterSingleton = GetStaticEnum(Z_Construct_UEnum_CommonAssetTools_FEImageFormat, (UObject*)Z_Construct_UPackage__Script_CommonAssetTools(), TEXT("FEImageFormat"));
		}
		return Z_Registration_Info_UEnum_FEImageFormat.OuterSingleton;
	}
	template<> COMMONASSETTOOLS_API UEnum* StaticEnum<FEImageFormat>()
	{
		return FEImageFormat_StaticEnum();
	}
	struct Z_Construct_UEnum_CommonAssetTools_FEImageFormat_Statics
	{
		static const UECodeGen_Private::FEnumeratorParam Enumerators[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[];
#endif
		static const UECodeGen_Private::FEnumParams EnumParams;
	};
	const UECodeGen_Private::FEnumeratorParam Z_Construct_UEnum_CommonAssetTools_FEImageFormat_Statics::Enumerators[] = {
		{ "FEImageFormat::Invalid", (int64)FEImageFormat::Invalid },
		{ "FEImageFormat::PNG", (int64)FEImageFormat::PNG },
		{ "FEImageFormat::JPEG", (int64)FEImageFormat::JPEG },
		{ "FEImageFormat::GrayscaleJPEG", (int64)FEImageFormat::GrayscaleJPEG },
		{ "FEImageFormat::BMP", (int64)FEImageFormat::BMP },
		{ "FEImageFormat::ICO", (int64)FEImageFormat::ICO },
		{ "FEImageFormat::EXR", (int64)FEImageFormat::EXR },
		{ "FEImageFormat::ICNS", (int64)FEImageFormat::ICNS },
		{ "FEImageFormat::TGA", (int64)FEImageFormat::TGA },
		{ "FEImageFormat::HDR", (int64)FEImageFormat::HDR },
		{ "FEImageFormat::TIFF", (int64)FEImageFormat::TIFF },
		{ "FEImageFormat::DDS", (int64)FEImageFormat::DDS },
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UEnum_CommonAssetTools_FEImageFormat_Statics::Enum_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "BMP.Comment", "/** Windows Bitmap. */" },
		{ "BMP.Name", "FEImageFormat::BMP" },
		{ "BMP.ToolTip", "Windows Bitmap." },
		{ "Category", "CommonAssetTool" },
		{ "DDS.Comment", "/** DirectDraw Surface */" },
		{ "DDS.Name", "FEImageFormat::DDS" },
		{ "DDS.ToolTip", "DirectDraw Surface" },
		{ "EXR.Comment", "/** OpenEXR (HDR) image file format. */" },
		{ "EXR.Name", "FEImageFormat::EXR" },
		{ "EXR.ToolTip", "OpenEXR (HDR) image file format." },
		{ "GrayscaleJPEG.Comment", "/** Single channel JPEG. */" },
		{ "GrayscaleJPEG.Name", "FEImageFormat::GrayscaleJPEG" },
		{ "GrayscaleJPEG.ToolTip", "Single channel JPEG." },
		{ "HDR.Comment", "/** Hdr file from radiance using RGBE */" },
		{ "HDR.Name", "FEImageFormat::HDR" },
		{ "HDR.ToolTip", "Hdr file from radiance using RGBE" },
		{ "ICNS.Comment", "/** Mac icon. */" },
		{ "ICNS.Name", "FEImageFormat::ICNS" },
		{ "ICNS.ToolTip", "Mac icon." },
		{ "ICO.Comment", "/** Windows Icon resource. */" },
		{ "ICO.Name", "FEImageFormat::ICO" },
		{ "ICO.ToolTip", "Windows Icon resource." },
		{ "Invalid.Comment", "/** Invalid or unrecognized format. */" },
		{ "Invalid.Name", "FEImageFormat::Invalid" },
		{ "Invalid.ToolTip", "Invalid or unrecognized format." },
		{ "JPEG.Comment", "/** Joint Photographic Experts Group. */" },
		{ "JPEG.Name", "FEImageFormat::JPEG" },
		{ "JPEG.ToolTip", "Joint Photographic Experts Group." },
		{ "ModuleRelativePath", "Public/ImageTool/CommonImageTool.h" },
		{ "PNG.Comment", "/** Portable Network Graphics. */" },
		{ "PNG.Name", "FEImageFormat::PNG" },
		{ "PNG.ToolTip", "Portable Network Graphics." },
		{ "TGA.Comment", "/** Truevision TGA / TARGA */" },
		{ "TGA.Name", "FEImageFormat::TGA" },
		{ "TGA.ToolTip", "Truevision TGA / TARGA" },
		{ "TIFF.Comment", "/** Tag Image File Format files */" },
		{ "TIFF.Name", "FEImageFormat::TIFF" },
		{ "TIFF.ToolTip", "Tag Image File Format files" },
	};
#endif
	const UECodeGen_Private::FEnumParams Z_Construct_UEnum_CommonAssetTools_FEImageFormat_Statics::EnumParams = {
		(UObject*(*)())Z_Construct_UPackage__Script_CommonAssetTools,
		nullptr,
		"FEImageFormat",
		"FEImageFormat",
		Z_Construct_UEnum_CommonAssetTools_FEImageFormat_Statics::Enumerators,
		UE_ARRAY_COUNT(Z_Construct_UEnum_CommonAssetTools_FEImageFormat_Statics::Enumerators),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EEnumFlags::None,
		(uint8)UEnum::ECppForm::EnumClass,
		METADATA_PARAMS(Z_Construct_UEnum_CommonAssetTools_FEImageFormat_Statics::Enum_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UEnum_CommonAssetTools_FEImageFormat_Statics::Enum_MetaDataParams))
	};
	UEnum* Z_Construct_UEnum_CommonAssetTools_FEImageFormat()
	{
		if (!Z_Registration_Info_UEnum_FEImageFormat.InnerSingleton)
		{
			UECodeGen_Private::ConstructUEnum(Z_Registration_Info_UEnum_FEImageFormat.InnerSingleton, Z_Construct_UEnum_CommonAssetTools_FEImageFormat_Statics::EnumParams);
		}
		return Z_Registration_Info_UEnum_FEImageFormat.InnerSingleton;
	}
	DEFINE_FUNCTION(UCommonImageTool::execImportImageWithBinary)
	{
		P_GET_TARRAY_REF(uint8,Z_Param_Out_ImgBin);
		P_GET_ENUM(FEImageFormat,Z_Param_ImgFormat);
		P_GET_OBJECT_REF(UTexture2D,Z_Param_Out_ImgObj);
		P_GET_PROPERTY_REF(FIntProperty,Z_Param_Out_OutWidth);
		P_GET_PROPERTY_REF(FIntProperty,Z_Param_Out_OutHeight);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ImportImageWithBinary(Z_Param_Out_ImgBin,FEImageFormat(Z_Param_ImgFormat),Z_Param_Out_ImgObj,Z_Param_Out_OutWidth,Z_Param_Out_OutHeight);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonImageTool::execImportImageWithPath)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_ImagePath);
		P_GET_OBJECT_REF(UTexture2D,Z_Param_Out_ImgObj);
		P_GET_PROPERTY_REF(FIntProperty,Z_Param_Out_OutWidth);
		P_GET_PROPERTY_REF(FIntProperty,Z_Param_Out_OutHeight);
		P_GET_TARRAY_REF(uint8,Z_Param_Out_ImgBin);
		P_GET_ENUM_REF(FEImageFormat,Z_Param_Out_ImgFormat);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ImportImageWithPath(Z_Param_ImagePath,Z_Param_Out_ImgObj,Z_Param_Out_OutWidth,Z_Param_Out_OutHeight,Z_Param_Out_ImgBin,(FEImageFormat&)(Z_Param_Out_ImgFormat));
		P_NATIVE_END;
	}
	void UCommonImageTool::StaticRegisterNativesUCommonImageTool()
	{
		UClass* Class = UCommonImageTool::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ImportImageWithBinary", &UCommonImageTool::execImportImageWithBinary },
			{ "ImportImageWithPath", &UCommonImageTool::execImportImageWithPath },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCommonImageTool_ImportImageWithBinary_Statics
	{
		struct CommonImageTool_eventImportImageWithBinary_Parms
		{
			TArray<uint8> ImgBin;
			FEImageFormat ImgFormat;
			UTexture2D* ImgObj;
			int32 OutWidth;
			int32 OutHeight;
		};
		static const UECodeGen_Private::FBytePropertyParams NewProp_ImgBin_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ImgBin_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_ImgBin;
		static const UECodeGen_Private::FBytePropertyParams NewProp_ImgFormat_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_ImgFormat;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_ImgObj;
		static const UECodeGen_Private::FIntPropertyParams NewProp_OutWidth;
		static const UECodeGen_Private::FIntPropertyParams NewProp_OutHeight;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UCommonImageTool_ImportImageWithBinary_Statics::NewProp_ImgBin_Inner = { "ImgBin", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonImageTool_ImportImageWithBinary_Statics::NewProp_ImgBin_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UCommonImageTool_ImportImageWithBinary_Statics::NewProp_ImgBin = { "ImgBin", nullptr, (EPropertyFlags)0x0010000008000182, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonImageTool_eventImportImageWithBinary_Parms, ImgBin), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UCommonImageTool_ImportImageWithBinary_Statics::NewProp_ImgBin_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonImageTool_ImportImageWithBinary_Statics::NewProp_ImgBin_MetaData)) };
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UCommonImageTool_ImportImageWithBinary_Statics::NewProp_ImgFormat_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UCommonImageTool_ImportImageWithBinary_Statics::NewProp_ImgFormat = { "ImgFormat", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonImageTool_eventImportImageWithBinary_Parms, ImgFormat), Z_Construct_UEnum_CommonAssetTools_FEImageFormat, METADATA_PARAMS(nullptr, 0) }; // 2587607141
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCommonImageTool_ImportImageWithBinary_Statics::NewProp_ImgObj = { "ImgObj", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonImageTool_eventImportImageWithBinary_Parms, ImgObj), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UCommonImageTool_ImportImageWithBinary_Statics::NewProp_OutWidth = { "OutWidth", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonImageTool_eventImportImageWithBinary_Parms, OutWidth), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UCommonImageTool_ImportImageWithBinary_Statics::NewProp_OutHeight = { "OutHeight", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonImageTool_eventImportImageWithBinary_Parms, OutHeight), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonImageTool_ImportImageWithBinary_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonImageTool_ImportImageWithBinary_Statics::NewProp_ImgBin_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonImageTool_ImportImageWithBinary_Statics::NewProp_ImgBin,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonImageTool_ImportImageWithBinary_Statics::NewProp_ImgFormat_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonImageTool_ImportImageWithBinary_Statics::NewProp_ImgFormat,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonImageTool_ImportImageWithBinary_Statics::NewProp_ImgObj,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonImageTool_ImportImageWithBinary_Statics::NewProp_OutWidth,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonImageTool_ImportImageWithBinary_Statics::NewProp_OutHeight,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonImageTool_ImportImageWithBinary_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonAssetTool" },
		{ "ModuleRelativePath", "Public/ImageTool/CommonImageTool.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonImageTool_ImportImageWithBinary_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonImageTool, nullptr, "ImportImageWithBinary", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCommonImageTool_ImportImageWithBinary_Statics::CommonImageTool_eventImportImageWithBinary_Parms), Z_Construct_UFunction_UCommonImageTool_ImportImageWithBinary_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonImageTool_ImportImageWithBinary_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonImageTool_ImportImageWithBinary_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonImageTool_ImportImageWithBinary_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonImageTool_ImportImageWithBinary()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCommonImageTool_ImportImageWithBinary_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonImageTool_ImportImageWithPath_Statics
	{
		struct CommonImageTool_eventImportImageWithPath_Parms
		{
			FString ImagePath;
			UTexture2D* ImgObj;
			int32 OutWidth;
			int32 OutHeight;
			TArray<uint8> ImgBin;
			FEImageFormat ImgFormat;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ImagePath_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_ImagePath;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_ImgObj;
		static const UECodeGen_Private::FIntPropertyParams NewProp_OutWidth;
		static const UECodeGen_Private::FIntPropertyParams NewProp_OutHeight;
		static const UECodeGen_Private::FBytePropertyParams NewProp_ImgBin_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_ImgBin;
		static const UECodeGen_Private::FBytePropertyParams NewProp_ImgFormat_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_ImgFormat;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonImageTool_ImportImageWithPath_Statics::NewProp_ImagePath_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UCommonImageTool_ImportImageWithPath_Statics::NewProp_ImagePath = { "ImagePath", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonImageTool_eventImportImageWithPath_Parms, ImagePath), METADATA_PARAMS(Z_Construct_UFunction_UCommonImageTool_ImportImageWithPath_Statics::NewProp_ImagePath_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonImageTool_ImportImageWithPath_Statics::NewProp_ImagePath_MetaData)) };
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCommonImageTool_ImportImageWithPath_Statics::NewProp_ImgObj = { "ImgObj", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonImageTool_eventImportImageWithPath_Parms, ImgObj), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UCommonImageTool_ImportImageWithPath_Statics::NewProp_OutWidth = { "OutWidth", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonImageTool_eventImportImageWithPath_Parms, OutWidth), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UCommonImageTool_ImportImageWithPath_Statics::NewProp_OutHeight = { "OutHeight", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonImageTool_eventImportImageWithPath_Parms, OutHeight), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UCommonImageTool_ImportImageWithPath_Statics::NewProp_ImgBin_Inner = { "ImgBin", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UCommonImageTool_ImportImageWithPath_Statics::NewProp_ImgBin = { "ImgBin", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonImageTool_eventImportImageWithPath_Parms, ImgBin), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UCommonImageTool_ImportImageWithPath_Statics::NewProp_ImgFormat_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UCommonImageTool_ImportImageWithPath_Statics::NewProp_ImgFormat = { "ImgFormat", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonImageTool_eventImportImageWithPath_Parms, ImgFormat), Z_Construct_UEnum_CommonAssetTools_FEImageFormat, METADATA_PARAMS(nullptr, 0) }; // 2587607141
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonImageTool_ImportImageWithPath_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonImageTool_ImportImageWithPath_Statics::NewProp_ImagePath,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonImageTool_ImportImageWithPath_Statics::NewProp_ImgObj,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonImageTool_ImportImageWithPath_Statics::NewProp_OutWidth,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonImageTool_ImportImageWithPath_Statics::NewProp_OutHeight,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonImageTool_ImportImageWithPath_Statics::NewProp_ImgBin_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonImageTool_ImportImageWithPath_Statics::NewProp_ImgBin,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonImageTool_ImportImageWithPath_Statics::NewProp_ImgFormat_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonImageTool_ImportImageWithPath_Statics::NewProp_ImgFormat,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonImageTool_ImportImageWithPath_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonAssetTool" },
		{ "ModuleRelativePath", "Public/ImageTool/CommonImageTool.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonImageTool_ImportImageWithPath_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonImageTool, nullptr, "ImportImageWithPath", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCommonImageTool_ImportImageWithPath_Statics::CommonImageTool_eventImportImageWithPath_Parms), Z_Construct_UFunction_UCommonImageTool_ImportImageWithPath_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonImageTool_ImportImageWithPath_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonImageTool_ImportImageWithPath_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonImageTool_ImportImageWithPath_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonImageTool_ImportImageWithPath()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCommonImageTool_ImportImageWithPath_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UCommonImageTool);
	UClass* Z_Construct_UClass_UCommonImageTool_NoRegister()
	{
		return UCommonImageTool::StaticClass();
	}
	struct Z_Construct_UClass_UCommonImageTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonImageTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonAssetTools,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCommonImageTool_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCommonImageTool_ImportImageWithBinary, "ImportImageWithBinary" }, // 315792254
		{ &Z_Construct_UFunction_UCommonImageTool_ImportImageWithPath, "ImportImageWithPath" }, // 2977886604
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonImageTool_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Category", "CommonAssetTool" },
		{ "IncludePath", "ImageTool/CommonImageTool.h" },
		{ "ModuleRelativePath", "Public/ImageTool/CommonImageTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonImageTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonImageTool>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UCommonImageTool_Statics::ClassParams = {
		&UCommonImageTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonImageTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonImageTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonImageTool()
	{
		if (!Z_Registration_Info_UClass_UCommonImageTool.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UCommonImageTool.OuterSingleton, Z_Construct_UClass_UCommonImageTool_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UCommonImageTool.OuterSingleton;
	}
	template<> COMMONASSETTOOLS_API UClass* StaticClass<UCommonImageTool>()
	{
		return UCommonImageTool::StaticClass();
	}
	UCommonImageTool::UCommonImageTool(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonImageTool);
	UCommonImageTool::~UCommonImageTool() {}
	struct Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageTool_h_Statics
	{
		static const FEnumRegisterCompiledInInfo EnumInfo[];
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FEnumRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageTool_h_Statics::EnumInfo[] = {
		{ FEImageFormat_StaticEnum, TEXT("FEImageFormat"), &Z_Registration_Info_UEnum_FEImageFormat, CONSTRUCT_RELOAD_VERSION_INFO(FEnumReloadVersionInfo, 2587607141U) },
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageTool_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UCommonImageTool, UCommonImageTool::StaticClass, TEXT("UCommonImageTool"), &Z_Registration_Info_UClass_UCommonImageTool, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UCommonImageTool), 921575366U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageTool_h_542824382(TEXT("/Script/CommonAssetTools"),
		Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageTool_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageTool_h_Statics::ClassInfo),
		nullptr, 0,
		Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageTool_h_Statics::EnumInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_ImageTool_CommonImageTool_h_Statics::EnumInfo));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
