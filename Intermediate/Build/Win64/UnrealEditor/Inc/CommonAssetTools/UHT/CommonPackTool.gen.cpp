// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PackageTool/CommonPackTool.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonPackTool() {}
// Cross Module References
	COMMONASSETTOOLS_API UClass* Z_Construct_UClass_UCommonPackTool();
	COMMONASSETTOOLS_API UClass* Z_Construct_UClass_UCommonPackTool_NoRegister();
	COMMONASSETTOOLS_API UScriptStruct* Z_Construct_UScriptStruct_FAssetPackageConfig();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	UPackage* Z_Construct_UPackage__Script_CommonAssetTools();
// End Cross Module References
	static FStructRegistrationInfo Z_Registration_Info_UScriptStruct_AssetPackageConfig;
class UScriptStruct* FAssetPackageConfig::StaticStruct()
{
	if (!Z_Registration_Info_UScriptStruct_AssetPackageConfig.OuterSingleton)
	{
		Z_Registration_Info_UScriptStruct_AssetPackageConfig.OuterSingleton = GetStaticStruct(Z_Construct_UScriptStruct_FAssetPackageConfig, (UObject*)Z_Construct_UPackage__Script_CommonAssetTools(), TEXT("AssetPackageConfig"));
	}
	return Z_Registration_Info_UScriptStruct_AssetPackageConfig.OuterSingleton;
}
template<> COMMONASSETTOOLS_API UScriptStruct* StaticStruct<FAssetPackageConfig>()
{
	return FAssetPackageConfig::StaticStruct();
}
	struct Z_Construct_UScriptStruct_FAssetPackageConfig_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_RefPath_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_RefPath;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_RefObject_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_RefObject;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UECodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAssetPackageConfig_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Category", "CommonAssetTool" },
		{ "ModuleRelativePath", "Public/PackageTool/CommonPackTool.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FAssetPackageConfig_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FAssetPackageConfig>();
	}
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAssetPackageConfig_Statics::NewProp_RefPath_MetaData[] = {
		{ "Category", "CommonAssetTool" },
		{ "ModuleRelativePath", "Public/PackageTool/CommonPackTool.h" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FAssetPackageConfig_Statics::NewProp_RefPath = { "RefPath", nullptr, (EPropertyFlags)0x0010000000000004, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FAssetPackageConfig, RefPath), METADATA_PARAMS(Z_Construct_UScriptStruct_FAssetPackageConfig_Statics::NewProp_RefPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAssetPackageConfig_Statics::NewProp_RefPath_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAssetPackageConfig_Statics::NewProp_RefObject_MetaData[] = {
		{ "Category", "CommonAssetTool" },
		{ "ModuleRelativePath", "Public/PackageTool/CommonPackTool.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FAssetPackageConfig_Statics::NewProp_RefObject = { "RefObject", nullptr, (EPropertyFlags)0x0010000000000004, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FAssetPackageConfig, RefObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FAssetPackageConfig_Statics::NewProp_RefObject_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAssetPackageConfig_Statics::NewProp_RefObject_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FAssetPackageConfig_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAssetPackageConfig_Statics::NewProp_RefPath,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAssetPackageConfig_Statics::NewProp_RefObject,
	};
	const UECodeGen_Private::FStructParams Z_Construct_UScriptStruct_FAssetPackageConfig_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CommonAssetTools,
		nullptr,
		&NewStructOps,
		"AssetPackageConfig",
		sizeof(FAssetPackageConfig),
		alignof(FAssetPackageConfig),
		Z_Construct_UScriptStruct_FAssetPackageConfig_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAssetPackageConfig_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FAssetPackageConfig_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAssetPackageConfig_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FAssetPackageConfig()
	{
		if (!Z_Registration_Info_UScriptStruct_AssetPackageConfig.InnerSingleton)
		{
			UECodeGen_Private::ConstructUScriptStruct(Z_Registration_Info_UScriptStruct_AssetPackageConfig.InnerSingleton, Z_Construct_UScriptStruct_FAssetPackageConfig_Statics::ReturnStructParams);
		}
		return Z_Registration_Info_UScriptStruct_AssetPackageConfig.InnerSingleton;
	}
	DEFINE_FUNCTION(UCommonPackTool::execLoadAssetPackFromFBX)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_DestinationPath);
		P_GET_UBOOL(Z_Param_bAllowAsyncImport);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<UObject*>*)Z_Param__Result=P_THIS->LoadAssetPackFromFBX(Z_Param_DestinationPath,Z_Param_bAllowAsyncImport);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonPackTool::execLoadAsset)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_AssetNameWithoutPathAndSuffix);
		P_GET_UBOOL(Z_Param_bIsLevelAsset);
		P_GET_TARRAY_REF(FAssetPackageConfig,Z_Param_Out_AssetObjects);
		P_GET_TARRAY_REF(FString,Z_Param_Out_LevelRefPaths);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->LoadAsset(Z_Param_AssetNameWithoutPathAndSuffix,Z_Param_bIsLevelAsset,Z_Param_Out_AssetObjects,Z_Param_Out_LevelRefPaths);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonPackTool::execLoadAssetPackage)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_InpakFullPath);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->LoadAssetPackage(Z_Param_InpakFullPath);
		P_NATIVE_END;
	}
	void UCommonPackTool::StaticRegisterNativesUCommonPackTool()
	{
		UClass* Class = UCommonPackTool::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "LoadAsset", &UCommonPackTool::execLoadAsset },
			{ "LoadAssetPackage", &UCommonPackTool::execLoadAssetPackage },
			{ "LoadAssetPackFromFBX", &UCommonPackTool::execLoadAssetPackFromFBX },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCommonPackTool_LoadAsset_Statics
	{
		struct CommonPackTool_eventLoadAsset_Parms
		{
			FString AssetNameWithoutPathAndSuffix;
			bool bIsLevelAsset;
			TArray<FAssetPackageConfig> AssetObjects;
			TArray<FString> LevelRefPaths;
		};
		static const UECodeGen_Private::FStrPropertyParams NewProp_AssetNameWithoutPathAndSuffix;
		static void NewProp_bIsLevelAsset_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_bIsLevelAsset;
		static const UECodeGen_Private::FStructPropertyParams NewProp_AssetObjects_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_AssetObjects;
		static const UECodeGen_Private::FStrPropertyParams NewProp_LevelRefPaths_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_LevelRefPaths;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UCommonPackTool_LoadAsset_Statics::NewProp_AssetNameWithoutPathAndSuffix = { "AssetNameWithoutPathAndSuffix", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonPackTool_eventLoadAsset_Parms, AssetNameWithoutPathAndSuffix), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UCommonPackTool_LoadAsset_Statics::NewProp_bIsLevelAsset_SetBit(void* Obj)
	{
		((CommonPackTool_eventLoadAsset_Parms*)Obj)->bIsLevelAsset = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCommonPackTool_LoadAsset_Statics::NewProp_bIsLevelAsset = { "bIsLevelAsset", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(CommonPackTool_eventLoadAsset_Parms), &Z_Construct_UFunction_UCommonPackTool_LoadAsset_Statics::NewProp_bIsLevelAsset_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UCommonPackTool_LoadAsset_Statics::NewProp_AssetObjects_Inner = { "AssetObjects", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UScriptStruct_FAssetPackageConfig, METADATA_PARAMS(nullptr, 0) }; // 897089184
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UCommonPackTool_LoadAsset_Statics::NewProp_AssetObjects = { "AssetObjects", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonPackTool_eventLoadAsset_Parms, AssetObjects), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) }; // 897089184
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UCommonPackTool_LoadAsset_Statics::NewProp_LevelRefPaths_Inner = { "LevelRefPaths", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UCommonPackTool_LoadAsset_Statics::NewProp_LevelRefPaths = { "LevelRefPaths", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonPackTool_eventLoadAsset_Parms, LevelRefPaths), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonPackTool_LoadAsset_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonPackTool_LoadAsset_Statics::NewProp_AssetNameWithoutPathAndSuffix,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonPackTool_LoadAsset_Statics::NewProp_bIsLevelAsset,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonPackTool_LoadAsset_Statics::NewProp_AssetObjects_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonPackTool_LoadAsset_Statics::NewProp_AssetObjects,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonPackTool_LoadAsset_Statics::NewProp_LevelRefPaths_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonPackTool_LoadAsset_Statics::NewProp_LevelRefPaths,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonPackTool_LoadAsset_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonAssetTool" },
		{ "ModuleRelativePath", "Public/PackageTool/CommonPackTool.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonPackTool_LoadAsset_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonPackTool, nullptr, "LoadAsset", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCommonPackTool_LoadAsset_Statics::CommonPackTool_eventLoadAsset_Parms), Z_Construct_UFunction_UCommonPackTool_LoadAsset_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonPackTool_LoadAsset_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonPackTool_LoadAsset_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonPackTool_LoadAsset_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonPackTool_LoadAsset()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCommonPackTool_LoadAsset_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonPackTool_LoadAssetPackage_Statics
	{
		struct CommonPackTool_eventLoadAssetPackage_Parms
		{
			FString InpakFullPath;
		};
		static const UECodeGen_Private::FStrPropertyParams NewProp_InpakFullPath;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UCommonPackTool_LoadAssetPackage_Statics::NewProp_InpakFullPath = { "InpakFullPath", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonPackTool_eventLoadAssetPackage_Parms, InpakFullPath), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonPackTool_LoadAssetPackage_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonPackTool_LoadAssetPackage_Statics::NewProp_InpakFullPath,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonPackTool_LoadAssetPackage_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonAssetTool" },
		{ "ModuleRelativePath", "Public/PackageTool/CommonPackTool.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonPackTool_LoadAssetPackage_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonPackTool, nullptr, "LoadAssetPackage", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCommonPackTool_LoadAssetPackage_Statics::CommonPackTool_eventLoadAssetPackage_Parms), Z_Construct_UFunction_UCommonPackTool_LoadAssetPackage_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonPackTool_LoadAssetPackage_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonPackTool_LoadAssetPackage_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonPackTool_LoadAssetPackage_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonPackTool_LoadAssetPackage()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCommonPackTool_LoadAssetPackage_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonPackTool_LoadAssetPackFromFBX_Statics
	{
		struct CommonPackTool_eventLoadAssetPackFromFBX_Parms
		{
			FString DestinationPath;
			bool bAllowAsyncImport;
			TArray<UObject*> ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_DestinationPath_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_DestinationPath;
		static void NewProp_bAllowAsyncImport_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_bAllowAsyncImport;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonPackTool_LoadAssetPackFromFBX_Statics::NewProp_DestinationPath_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UCommonPackTool_LoadAssetPackFromFBX_Statics::NewProp_DestinationPath = { "DestinationPath", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonPackTool_eventLoadAssetPackFromFBX_Parms, DestinationPath), METADATA_PARAMS(Z_Construct_UFunction_UCommonPackTool_LoadAssetPackFromFBX_Statics::NewProp_DestinationPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonPackTool_LoadAssetPackFromFBX_Statics::NewProp_DestinationPath_MetaData)) };
	void Z_Construct_UFunction_UCommonPackTool_LoadAssetPackFromFBX_Statics::NewProp_bAllowAsyncImport_SetBit(void* Obj)
	{
		((CommonPackTool_eventLoadAssetPackFromFBX_Parms*)Obj)->bAllowAsyncImport = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCommonPackTool_LoadAssetPackFromFBX_Statics::NewProp_bAllowAsyncImport = { "bAllowAsyncImport", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(CommonPackTool_eventLoadAssetPackFromFBX_Parms), &Z_Construct_UFunction_UCommonPackTool_LoadAssetPackFromFBX_Statics::NewProp_bAllowAsyncImport_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCommonPackTool_LoadAssetPackFromFBX_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UCommonPackTool_LoadAssetPackFromFBX_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonPackTool_eventLoadAssetPackFromFBX_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonPackTool_LoadAssetPackFromFBX_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonPackTool_LoadAssetPackFromFBX_Statics::NewProp_DestinationPath,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonPackTool_LoadAssetPackFromFBX_Statics::NewProp_bAllowAsyncImport,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonPackTool_LoadAssetPackFromFBX_Statics::NewProp_ReturnValue_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonPackTool_LoadAssetPackFromFBX_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonPackTool_LoadAssetPackFromFBX_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonAssetTool" },
		{ "ModuleRelativePath", "Public/PackageTool/CommonPackTool.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonPackTool_LoadAssetPackFromFBX_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonPackTool, nullptr, "LoadAssetPackFromFBX", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCommonPackTool_LoadAssetPackFromFBX_Statics::CommonPackTool_eventLoadAssetPackFromFBX_Parms), Z_Construct_UFunction_UCommonPackTool_LoadAssetPackFromFBX_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonPackTool_LoadAssetPackFromFBX_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonPackTool_LoadAssetPackFromFBX_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonPackTool_LoadAssetPackFromFBX_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonPackTool_LoadAssetPackFromFBX()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCommonPackTool_LoadAssetPackFromFBX_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UCommonPackTool);
	UClass* Z_Construct_UClass_UCommonPackTool_NoRegister()
	{
		return UCommonPackTool::StaticClass();
	}
	struct Z_Construct_UClass_UCommonPackTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_SpawnedActors_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_SpawnedActors_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_SpawnedActors;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonPackTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonAssetTools,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCommonPackTool_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCommonPackTool_LoadAsset, "LoadAsset" }, // 1351703857
		{ &Z_Construct_UFunction_UCommonPackTool_LoadAssetPackage, "LoadAssetPackage" }, // 2062939106
		{ &Z_Construct_UFunction_UCommonPackTool_LoadAssetPackFromFBX, "LoadAssetPackFromFBX" }, // 2189293797
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonPackTool_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Category", "CommonAssetTool" },
		{ "IncludePath", "PackageTool/CommonPackTool.h" },
		{ "ModuleRelativePath", "Public/PackageTool/CommonPackTool.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UCommonPackTool_Statics::NewProp_SpawnedActors_Inner = { "SpawnedActors", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonPackTool_Statics::NewProp_SpawnedActors_MetaData[] = {
		{ "ModuleRelativePath", "Public/PackageTool/CommonPackTool.h" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UCommonPackTool_Statics::NewProp_SpawnedActors = { "SpawnedActors", nullptr, (EPropertyFlags)0x0040000000000000, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UCommonPackTool, SpawnedActors), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UCommonPackTool_Statics::NewProp_SpawnedActors_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonPackTool_Statics::NewProp_SpawnedActors_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCommonPackTool_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonPackTool_Statics::NewProp_SpawnedActors_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonPackTool_Statics::NewProp_SpawnedActors,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonPackTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonPackTool>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UCommonPackTool_Statics::ClassParams = {
		&UCommonPackTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UCommonPackTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UCommonPackTool_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonPackTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonPackTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonPackTool()
	{
		if (!Z_Registration_Info_UClass_UCommonPackTool.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UCommonPackTool.OuterSingleton, Z_Construct_UClass_UCommonPackTool_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UCommonPackTool.OuterSingleton;
	}
	template<> COMMONASSETTOOLS_API UClass* StaticClass<UCommonPackTool>()
	{
		return UCommonPackTool::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonPackTool);
	struct Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackTool_h_Statics
	{
		static const FStructRegisterCompiledInInfo ScriptStructInfo[];
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FStructRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackTool_h_Statics::ScriptStructInfo[] = {
		{ FAssetPackageConfig::StaticStruct, Z_Construct_UScriptStruct_FAssetPackageConfig_Statics::NewStructOps, TEXT("AssetPackageConfig"), &Z_Registration_Info_UScriptStruct_AssetPackageConfig, CONSTRUCT_RELOAD_VERSION_INFO(FStructReloadVersionInfo, sizeof(FAssetPackageConfig), 897089184U) },
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackTool_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UCommonPackTool, UCommonPackTool::StaticClass, TEXT("UCommonPackTool"), &Z_Registration_Info_UClass_UCommonPackTool, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UCommonPackTool), 4293732856U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackTool_h_71384891(TEXT("/Script/CommonAssetTools"),
		Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackTool_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackTool_h_Statics::ClassInfo),
		Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackTool_h_Statics::ScriptStructInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackTool_h_Statics::ScriptStructInfo),
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
