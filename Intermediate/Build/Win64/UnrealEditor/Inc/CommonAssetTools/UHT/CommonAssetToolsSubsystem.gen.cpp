// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonAssetToolsSubsystem.h"
#include "../../Source/Runtime/Engine/Classes/Engine/GameInstance.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonAssetToolsSubsystem() {}
// Cross Module References
	COMMONASSETTOOLS_API UClass* Z_Construct_UClass_UCommonAssetToolFactoryBase_NoRegister();
	COMMONASSETTOOLS_API UClass* Z_Construct_UClass_UCommonAssetToolsSubsystem();
	COMMONASSETTOOLS_API UClass* Z_Construct_UClass_UCommonAssetToolsSubsystem_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UGameInstanceSubsystem();
	UPackage* Z_Construct_UPackage__Script_CommonAssetTools();
// End Cross Module References
	DEFINE_FUNCTION(UCommonAssetToolsSubsystem::execCreateTool)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_OBJECT(UClass,Z_Param_ToolClass);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UCommonAssetToolFactoryBase**)Z_Param__Result=UCommonAssetToolsSubsystem::CreateTool(Z_Param_WorldContextObject,Z_Param_ToolClass);
		P_NATIVE_END;
	}
	void UCommonAssetToolsSubsystem::StaticRegisterNativesUCommonAssetToolsSubsystem()
	{
		UClass* Class = UCommonAssetToolsSubsystem::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CreateTool", &UCommonAssetToolsSubsystem::execCreateTool },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCommonAssetToolsSubsystem_CreateTool_Statics
	{
		struct CommonAssetToolsSubsystem_eventCreateTool_Parms
		{
			const UObject* WorldContextObject;
			TSubclassOf<UCommonAssetToolFactoryBase>  ToolClass;
			UCommonAssetToolFactoryBase* ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_WorldContextObject_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UECodeGen_Private::FClassPropertyParams NewProp_ToolClass;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonAssetToolsSubsystem_CreateTool_Statics::NewProp_WorldContextObject_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCommonAssetToolsSubsystem_CreateTool_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000082, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonAssetToolsSubsystem_eventCreateTool_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UCommonAssetToolsSubsystem_CreateTool_Statics::NewProp_WorldContextObject_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonAssetToolsSubsystem_CreateTool_Statics::NewProp_WorldContextObject_MetaData)) };
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UCommonAssetToolsSubsystem_CreateTool_Statics::NewProp_ToolClass = { "ToolClass", nullptr, (EPropertyFlags)0x0014000000000080, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonAssetToolsSubsystem_eventCreateTool_Parms, ToolClass), Z_Construct_UClass_UClass, Z_Construct_UClass_UCommonAssetToolFactoryBase_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCommonAssetToolsSubsystem_CreateTool_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonAssetToolsSubsystem_eventCreateTool_Parms, ReturnValue), Z_Construct_UClass_UCommonAssetToolFactoryBase_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonAssetToolsSubsystem_CreateTool_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonAssetToolsSubsystem_CreateTool_Statics::NewProp_WorldContextObject,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonAssetToolsSubsystem_CreateTool_Statics::NewProp_ToolClass,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonAssetToolsSubsystem_CreateTool_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonAssetToolsSubsystem_CreateTool_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonAssetTool" },
		{ "DeterminesOutputType", "ToolClass" },
		{ "ModuleRelativePath", "Public/CommonAssetToolsSubsystem.h" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonAssetToolsSubsystem_CreateTool_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonAssetToolsSubsystem, nullptr, "CreateTool", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCommonAssetToolsSubsystem_CreateTool_Statics::CommonAssetToolsSubsystem_eventCreateTool_Parms), Z_Construct_UFunction_UCommonAssetToolsSubsystem_CreateTool_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonAssetToolsSubsystem_CreateTool_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonAssetToolsSubsystem_CreateTool_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonAssetToolsSubsystem_CreateTool_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonAssetToolsSubsystem_CreateTool()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCommonAssetToolsSubsystem_CreateTool_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UCommonAssetToolsSubsystem);
	UClass* Z_Construct_UClass_UCommonAssetToolsSubsystem_NoRegister()
	{
		return UCommonAssetToolsSubsystem::StaticClass();
	}
	struct Z_Construct_UClass_UCommonAssetToolsSubsystem_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonAssetToolsSubsystem_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGameInstanceSubsystem,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonAssetTools,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCommonAssetToolsSubsystem_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCommonAssetToolsSubsystem_CreateTool, "CreateTool" }, // 2233618031
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonAssetToolsSubsystem_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "CommonAssetToolsSubsystem.h" },
		{ "ModuleRelativePath", "Public/CommonAssetToolsSubsystem.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonAssetToolsSubsystem_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonAssetToolsSubsystem>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UCommonAssetToolsSubsystem_Statics::ClassParams = {
		&UCommonAssetToolsSubsystem::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonAssetToolsSubsystem_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonAssetToolsSubsystem_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonAssetToolsSubsystem()
	{
		if (!Z_Registration_Info_UClass_UCommonAssetToolsSubsystem.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UCommonAssetToolsSubsystem.OuterSingleton, Z_Construct_UClass_UCommonAssetToolsSubsystem_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UCommonAssetToolsSubsystem.OuterSingleton;
	}
	template<> COMMONASSETTOOLS_API UClass* StaticClass<UCommonAssetToolsSubsystem>()
	{
		return UCommonAssetToolsSubsystem::StaticClass();
	}
	UCommonAssetToolsSubsystem::UCommonAssetToolsSubsystem() {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonAssetToolsSubsystem);
	UCommonAssetToolsSubsystem::~UCommonAssetToolsSubsystem() {}
	struct Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolsSubsystem_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolsSubsystem_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UCommonAssetToolsSubsystem, UCommonAssetToolsSubsystem::StaticClass, TEXT("UCommonAssetToolsSubsystem"), &Z_Registration_Info_UClass_UCommonAssetToolsSubsystem, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UCommonAssetToolsSubsystem), 2274370780U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolsSubsystem_h_1110589087(TEXT("/Script/CommonAssetTools"),
		Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolsSubsystem_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolsSubsystem_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
