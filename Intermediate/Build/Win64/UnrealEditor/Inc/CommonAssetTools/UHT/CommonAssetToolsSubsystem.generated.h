// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "CommonAssetToolsSubsystem.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UCommonAssetToolFactoryBase;
class UObject;
#ifdef COMMONASSETTOOLS_CommonAssetToolsSubsystem_generated_h
#error "CommonAssetToolsSubsystem.generated.h already included, missing '#pragma once' in CommonAssetToolsSubsystem.h"
#endif
#define COMMONASSETTOOLS_CommonAssetToolsSubsystem_generated_h

#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolsSubsystem_h_14_SPARSE_DATA
#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolsSubsystem_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execCreateTool);


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolsSubsystem_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execCreateTool);


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolsSubsystem_h_14_ACCESSORS
#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolsSubsystem_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonAssetToolsSubsystem(); \
	friend struct Z_Construct_UClass_UCommonAssetToolsSubsystem_Statics; \
public: \
	DECLARE_CLASS(UCommonAssetToolsSubsystem, UGameInstanceSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonAssetTools"), NO_API) \
	DECLARE_SERIALIZER(UCommonAssetToolsSubsystem)


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolsSubsystem_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUCommonAssetToolsSubsystem(); \
	friend struct Z_Construct_UClass_UCommonAssetToolsSubsystem_Statics; \
public: \
	DECLARE_CLASS(UCommonAssetToolsSubsystem, UGameInstanceSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonAssetTools"), NO_API) \
	DECLARE_SERIALIZER(UCommonAssetToolsSubsystem)


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolsSubsystem_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonAssetToolsSubsystem(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonAssetToolsSubsystem) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonAssetToolsSubsystem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonAssetToolsSubsystem); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonAssetToolsSubsystem(UCommonAssetToolsSubsystem&&); \
	NO_API UCommonAssetToolsSubsystem(const UCommonAssetToolsSubsystem&); \
public: \
	NO_API virtual ~UCommonAssetToolsSubsystem();


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolsSubsystem_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonAssetToolsSubsystem(); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonAssetToolsSubsystem(UCommonAssetToolsSubsystem&&); \
	NO_API UCommonAssetToolsSubsystem(const UCommonAssetToolsSubsystem&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonAssetToolsSubsystem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonAssetToolsSubsystem); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UCommonAssetToolsSubsystem) \
	NO_API virtual ~UCommonAssetToolsSubsystem();


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolsSubsystem_h_11_PROLOG
#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolsSubsystem_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolsSubsystem_h_14_SPARSE_DATA \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolsSubsystem_h_14_RPC_WRAPPERS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolsSubsystem_h_14_ACCESSORS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolsSubsystem_h_14_INCLASS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolsSubsystem_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolsSubsystem_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolsSubsystem_h_14_SPARSE_DATA \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolsSubsystem_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolsSubsystem_h_14_ACCESSORS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolsSubsystem_h_14_INCLASS_NO_PURE_DECLS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolsSubsystem_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONASSETTOOLS_API UClass* StaticClass<class UCommonAssetToolsSubsystem>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_CommonAssetToolsSubsystem_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
