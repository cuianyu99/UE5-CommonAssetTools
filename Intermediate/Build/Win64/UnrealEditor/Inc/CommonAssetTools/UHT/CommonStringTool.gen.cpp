// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "StringTool/CommonStringTool.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonStringTool() {}
// Cross Module References
	COMMONASSETTOOLS_API UClass* Z_Construct_UClass_UCommonStringTool();
	COMMONASSETTOOLS_API UClass* Z_Construct_UClass_UCommonStringTool_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_CommonAssetTools();
// End Cross Module References
	DEFINE_FUNCTION(UCommonStringTool::execAnyStringToSha1)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_AnyStr);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=P_THIS->AnyStringToSha1(Z_Param_AnyStr);
		P_NATIVE_END;
	}
	void UCommonStringTool::StaticRegisterNativesUCommonStringTool()
	{
		UClass* Class = UCommonStringTool::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AnyStringToSha1", &UCommonStringTool::execAnyStringToSha1 },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCommonStringTool_AnyStringToSha1_Statics
	{
		struct CommonStringTool_eventAnyStringToSha1_Parms
		{
			FString AnyStr;
			FString ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_AnyStr_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_AnyStr;
		static const UECodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonStringTool_AnyStringToSha1_Statics::NewProp_AnyStr_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UCommonStringTool_AnyStringToSha1_Statics::NewProp_AnyStr = { "AnyStr", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonStringTool_eventAnyStringToSha1_Parms, AnyStr), METADATA_PARAMS(Z_Construct_UFunction_UCommonStringTool_AnyStringToSha1_Statics::NewProp_AnyStr_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonStringTool_AnyStringToSha1_Statics::NewProp_AnyStr_MetaData)) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UCommonStringTool_AnyStringToSha1_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CommonStringTool_eventAnyStringToSha1_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonStringTool_AnyStringToSha1_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonStringTool_AnyStringToSha1_Statics::NewProp_AnyStr,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonStringTool_AnyStringToSha1_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonStringTool_AnyStringToSha1_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonAssetTool" },
		{ "Comment", "/// \xe4\xbb\xbb\xe6\x84\x8f\xe5\xad\x97\xe7\xac\xa6\xe4\xb8\xb2\xe8\xbd\xacSHA1\xe7\xa0\x81\n" },
		{ "ModuleRelativePath", "Public/StringTool/CommonStringTool.h" },
		{ "ToolTip", "\xe4\xbb\xbb\xe6\x84\x8f\xe5\xad\x97\xe7\xac\xa6\xe4\xb8\xb2\xe8\xbd\xacSHA1\xe7\xa0\x81" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonStringTool_AnyStringToSha1_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonStringTool, nullptr, "AnyStringToSha1", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCommonStringTool_AnyStringToSha1_Statics::CommonStringTool_eventAnyStringToSha1_Parms), Z_Construct_UFunction_UCommonStringTool_AnyStringToSha1_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonStringTool_AnyStringToSha1_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonStringTool_AnyStringToSha1_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonStringTool_AnyStringToSha1_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonStringTool_AnyStringToSha1()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCommonStringTool_AnyStringToSha1_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UCommonStringTool);
	UClass* Z_Construct_UClass_UCommonStringTool_NoRegister()
	{
		return UCommonStringTool::StaticClass();
	}
	struct Z_Construct_UClass_UCommonStringTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonStringTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonAssetTools,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCommonStringTool_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCommonStringTool_AnyStringToSha1, "AnyStringToSha1" }, // 3654184283
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonStringTool_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Category", "CommonAssetTool" },
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "StringTool/CommonStringTool.h" },
		{ "ModuleRelativePath", "Public/StringTool/CommonStringTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonStringTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonStringTool>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UCommonStringTool_Statics::ClassParams = {
		&UCommonStringTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonStringTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonStringTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonStringTool()
	{
		if (!Z_Registration_Info_UClass_UCommonStringTool.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UCommonStringTool.OuterSingleton, Z_Construct_UClass_UCommonStringTool_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UCommonStringTool.OuterSingleton;
	}
	template<> COMMONASSETTOOLS_API UClass* StaticClass<UCommonStringTool>()
	{
		return UCommonStringTool::StaticClass();
	}
	UCommonStringTool::UCommonStringTool(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonStringTool);
	UCommonStringTool::~UCommonStringTool() {}
	struct Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_StringTool_CommonStringTool_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_StringTool_CommonStringTool_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UCommonStringTool, UCommonStringTool::StaticClass, TEXT("UCommonStringTool"), &Z_Registration_Info_UClass_UCommonStringTool, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UCommonStringTool), 1847961019U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_StringTool_CommonStringTool_h_2637088602(TEXT("/Script/CommonAssetTools"),
		Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_StringTool_CommonStringTool_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_StringTool_CommonStringTool_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
