// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "StringTool/CommonStringTool.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMMONASSETTOOLS_CommonStringTool_generated_h
#error "CommonStringTool.generated.h already included, missing '#pragma once' in CommonStringTool.h"
#endif
#define COMMONASSETTOOLS_CommonStringTool_generated_h

#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_StringTool_CommonStringTool_h_15_SPARSE_DATA
#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_StringTool_CommonStringTool_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execAnyStringToSha1);


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_StringTool_CommonStringTool_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execAnyStringToSha1);


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_StringTool_CommonStringTool_h_15_ACCESSORS
#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_StringTool_CommonStringTool_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonStringTool(); \
	friend struct Z_Construct_UClass_UCommonStringTool_Statics; \
public: \
	DECLARE_CLASS(UCommonStringTool, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonAssetTools"), NO_API) \
	DECLARE_SERIALIZER(UCommonStringTool)


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_StringTool_CommonStringTool_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUCommonStringTool(); \
	friend struct Z_Construct_UClass_UCommonStringTool_Statics; \
public: \
	DECLARE_CLASS(UCommonStringTool, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonAssetTools"), NO_API) \
	DECLARE_SERIALIZER(UCommonStringTool)


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_StringTool_CommonStringTool_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonStringTool(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonStringTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonStringTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonStringTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonStringTool(UCommonStringTool&&); \
	NO_API UCommonStringTool(const UCommonStringTool&); \
public: \
	NO_API virtual ~UCommonStringTool();


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_StringTool_CommonStringTool_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonStringTool(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonStringTool(UCommonStringTool&&); \
	NO_API UCommonStringTool(const UCommonStringTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonStringTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonStringTool); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonStringTool) \
	NO_API virtual ~UCommonStringTool();


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_StringTool_CommonStringTool_h_12_PROLOG
#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_StringTool_CommonStringTool_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_StringTool_CommonStringTool_h_15_SPARSE_DATA \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_StringTool_CommonStringTool_h_15_RPC_WRAPPERS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_StringTool_CommonStringTool_h_15_ACCESSORS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_StringTool_CommonStringTool_h_15_INCLASS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_StringTool_CommonStringTool_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_StringTool_CommonStringTool_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_StringTool_CommonStringTool_h_15_SPARSE_DATA \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_StringTool_CommonStringTool_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_StringTool_CommonStringTool_h_15_ACCESSORS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_StringTool_CommonStringTool_h_15_INCLASS_NO_PURE_DECLS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_StringTool_CommonStringTool_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONASSETTOOLS_API UClass* StaticClass<class UCommonStringTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_StringTool_CommonStringTool_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
