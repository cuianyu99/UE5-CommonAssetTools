// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "PackageTool/CommonPackageToolFactory.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UCommonPackTool;
#ifdef COMMONASSETTOOLS_CommonPackageToolFactory_generated_h
#error "CommonPackageToolFactory.generated.h already included, missing '#pragma once' in CommonPackageToolFactory.h"
#endif
#define COMMONASSETTOOLS_CommonPackageToolFactory_generated_h

#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackageToolFactory_h_12_SPARSE_DATA
#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackageToolFactory_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execCreateTool);


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackageToolFactory_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execCreateTool);


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackageToolFactory_h_12_ACCESSORS
#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackageToolFactory_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonPackageToolFactory(); \
	friend struct Z_Construct_UClass_UCommonPackageToolFactory_Statics; \
public: \
	DECLARE_CLASS(UCommonPackageToolFactory, UCommonAssetToolFactoryBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonAssetTools"), NO_API) \
	DECLARE_SERIALIZER(UCommonPackageToolFactory)


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackageToolFactory_h_12_INCLASS \
private: \
	static void StaticRegisterNativesUCommonPackageToolFactory(); \
	friend struct Z_Construct_UClass_UCommonPackageToolFactory_Statics; \
public: \
	DECLARE_CLASS(UCommonPackageToolFactory, UCommonAssetToolFactoryBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonAssetTools"), NO_API) \
	DECLARE_SERIALIZER(UCommonPackageToolFactory)


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackageToolFactory_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonPackageToolFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonPackageToolFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonPackageToolFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonPackageToolFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonPackageToolFactory(UCommonPackageToolFactory&&); \
	NO_API UCommonPackageToolFactory(const UCommonPackageToolFactory&); \
public: \
	NO_API virtual ~UCommonPackageToolFactory();


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackageToolFactory_h_12_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonPackageToolFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonPackageToolFactory(UCommonPackageToolFactory&&); \
	NO_API UCommonPackageToolFactory(const UCommonPackageToolFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonPackageToolFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonPackageToolFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonPackageToolFactory) \
	NO_API virtual ~UCommonPackageToolFactory();


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackageToolFactory_h_9_PROLOG
#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackageToolFactory_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackageToolFactory_h_12_SPARSE_DATA \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackageToolFactory_h_12_RPC_WRAPPERS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackageToolFactory_h_12_ACCESSORS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackageToolFactory_h_12_INCLASS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackageToolFactory_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackageToolFactory_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackageToolFactory_h_12_SPARSE_DATA \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackageToolFactory_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackageToolFactory_h_12_ACCESSORS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackageToolFactory_h_12_INCLASS_NO_PURE_DECLS \
	FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackageToolFactory_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONASSETTOOLS_API UClass* StaticClass<class UCommonPackageToolFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Demo_ZHBS_Plugins_CommonAssetTools_Source_CommonAssetTools_Public_PackageTool_CommonPackageToolFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
